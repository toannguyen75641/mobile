<?php

Route::prefix('admin')->group(function() {
	Route::get('/', 'Admin\ProductController@index')->name('admin');

	Route::name('product.')->prefix('product')->group(function() {
		Route::get('/', 'Admin\ProductController@index')->name('index');
		Route::get('/show/{id}', 'Admin\ProductController@show')->name('show');
		Route::get('/create', 'Admin\ProductController@create')->name('create');
		Route::post('/store', 'Admin\ProductController@store')->name('store');
		Route::get('/edit/{id}', 'Admin\ProductController@edit')->name('edit');
		Route::post('/update/{id}', 'Admin\ProductController@update')->name('update');
		Route::get('/destroy/{id}', 'Admin\ProductController@destroy')->name('destroy');
	});

	Route::name('category.')->prefix('category')->group(function() {
		Route::get('/', 'Admin\CategoryController@index')->name('index');
		Route::get('/show/{id}', 'Admin\CategoryController@show')->name('show');
		Route::get('/create', 'Admin\CategoryController@create')->name('create');
		Route::post('/store', 'Admin\CategoryController@store')->name('store');
		Route::get('/edit/{id}', 'Admin\CategoryController@edit')->name('edit');
		Route::post('/update/{id}', 'Admin\CategoryController@update')->name('update');
		Route::get('/destroy/{id}', 'Admin\CategoryController@destroy')->name('destroy');
	});

	Route::name('customer.')->prefix('customer')->group(function() {
		Route::get('/', 'Admin\CustomerController@index')->name('index');
		Route::get('/show/{id}', 'Admin\CustomerController@show')->name('show');
		Route::get('/destroy/{id}', 'Admin\CustomerController@destroy')->name('destroy');
	});

	Route::name('order.')->prefix('order')->group(function() {
		Route::get('/', 'Admin\OrderController@index')->name('index');
		Route::get('/show/{id}', 'Admin\OrderController@show')->name('show');
		Route::get('/edit/{id}', 'Admin\OrderController@edit')->name('edit');
		Route::post('/update/{id}', 'Admin\OrderController@update')->name('update');
		Route::get('/destroy/{id}', 'Admin\OrderController@destroy')->name('destroy');
	});

	Route::name('user.')->prefix('user')->group(function() {
		Route::get('/', 'Admin\UserController@index')->name('index');
		Route::get('/show/{id}', 'Admin\UserController@show')->name('show');
		Route::get('/edit/{id}', 'Admin\UserController@edit')->name('edit');
		Route::post('/update/{id}', 'Admin\UserController@update')->name('update');
		Route::get('/destroy/{id}', 'Admin\UserController@destroy')->name('destroy');
	});

	Route::name('role.')->prefix('role')->group(function() {
		Route::get('/', 'Admin\RoleController@index')->name('index');
		Route::get('/show/{id}', 'Admin\RoleController@show')->name('show');
		Route::get('/create', 'Admin\RoleController@create')->name('create');
		Route::post('/store', 'Admin\RoleController@store')->name('store');
		Route::get('/edit/{id}', 'Admin\RoleController@edit')->name('edit');
		Route::post('/update/{id}', 'Admin\RoleController@update')->name('update');
		Route::get('/destroy/{id}', 'Admin\RoleController@destroy')->name('destroy');
	});

	Route::name('slider.')->prefix('slider')->group(function() {
		Route::get('/', 'Admin\SliderController@index')->name('index');
		Route::get('/show/{id}', 'Admin\SliderController@show')->name('show');
		Route::get('/create', 'Admin\SliderController@create')->name('create');
		Route::post('/store', 'Admin\SliderController@store')->name('store');
		Route::get('/edit/{id}', 'Admin\SliderController@edit')->name('edit');
		Route::post('/update/{id}', 'Admin\SliderController@update')->name('update');
		Route::get('/destroy/{id}', 'Admin\SliderController@destroy')->name('destroy');
	});

	Route::name('slide.')->prefix('slide')->group(function() {
		Route::get('/', 'Admin\SlideController@index')->name('index');
		Route::get('/show/{id}', 'Admin\SlideController@show')->name('show');
		Route::get('/create', 'Admin\SlideController@create')->name('create');
		Route::post('/store', 'Admin\SlideController@store')->name('store');
		Route::get('/edit/{id}', 'Admin\SlideController@edit')->name('edit');
		Route::post('/update/{id}', 'Admin\SlideController@update')->name('update');
		Route::get('/destroy/{id}', 'Admin\SlideController@destroy')->name('destroy');
	});

	Route::name('carts.')->prefix('carts')->group(function() {
		Route::get('/', 'Admin\CartController@index')->name('index');
	});

	Route::name('contacts.')->prefix('contacts')->group(function() {
		Route::get('/', 'Admin\ContactController@index')->name('index');
		Route::get('/destroy/{id}', 'Admin\ContactController@destroy')->name('destroy');
	});

	Route::name('admin.')->group(function() {
		Route::get('login','Admin\AuthController@showLoginForm')->name('login');
		Route::post('login','Admin\AuthController@login');
		Route::get('register','Admin\AuthController@showRegistrationForm')->name('register');
		Route::post('register','Admin\AuthController@register');
		Route::post('logout','Admin\AuthController@logout')->name('logout');
	});
});

Auth::routes(['verify' => true]);

Route::get('/', 'HomeController@index')->name('home');

Route::name('store.')->prefix('store')->group(function() {
	Route::get('/', 'HomeController@store')->name('index');
	Route::get('/{id}', 'HomeController@category')->name('id');
});

Route::name('filter.')->prefix('filter')->group(function() {
	Route::post('/', 'HomeController@filter')->name('index');
});

Route::name('page.')->prefix('page')->group(function() {
	Route::get('/', 'HomeController@filter')->name('index');
	Route::get('?page={page}', 'HomeController@filter')->name('page');
});

Route::get('product/{id}', 'HomeController@product')->name('product');

Route::name('cart.')->prefix('cart')->group(function() {
	Route::get('/', 'CartController@index')->name('index');
	Route::put('add', 'CartController@add')->name('add');
	Route::patch('update', 'CartController@update')->name('update');
	Route::delete('delete', 'CartController@delete')->name('delete');
});

Route::get('search', 'HomeController@search')->name('search');

Route::name('contact.')->prefix('contact')->group(function() {
	Route::get('/', 'ContactController@index')->name('index');
	Route::post('/send', 'ContactController@sendMail')->middleware('verified')->name('sendmail');
});

Route::name('checkout.')->prefix('checkout')->group(function() {
	Route::get('/', 'CheckOutController@index')->name('index');
	Route::post('checkout', 'CheckOutController@checkout')->name('checkout');
});
