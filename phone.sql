-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th6 03, 2019 lúc 06:33 PM
-- Phiên bản máy phục vụ: 10.1.37-MariaDB
-- Phiên bản PHP: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `phone`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `carts`
--

CREATE TABLE `carts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty` int(11) NOT NULL DEFAULT '1',
  `stock_qty` int(11) NOT NULL,
  `price` double(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `carts`
--

INSERT INTO `carts` (`id`, `customer_id`, `product_id`, `name`, `image`, `qty`, `stock_qty`, `price`, `created_at`) VALUES
(2, 7, 171, 'Product Name', 'https://lorempixel.com/640/480/?67642', 2, 68, 9.03, '2019-06-03 01:48:51'),
(3, 7, 172, 'Product Name', 'https://lorempixel.com/640/480/?90561', 1, 56, 7.98, '2019-06-03 01:48:51');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Iphone', 'Est molestiae dicta quo dolorem aliquid. Debitis debitis expedita molestiae ducimus aut nostrum. Aspernatur aut sint harum ea.', '2019-03-14 02:14:13', '2019-03-15 02:07:04'),
(2, 'Samsung', 'Enim aperiam earum quia recusandae quibusdam ut distinctio. Unde in nihil qui. Sed quae dolore voluptates optio commodi quasi eius. Et corrupti ut ut facere provident voluptatem.', '2019-03-14 02:14:13', '2019-03-15 02:07:12'),
(3, 'Oppo', 'Natus est asperiores nobis doloribus. Adipisci exercitationem exercitationem quasi quis eum. Nesciunt omnis eum in.', '2019-03-14 02:14:13', '2019-03-15 02:07:40'),
(4, 'Nokia', 'Doloremque quo fuga voluptatibus. Quia et nisi non ea. Excepturi eaque similique voluptatem ipsam quia consequatur necessitatibus. Et earum dignissimos et qui.', '2019-03-14 02:14:13', '2019-03-15 02:07:50'),
(5, 'Xiaomi', 'Eligendi dolor neque dolorem omnis aut. Saepe excepturi necessitatibus est nihil neque. Quae esse asperiores sed soluta eligendi qui sunt.', '2019-03-14 02:14:13', '2019-03-15 02:09:01'),
(6, 'Huawei', 'Praesentium unde sapiente qui quia corrupti repudiandae dignissimos. Porro magnam dolorum soluta tenetur ut.', '2019-03-14 02:14:13', '2019-03-15 02:09:21'),
(7, 'Vsmart', 'Voluptatem molestiae omnis tempore consequatur quisquam. Modi vero et neque ipsum sed aliquid magni.', '2019-03-14 02:14:13', '2019-03-15 02:09:35'),
(8, 'Asus', 'Magnam impedit sint natus iste unde exercitationem autem vel. Ab adipisci dolores autem autem ipsum.', '2019-03-14 02:14:13', '2019-03-15 02:09:46'),
(9, 'Coolpad', 'Enim ut in nemo quasi sed debitis et et. Odit cupiditate consequatur quia atque. Vero qui possimus magni sit voluptate. Enim repellendus velit eum optio qui officia voluptas.', '2019-03-14 02:14:13', '2019-03-15 02:09:57'),
(10, 'Mobiistar', 'Inventore velit aut eaque quo quos rerum. Distinctio deleniti quis perspiciatis sapiente minus nesciunt accusamus. Iusto explicabo autem molestiae commodi quisquam aut. Sint incidunt culpa ut velit.', '2019-03-14 02:14:13', '2019-03-15 02:10:21');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `contacts`
--

INSERT INTO `contacts` (`id`, `customer_id`, `name`, `email`, `subject`, `message`, `created_at`) VALUES
(16, 7, 'toan', 'toan.nguyen.jvb@gmail.com', 'Hello', 'abc', '2019-06-03 03:22:32'),
(17, 7, 'toan', 'toan.nguyen.jvb@gmail.com', 'Hello', 'abc', '2019-06-03 03:25:14');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `gender` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `customers`
--

INSERT INTO `customers` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `phone`, `gender`, `birthday`, `address`, `created_at`, `updated_at`) VALUES
(7, 'toan', 'toan.nguyen.jvb@gmail.com', '2019-06-02 08:22:04', '$2y$10$tla/cOa8k./odmd/eecvae/trseRc8ETm8CbrNAO7KbTlaKq/0ke.', NULL, NULL, NULL, NULL, NULL, '2019-06-02 08:21:39', '2019-06-02 08:22:04'),
(8, 'duc toan', NULL, NULL, '$2y$10$3mZwO.0HS7TdIPydZuhXkugg8YY0OrCYteEzPxJU9rhfcweaaFqx.', NULL, 969116097, 'male', '1111-02-11', '211 trung van', '2019-06-02 23:58:17', '2019-06-02 23:58:17');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `image_albums`
--

CREATE TABLE `image_albums` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `image_albums`
--

INSERT INTO `image_albums` (`id`, `product_id`, `image`, `url`) VALUES
(16, 119, '636748771945393060_iPhone-Xs-Max-gold.png', 'uploads/636748771945393060_iPhone-Xs-Max-gold.png'),
(17, 119, '636767481289803972_iphone-xs-gold-3.png', 'uploads/636767481289803972_iphone-xs-gold-3.png'),
(18, 119, '636767481293188883_iphone-xs-gold-2.png', 'uploads/636767481293188883_iphone-xs-gold-2.png'),
(19, 119, '636767481293463872_iphone-xs-gold-4.png', 'uploads/636767481293463872_iphone-xs-gold-4.png');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(76, '2014_10_12_100000_create_password_resets_table', 1),
(87, '2019_03_07_090306_create_products_table', 2),
(88, '2019_03_07_092232_create_categories_table', 2),
(89, '2019_03_07_092332_create_product_details_table', 2),
(90, '2019_03_07_092607_create_image_albums_table', 2),
(92, '2019_03_07_092657_create_orders_table', 2),
(93, '2019_03_07_092718_create_order_details_table', 2),
(95, '2019_03_18_101631_create_roles_table', 3),
(96, '2019_03_18_102035_create_role_users_table', 3),
(97, '2019_03_19_141943_create_permissions_table', 3),
(98, '2019_03_19_145047_create_permission_role_table', 4),
(103, '2019_03_27_083354_create_slides_table', 5),
(104, '2019_03_27_094926_create_sliders_table', 5),
(111, '2019_05_13_082906_create_carts_table', 6),
(112, '2014_10_12_000000_create_users_table', 7),
(113, '2019_03_07_092748_create_contacts_table', 7),
(116, '2019_03_07_092636_create_customers_table', 8),
(117, '2019_06_01_074806_create_customer_accounts_table', 8);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `customer_id` bigint(20) NOT NULL,
  `amount` double(10,2) NOT NULL DEFAULT '0.00',
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `orders`
--

INSERT INTO `orders` (`id`, `customer_id`, `amount`, `state`, `created_at`, `updated_at`) VALUES
(1, 8, 9.03, 0, '2019-06-02 23:58:17', '2019-06-02 23:58:17');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `order_details`
--

CREATE TABLE `order_details` (
  `order_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` double(10,2) NOT NULL,
  `amount` double(8,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `order_details`
--

INSERT INTO `order_details` (`order_id`, `product_id`, `quantity`, `price`, `amount`) VALUES
(1, 171, 1, 9.03, 9.03);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('toan.nguyen.jvb@gmail.com', '$2y$10$v9JaphqngDlRN7E.5PNUNefRI2isKGyDYCr2WXvT/qONNWbhsH1hO', '2019-06-02 08:55:23');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `for` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `for`, `created_at`) VALUES
(1, 'Category-Create', 'category', '2019-03-19 15:29:02'),
(2, 'Category-Update', 'category', '2019-03-19 15:29:02'),
(3, 'Category-Delete', 'category', '2019-03-19 15:29:02'),
(4, 'Product-Create', 'product', '2019-03-19 15:29:02'),
(5, 'Product-Update', 'product', '2019-03-19 15:29:02'),
(6, 'Product-Delete', 'product', '2019-03-19 15:29:02'),
(7, 'User-Update', 'user', '2019-03-19 15:29:02'),
(8, 'User-Delete', 'user', '2019-03-19 15:29:02'),
(9, 'Role-Create', 'role', '2019-03-25 17:00:00'),
(10, 'Role-Update', 'role', '2019-03-25 17:00:00'),
(11, 'Role-Delete', 'role', '2019-03-25 17:00:00'),
(12, 'Slider-Create', 'slider', '2019-04-03 17:00:00'),
(13, 'Slider-Update', 'slider', '2019-04-03 17:00:00'),
(14, 'Slider-Delete', 'slider', '2019-04-03 17:00:00'),
(15, 'Contact-Delete', 'contact', '2019-06-02 17:00:00'),
(16, 'Order-Update', 'order', '2019-06-02 17:00:00'),
(17, 'Order-Delete', 'order', '2019-06-02 17:00:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `permission_role`
--

CREATE TABLE `permission_role` (
  `role_id` bigint(20) NOT NULL,
  `permission_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `permission_role`
--

INSERT INTO `permission_role` (`role_id`, `permission_id`) VALUES
(2, 1),
(2, 2),
(2, 3),
(2, 4),
(2, 5),
(2, 6);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

CREATE TABLE `products` (
  `id` bigint(20) NOT NULL,
  `category_id` bigint(20) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(10,2) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`id`, `category_id`, `name`, `price`, `quantity`, `description`, `image`, `status`, `created_at`, `updated_at`) VALUES
(119, 1, 'Iphone XS max 512GB', 8.27, 72, 'Alias eligendi dolor officia vero. Ut qui tenetur dignissimos ut nostrum molestiae velit. Ex quo perspiciatis maxime ut.', 'uploads/636748771945393060_iPhone-Xs-Max-gold.png', 1, '2019-03-22 03:29:43', '2019-05-28 21:30:57'),
(120, 9, 'Product Name', 7.16, 64, 'Ducimus aut voluptatem et ipsum quo a accusamus. Libero vel libero qui nulla adipisci sit. Delectus aut voluptatem tempore ducimus qui placeat quisquam.', 'https://lorempixel.com/640/480/?70687', 1, '2019-03-22 03:29:43', '2019-05-28 21:08:03'),
(121, 2, 'Product Name', 4.33, 53, 'Distinctio ut voluptas porro soluta. Est aperiam sit ipsum et. Est ratione quidem unde et magnam. Dignissimos reiciendis voluptatem et.', 'https://lorempixel.com/640/480/?82596', 0, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(122, 6, 'Product Name', 7.73, 36, 'Blanditiis rerum cumque ipsum exercitationem inventore. Facilis voluptates modi aperiam nobis. Perspiciatis qui voluptatem corporis voluptatem.', 'https://lorempixel.com/640/480/?33094', 1, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(123, 7, 'Product Name', 2.50, 29, 'Libero omnis quo est repellat ab qui enim nobis. Et dolores facilis aliquid aut. Aliquam sunt reiciendis vero exercitationem voluptatem.', 'https://lorempixel.com/640/480/?96361', 1, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(124, 3, 'Product Name', 2.74, 90, 'Doloremque qui sed nostrum magni ullam qui. Qui numquam hic non blanditiis at. Quibusdam iusto fuga deserunt mollitia et vitae error beatae. Et vel qui sit.', 'https://lorempixel.com/640/480/?67793', 1, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(125, 1, 'Product Name', 0.88, 58, 'Nam saepe voluptas et quidem et illum. Ex assumenda atque earum voluptatem. Ducimus dolores sunt nisi aut velit corporis. Voluptates natus voluptas rem quos perspiciatis.', 'https://lorempixel.com/640/480/?78316', 1, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(126, 8, 'Product Name', 9.99, 86, 'Ea a neque accusantium ipsum. Aut nobis aspernatur eos nisi. Id in veritatis voluptatem quos consequatur labore quisquam.', 'https://lorempixel.com/640/480/?45485', 1, '2019-03-21 03:29:43', '2019-03-22 03:29:43'),
(127, 9, 'Product Name', 4.78, 72, 'Fuga qui quia nesciunt. Pariatur impedit quaerat et ut. Numquam rem quaerat blanditiis numquam provident omnis et. Architecto eos voluptates aut reprehenderit.', 'https://lorempixel.com/640/480/?57588', 1, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(128, 10, 'Product Name', 7.51, 75, 'Ex magni a est ea rerum ab. Rerum ut animi labore exercitationem et. Possimus ea optio corrupti sed unde est. Sit cumque repudiandae quaerat qui enim.', 'https://lorempixel.com/640/480/?81675', 1, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(129, 5, 'Product Name', 9.56, 39, 'Eos et qui aliquid porro fuga est. Voluptatem dolores placeat recusandae nostrum quasi ex. Libero autem non aut ex.', 'https://lorempixel.com/640/480/?36707', 0, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(130, 2, 'Product Name', 1.02, 66, 'Praesentium non quidem fuga. Exercitationem quae provident velit consectetur est sit. Est reprehenderit voluptatem distinctio eveniet adipisci odit molestiae.', 'https://lorempixel.com/640/480/?45173', 1, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(131, 9, 'Product Name', 5.40, 54, 'Dolore sunt aliquam aspernatur nulla magni sed. Iusto sed enim minus nemo quo dolor. Et eos nesciunt alias. Aut quae molestias quis natus sunt. Inventore accusamus sed distinctio minus.', 'https://lorempixel.com/640/480/?17337', 0, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(132, 6, 'Product Name', 8.62, 38, 'Dicta laborum corporis eveniet. Error dolor consequuntur deleniti aut ut. Sunt cupiditate ipsum ab rerum. Nam ut suscipit nihil.', 'https://lorempixel.com/640/480/?22265', 1, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(133, 9, 'Product Name', 7.30, 50, 'Quia inventore nulla ipsa est exercitationem odit. Ea rerum quia est officia quaerat ullam fuga. Voluptatem illum cupiditate animi voluptates at. Ex facere deleniti vitae tempora.', 'https://lorempixel.com/640/480/?65899', 1, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(134, 4, 'Product Name', 1.76, 85, 'Eius voluptates sed consequatur inventore praesentium. Porro quis est inventore rerum. Esse possimus officiis ipsam natus.', 'https://lorempixel.com/640/480/?54609', 0, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(135, 6, 'Product Name', 5.40, 9, 'Accusamus qui temporibus consequatur dolor. Autem corporis autem ut qui rem amet. Aut at totam est quos ipsum optio in.', 'https://lorempixel.com/640/480/?11499', 1, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(136, 2, 'Product Name', 3.37, 48, 'Vel modi qui delectus quia ut exercitationem. Eos consequatur odio itaque suscipit nesciunt distinctio. Ea quae eligendi nihil et.', 'https://lorempixel.com/640/480/?40253', 0, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(137, 2, 'Product Name', 5.85, 18, 'Aut ratione quo ex eius. Sed in occaecati quo beatae qui. Nihil dolorum omnis delectus. Sint eaque molestiae quia et nostrum.', 'https://lorempixel.com/640/480/?66117', 0, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(138, 8, 'Product Name', 8.92, 22, 'Placeat voluptatem sed omnis harum. Architecto fuga eos nobis repellat velit. Voluptas iusto et excepturi nihil exercitationem quasi necessitatibus maxime.', 'https://lorempixel.com/640/480/?31996', 1, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(139, 3, 'Product Name', 2.24, 22, 'Velit nemo iure ea temporibus. Fugiat et voluptatem minus et explicabo ex impedit vero. Sint impedit voluptates excepturi voluptates ut eligendi. Voluptas ad laudantium nemo quia sint.', 'https://lorempixel.com/640/480/?67075', 1, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(140, 6, 'Product Name', 5.64, 16, 'Dolorum temporibus possimus quidem nesciunt. Repudiandae esse ab omnis rerum. Beatae commodi dolor minus modi voluptates.', 'https://lorempixel.com/640/480/?27079', 0, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(141, 4, 'Product Name', 8.17, 49, 'A quo necessitatibus est eaque minima ullam. Et quia dolorem architecto modi ut qui. Molestiae eos voluptatibus sit doloremque saepe est perferendis. Fugit aperiam veniam ullam qui atque.', 'https://lorempixel.com/640/480/?40340', 1, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(142, 5, 'Product Name', 8.26, 83, 'In incidunt provident voluptas reiciendis rem sit molestias. Est reprehenderit consequatur sed occaecati qui est natus. Id inventore dicta dicta et omnis. Facilis nostrum odit est dicta ad ipsum.', 'https://lorempixel.com/640/480/?67719', 1, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(143, 2, 'Product Name', 7.17, 5, 'Consequatur dolorum aut nam eligendi ad dolor. Sunt quia a incidunt esse. Aspernatur libero porro esse dolorem.', 'https://lorempixel.com/640/480/?64882', 1, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(144, 10, 'Product Name', 2.51, 4, 'Non id ut neque voluptatibus et. Ipsum est ut est incidunt vel. Totam et id asperiores unde sed natus.', 'https://lorempixel.com/640/480/?78060', 0, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(145, 6, 'Product Name', 6.28, 59, 'Magnam rerum rerum iste voluptates qui mollitia. Fuga omnis assumenda blanditiis et perferendis qui. Quo id iste totam consequatur hic vero molestiae et.', 'https://lorempixel.com/640/480/?53291', 0, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(146, 3, 'Product Name', 8.63, 79, 'Ut est est unde itaque laudantium. Quaerat nobis expedita sunt et. A omnis facere recusandae cupiditate eaque a sint. Inventore et numquam quod minima.', 'https://lorempixel.com/640/480/?21909', 1, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(147, 5, 'Product Name', 1.93, 21, 'Quia minus laborum adipisci sint voluptatum nam. Vero quidem qui vitae laudantium aut.', 'https://lorempixel.com/640/480/?48029', 1, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(148, 5, 'Product Name', 4.71, 100, 'Et nihil id odit non soluta qui. Animi autem est unde ad. Quam maiores maxime voluptas non.', 'https://lorempixel.com/640/480/?62995', 1, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(149, 6, 'Product Name', 8.15, 94, 'Consequuntur odit recusandae et aut ipsum provident consequatur ut. Veritatis optio odit et sed suscipit modi vitae. Eos dolorum non omnis placeat.', 'https://lorempixel.com/640/480/?94943', 0, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(150, 7, 'Product Name', 1.73, 63, 'Fugiat nostrum culpa ad velit. Dolores provident praesentium laudantium maiores. Qui nam facere veniam.', 'https://lorempixel.com/640/480/?10571', 0, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(151, 4, 'Product Name', 4.99, 90, 'Dolores et est voluptas cupiditate dolorem. Est tenetur cumque veritatis sit dicta quod eligendi. Rerum esse qui dolor consequatur sapiente cumque. Rem voluptas consequatur esse et.', 'https://lorempixel.com/640/480/?87368', 1, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(152, 7, 'Product Name', 1.97, 2, 'Cumque eveniet quo et. Sed optio quas iure unde omnis aut. Possimus nihil facere sed totam. Ut illum veniam magni necessitatibus suscipit est repellendus.', 'https://lorempixel.com/640/480/?60220', 0, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(153, 8, 'Product Name', 1.22, 68, 'Quo amet autem minus et. Qui enim harum eos dolor dolor incidunt. Voluptatem consequatur consequatur aut voluptatem. Illo quia sit necessitatibus molestiae eum.', 'https://lorempixel.com/640/480/?12977', 0, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(154, 6, 'Product Name', 4.42, 60, 'Nihil est suscipit hic consequatur. Quaerat dicta id qui consequuntur. Similique eos consectetur iure autem qui sit.', 'https://lorempixel.com/640/480/?24190', 0, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(155, 9, 'Product Name', 5.80, 77, 'Deserunt quisquam totam consectetur quod sed quo laboriosam sapiente. Rerum in perferendis beatae quasi hic veritatis. Qui incidunt fugit vero ipsa quaerat sit. Quis autem quod a assumenda suscipit.', 'https://lorempixel.com/640/480/?89798', 1, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(156, 8, 'Product Name', 7.30, 52, 'Similique sint ut et ad aliquid. Quia dolorem autem error laboriosam voluptate eaque. Expedita reiciendis est architecto aut dolor. Sit in voluptatum rerum vel vel delectus.', 'https://lorempixel.com/640/480/?43678', 1, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(157, 3, 'Product Name', 9.13, 3, 'Mollitia et qui eum consequatur. Doloribus nemo et vel. Fuga repellat iste est laborum dolorem molestiae expedita. Numquam natus ipsa sunt et porro ut dicta.', 'https://lorempixel.com/640/480/?25012', 1, '2019-03-22 03:29:43', '2019-05-28 01:09:17'),
(158, 8, 'Product Name', 4.65, 61, 'Est velit at ea est sed in aut qui. Sunt suscipit omnis sunt in ex.', 'https://lorempixel.com/640/480/?95632', 1, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(159, 5, 'Product Name', 5.02, 18, 'Ab aut fuga aliquam enim quo sapiente quisquam. Eius recusandae pariatur nobis illum ducimus doloremque est. Ducimus laboriosam neque temporibus alias sed id pariatur.', 'https://lorempixel.com/640/480/?52928', 1, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(160, 2, 'Product Name', 5.82, 85, 'Sunt harum est similique ratione nostrum ipsa molestiae. Nobis aliquam et molestiae odit asperiores. Rem similique quas sed incidunt eligendi mollitia. Possimus consequatur molestiae est.', 'https://lorempixel.com/640/480/?79423', 1, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(161, 5, 'Product Name', 4.41, 2, 'Perspiciatis necessitatibus consectetur tempora omnis voluptas. Voluptatem dolores et et impedit sint. Beatae ut aut expedita qui consectetur voluptate amet.', 'https://lorempixel.com/640/480/?24737', 0, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(162, 10, 'Product Name', 1.02, 100, 'Quia rem illum perferendis soluta tempora. Qui fuga aut id eaque soluta. Velit in sit enim eos asperiores commodi omnis.', 'https://lorempixel.com/640/480/?75123', 1, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(163, 8, 'Product Name', 6.12, 13, 'Vel est voluptates repudiandae magni quia. Quo qui perferendis veritatis ipsam nihil. Magnam architecto expedita magnam veritatis pariatur totam.', 'https://lorempixel.com/640/480/?87296', 1, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(164, 2, 'Product Name', 4.78, 53, 'Et veritatis eaque sint accusantium maxime consectetur. Neque voluptatibus blanditiis qui.', 'https://lorempixel.com/640/480/?49690', 1, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(165, 3, 'Product Name', 2.52, 72, 'Nostrum repellendus veritatis occaecati aspernatur ratione corporis autem. Culpa placeat quo harum sapiente. Voluptatem ut distinctio necessitatibus sed sit sunt possimus.', 'https://lorempixel.com/640/480/?43786', 0, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(166, 8, 'Product Name', 1.60, 51, 'Quod molestias iusto nihil laudantium. Eos sapiente quas ut dolorem sed deleniti. Illo reiciendis enim iure iusto qui. Nam accusamus impedit laudantium.', 'https://lorempixel.com/640/480/?10297', 0, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(167, 1, 'Product Name', 2.84, 75, 'At aspernatur sit excepturi magnam ut vel corrupti. Quod est autem quis velit eum. Deleniti doloremque minus porro qui fugiat libero. Iste voluptatem vero consequatur nulla.', 'https://lorempixel.com/640/480/?72197', 0, '2019-03-22 03:29:43', '2019-03-22 03:29:43'),
(168, 8, 'Product Name', 2.77, 37, 'Eum molestiae excepturi quas voluptatem dolorem dolorem. Rem ullam sequi eos temporibus. Explicabo provident ab deserunt et.', 'https://lorempixel.com/640/480/?38818', 1, '2019-03-22 03:29:44', '2019-05-27 01:26:10'),
(169, 3, 'Product Name', 0.27, 36, 'Quo qui dicta totam eum. Voluptatem voluptatem iusto repudiandae voluptates similique rerum nam. Cumque est qui expedita eos asperiores fugiat placeat.', 'https://lorempixel.com/640/480/?88328', 1, '2019-03-22 03:29:44', '2019-05-27 01:26:10'),
(170, 10, 'Product Name', 8.33, 74, 'Fugiat in autem iusto illum tempora et necessitatibus illo. Dolorum voluptas cumque odio nulla aspernatur adipisci exercitationem quibusdam.', 'https://lorempixel.com/640/480/?23367', 0, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(171, 2, 'Product Name', 9.03, 68, 'Sapiente quo velit corrupti quia. Earum vel tempora expedita hic nesciunt deserunt minima saepe.', 'https://lorempixel.com/640/480/?67642', 1, '2019-03-22 03:29:44', '2019-06-02 23:58:17'),
(172, 9, 'Product Name', 7.98, 56, 'Totam consectetur delectus eos ipsa vero. Atque voluptatem fugit repellendus officia rerum delectus. Aspernatur et sed veritatis rerum corrupti.', 'https://lorempixel.com/640/480/?90561', 1, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(173, 10, 'Product Name', 7.58, 59, 'Quis quia ea rerum. Et veniam iste aut. Ut aut qui porro ullam.', 'https://lorempixel.com/640/480/?77273', 0, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(174, 7, 'Product Name', 3.55, 40, 'Inventore asperiores neque est pariatur iure accusamus eum. Harum est reprehenderit reiciendis ut est eligendi. Rerum velit enim ut sit neque voluptatem. Ea molestias distinctio sequi et assumenda.', 'https://lorempixel.com/640/480/?99741', 1, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(175, 9, 'Product Name', 9.15, 4, 'Harum eos sunt quod deleniti voluptas sunt. Rem pariatur quam repellendus. A sequi distinctio ratione dicta. Ut mollitia voluptatem vero reiciendis cumque.', 'https://lorempixel.com/640/480/?22728', 0, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(176, 4, 'Product Name', 6.56, 94, 'Ut explicabo dolor in quis repudiandae id. Dolorem qui in officiis sunt fuga atque. Ipsam deleniti nobis deleniti aut et qui sint eum. Inventore voluptates neque fugiat deleniti quis.', 'https://lorempixel.com/640/480/?10949', 1, '2019-03-22 03:29:44', '2019-06-01 07:13:07'),
(177, 6, 'Product Name', 1.06, 89, 'Rem eum quod doloremque architecto accusantium dolorem. Voluptatem architecto quia et voluptate sequi illo. Aliquid et est voluptas eum deserunt est.', 'https://lorempixel.com/640/480/?16882', 1, '2019-03-22 03:29:44', '2019-06-01 08:28:25'),
(178, 9, 'Product Name', 9.36, 87, 'Explicabo cumque necessitatibus ut ut sed quasi dolorem. Tempore et voluptas et. Et corrupti quidem sequi molestiae aut ex sunt. Eligendi alias accusamus et et eligendi sequi error quaerat.', 'https://lorempixel.com/640/480/?33392', 1, '2019-03-22 03:29:44', '2019-06-01 08:28:25'),
(179, 6, 'Product Name', 9.67, 2, 'Rem quasi eum saepe aut voluptas et voluptatem. Sunt deserunt sed velit omnis. Nemo doloremque aliquam impedit. Quod quia voluptatem eum magnam impedit.', 'https://lorempixel.com/640/480/?37019', 0, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(180, 9, 'Product Name', 8.16, 17, 'Ex quasi sunt quia minima totam. Asperiores fugit fuga qui cum alias explicabo iste quia. Itaque omnis aut tempora consequatur eveniet non omnis.', 'https://lorempixel.com/640/480/?15466', 1, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(181, 8, 'Product Name', 7.96, 37, 'Est est atque ipsa ut iusto voluptatem repellendus atque. Natus pariatur ad tempora sunt sapiente hic.', 'https://lorempixel.com/640/480/?85380', 0, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(182, 5, 'Product Name', 1.02, 16, 'Maiores porro accusamus praesentium qui. Consequuntur provident et nesciunt deserunt culpa quasi. Recusandae magni ut eius odio nulla enim consectetur earum. Aut possimus perspiciatis magni.', 'https://lorempixel.com/640/480/?73585', 0, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(183, 4, 'Product Name', 4.65, 99, 'In alias fuga inventore quis est velit. Laborum aut velit excepturi est inventore debitis. Voluptatum ex illum possimus officia nihil. Quas explicabo cum in odio laborum fugit cumque.', 'https://lorempixel.com/640/480/?71169', 1, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(184, 5, 'Product Name', 0.62, 16, 'Expedita labore ullam exercitationem laboriosam ut. Suscipit aut error debitis nulla aliquam nostrum. Corporis eaque est et quidem nisi.', 'https://lorempixel.com/640/480/?33526', 1, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(185, 10, 'Product Name', 2.88, 5, 'Id et dolores ex maxime. Fuga in dolorem facere occaecati vel quibusdam. Dolor nesciunt non praesentium qui. Porro impedit a a cumque aut dolorem. Quaerat sed repellendus occaecati dolor qui aut.', 'https://lorempixel.com/640/480/?67494', 1, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(186, 3, 'Product Name', 2.54, 10, 'Ut est doloremque nam quos. Ducimus fugiat officiis a. Saepe sequi sit optio consequatur nam. Sed corporis iste ea possimus omnis debitis. Deserunt ea dolorem aperiam facilis unde voluptate illo.', 'https://lorempixel.com/640/480/?53308', 0, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(187, 2, 'Product Name', 0.33, 70, 'Amet quo eum maxime eligendi. Blanditiis veritatis eligendi occaecati quas dolorum quasi ab blanditiis. Et unde omnis id.', 'https://lorempixel.com/640/480/?98570', 0, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(188, 7, 'Product Name', 8.74, 86, 'Sint molestiae quas occaecati voluptatem et. Illum doloremque dolorem sit aliquam. Sint enim facilis sint reiciendis. Quibusdam voluptas laboriosam aut voluptatem eveniet et libero.', 'https://lorempixel.com/640/480/?12750', 1, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(189, 5, 'Product Name', 1.96, 6, 'Quidem voluptatem facilis neque quam voluptas est quae. Adipisci non qui ut quo. Sunt minus occaecati officia corrupti illum consequatur voluptas. Autem aut odit voluptates praesentium.', 'https://lorempixel.com/640/480/?22049', 0, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(190, 10, 'Product Name', 3.25, 43, 'Quo et quis dolorum ad perspiciatis. Sed aperiam eos vero qui excepturi inventore. Ducimus voluptatem eaque rerum qui qui.', 'https://lorempixel.com/640/480/?10689', 0, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(191, 1, 'Product Name', 4.28, 74, 'Quo reiciendis quis quidem dolores quo rem. Eius assumenda sed tempora. Eum voluptas officia doloribus dolorem ea. Fugit atque optio ut.', 'https://lorempixel.com/640/480/?32691', 1, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(192, 2, 'Product Name', 8.07, 22, 'Quo sunt quos aperiam fugit. Quasi perspiciatis qui odit molestiae dignissimos veniam sed. Dolorem non est voluptas exercitationem fugit. Ea et consequuntur fugit consectetur.', 'https://lorempixel.com/640/480/?55137', 1, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(193, 5, 'Product Name', 1.59, 54, 'Nulla eum velit excepturi numquam quaerat esse iste voluptatem. Ut dolorem necessitatibus delectus nobis. Perspiciatis tenetur placeat non assumenda. Iusto voluptatibus ut laudantium provident.', 'https://lorempixel.com/640/480/?40656', 1, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(194, 3, 'Product Name', 7.84, 85, 'Delectus omnis ea nemo animi voluptatem quia. Enim molestias veritatis odit consectetur. Et eum sunt nihil adipisci vero. Est molestiae ipsa ea voluptatem vitae eius odio dolorum.', 'https://lorempixel.com/640/480/?62086', 0, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(195, 10, 'Product Name', 4.26, 49, 'Ut quod sit id voluptas. Molestias non perspiciatis aut nihil. Vitae deserunt animi id maiores. Eum ad sed nobis quia velit dicta nostrum molestias.', 'https://lorempixel.com/640/480/?78048', 1, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(196, 9, 'Product Name', 8.40, 86, 'Vero voluptate doloremque omnis voluptas necessitatibus. Animi architecto nemo nobis dolor quos dignissimos reprehenderit. Minima asperiores sed ipsam recusandae.', 'https://lorempixel.com/640/480/?43727', 0, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(197, 8, 'Product Name', 8.63, 36, 'Molestias iure sit sed vitae aut explicabo sapiente. Modi ut dicta dignissimos est voluptas distinctio. Qui quis aut optio. Similique ut non et quas dolorem fugiat autem reprehenderit.', 'https://lorempixel.com/640/480/?17721', 0, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(198, 7, 'Product Name', 4.30, 22, 'Et delectus nam dolore est. Facilis sed soluta rerum. Suscipit dolore nemo ut perferendis sit. Sequi molestiae laborum placeat.', 'https://lorempixel.com/640/480/?93754', 0, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(199, 7, 'Product Name', 7.09, 62, 'Esse quia adipisci voluptate. Qui tempore sit delectus quos architecto quasi. Distinctio commodi quo rem nulla consequatur cupiditate earum quos. Soluta est quia beatae sed vero.', 'https://lorempixel.com/640/480/?36566', 1, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(200, 4, 'Product Name', 6.10, 40, 'Consectetur soluta sit hic placeat. Voluptatem nostrum et delectus nulla. Molestiae facere nostrum rerum ut. Dolor facere quia voluptate possimus quo veniam.', 'https://lorempixel.com/640/480/?10029', 0, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(201, 5, 'Product Name', 3.10, 25, 'Et autem et dignissimos eligendi aliquid. Quibusdam est modi culpa dolorum sed non voluptatum excepturi. Distinctio eaque error et esse est dolores enim nostrum.', 'https://lorempixel.com/640/480/?31535', 0, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(202, 3, 'Product Name', 5.17, 72, 'Commodi nobis pariatur quas totam rem. Eum architecto qui delectus.', 'https://lorempixel.com/640/480/?62359', 0, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(203, 6, 'Product Name', 5.60, 42, 'Doloremque tenetur exercitationem enim et ut voluptas. Quam voluptatem voluptate iste unde vero. Odio officiis officiis tempore qui quam.', 'https://lorempixel.com/640/480/?70475', 1, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(204, 8, 'Product Name', 3.64, 44, 'Consequatur inventore quam qui earum consectetur facilis nisi. Doloribus et eaque nostrum mollitia aut voluptatem. Aliquid expedita enim id et eligendi delectus. Aut error vitae qui fugit qui.', 'https://lorempixel.com/640/480/?47598', 1, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(205, 4, 'Product Name', 9.58, 11, 'Debitis aliquam qui eos cumque eveniet et suscipit. Enim voluptatum sit debitis sed. Velit autem officia non et.', 'https://lorempixel.com/640/480/?17252', 0, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(206, 2, 'Product Name', 9.78, 35, 'Mollitia deleniti ipsa saepe ut sunt nemo aperiam. Qui nihil sed eum molestiae dolorem eaque sed. Rerum nihil est quos nobis.', 'https://lorempixel.com/640/480/?83475', 0, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(207, 5, 'Product Name', 2.11, 35, 'Voluptatem eum rerum est laborum est. Vel voluptatem et id perspiciatis quam. Voluptatem maiores cupiditate quod non. Qui delectus unde et ab.', 'https://lorempixel.com/640/480/?99014', 0, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(208, 4, 'Product Name', 8.28, 43, 'Ipsum pariatur nemo sed voluptas veritatis. Eos enim dicta maxime et. Ducimus impedit omnis sed.', 'https://lorempixel.com/640/480/?23821', 0, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(209, 1, 'Product Name', 4.95, 92, 'Aut excepturi consequatur voluptatem temporibus. Non et est cum qui ipsum veniam tempore. Vitae neque doloremque fuga veniam ut. Eum incidunt omnis vero ipsa fuga.', 'https://lorempixel.com/640/480/?82415', 0, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(210, 2, 'Product Name', 4.56, 65, 'Animi et voluptatum nihil qui perspiciatis magni. Molestiae repellat a fugiat commodi animi sit numquam. Animi totam repudiandae aut. Sequi hic rerum nesciunt cumque.', 'https://lorempixel.com/640/480/?93414', 1, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(211, 5, 'Product Name', 5.80, 42, 'Enim accusamus dicta aut quis id quia. Sint perferendis aliquam eos eum eum mollitia odio. Enim minus odio dignissimos.', 'https://lorempixel.com/640/480/?71482', 0, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(212, 8, 'Product Name', 0.91, 45, 'Quo molestias consequuntur a ea veniam id non. Quaerat omnis sed delectus at et nulla. Corrupti ipsam odio harum est.', 'https://lorempixel.com/640/480/?31105', 1, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(213, 2, 'Product Name', 3.23, 55, 'Qui quis consectetur tempore nulla placeat. Voluptatum dolore quis non sed sit modi. Nihil sapiente aspernatur error dolorum impedit.', 'https://lorempixel.com/640/480/?21237', 0, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(214, 1, 'Product Name', 0.36, 58, 'Laudantium deserunt omnis recusandae harum. Corrupti et eaque aut sapiente itaque non qui.', 'https://lorempixel.com/640/480/?62941', 1, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(215, 1, 'Product Name', 4.42, 46, 'Ipsam ducimus earum laudantium deleniti vel repellendus repellendus. Ut esse nemo ullam quod cum alias. Ea non alias veritatis.', 'https://lorempixel.com/640/480/?50475', 1, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(216, 3, 'Product Name', 8.44, 26, 'Incidunt quidem enim accusamus sit. Qui provident dolorem voluptatibus est. Et sequi impedit corporis hic.', 'https://lorempixel.com/640/480/?78263', 0, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(217, 5, 'Product Name', 5.50, 44, 'Mollitia quod beatae officiis ipsa. Voluptates ipsam quia quaerat quaerat. Quas ab quasi reiciendis. Ratione facilis ex sunt non asperiores eos. Iure animi officiis qui nihil sunt soluta sed.', 'https://lorempixel.com/640/480/?91618', 0, '2019-03-22 03:29:44', '2019-03-22 03:29:44'),
(218, 2, 'Product Name', 5.44, 13, 'Voluptas culpa sit doloremque. Et harum repellat cupiditate occaecati necessitatibus. Exercitationem dolor ratione reprehenderit ratione maxime et.', 'https://lorempixel.com/640/480/?17925', 1, '2019-03-22 03:29:44', '2019-03-22 03:29:44');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_details`
--

CREATE TABLE `product_details` (
  `product_id` bigint(20) NOT NULL,
  `feature` text COLLATE utf8mb4_unicode_ci,
  `screen` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `camera` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ram` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rom` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `battery` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `product_details`
--

INSERT INTO `product_details` (`product_id`, `feature`, `screen`, `camera`, `ram`, `rom`, `weight`, `battery`) VALUES
(119, 'Call, message, video, listen to music', '6.5inches', '1080p', '4GB', '512GB', '208g', 'Li-ion');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'SuperAdmin', '2019-03-19 15:23:22', '2019-03-19 15:23:22'),
(2, 'Admin', '2019-03-19 15:23:22', '2019-05-16 12:05:34'),
(4, 'User', '2019-06-03 09:00:20', '2019-06-03 09:00:20');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `role_user`
--

CREATE TABLE `role_user` (
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(3, 1),
(4, 4);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `sliders`
--

CREATE TABLE `sliders` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '0: slide-show; 1 banner-new; 2: sidebar-left; 3: sidebar-right',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `sliders`
--

INSERT INTO `sliders` (`id`, `user_id`, `name`, `type`, `created_at`, `updated_at`) VALUES
(1, 3, 'Slider show', 0, '2019-06-02 12:21:27', '2019-06-02 12:21:27'),
(2, 3, 'Banner News', 1, '2019-06-02 12:22:06', '2019-06-02 12:22:06');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `slides`
--

CREATE TABLE `slides` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `slider_id` bigint(20) NOT NULL,
  `order` tinyint(4) NOT NULL DEFAULT '0',
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `slides`
--

INSERT INTO `slides` (`id`, `user_id`, `slider_id`, `order`, `image`, `url`, `status`, `created_at`, `updated_at`) VALUES
(1, 3, 1, 4, '05_04_2019_05_59_38_800-300.png', 'uploads/slides/05_04_2019_05_59_38_800-300.png', 1, '2019-06-02 12:21:27', '2019-06-02 12:21:27'),
(2, 3, 1, 2, '14_11_2017_09_39_38_iphone-du-phong-800-300.png', 'uploads/slides/14_11_2017_09_39_38_iphone-du-phong-800-300.png', 1, '2019-06-02 12:21:27', '2019-06-02 12:21:27'),
(3, 3, 1, 3, '30_04_2019_13_24_57_800-300.png', 'uploads/slides/30_04_2019_13_24_57_800-300.png', 1, '2019-06-02 12:21:27', '2019-06-02 12:21:27'),
(4, 3, 1, 1, '31_03_2019_23_53_00_800-300.png', 'uploads/slides/31_03_2019_23_53_00_800-300.png', 1, '2019-06-02 12:21:27', '2019-06-02 12:21:27'),
(5, 3, 2, 1, '14_11_2017_09_39_38_iphone-du-phong-800-300.png', 'uploads/slides/14_11_2017_09_39_38_iphone-du-phong-800-300.png', 1, '2019-06-02 12:22:06', '2019-06-02 12:22:06'),
(6, 3, 2, 3, '30_04_2019_13_24_57_800-300.png', 'uploads/slides/30_04_2019_13_24_57_800-300.png', 1, '2019-06-02 12:22:06', '2019-06-02 12:22:06'),
(7, 3, 2, 1, '31_03_2019_23_53_00_800-300.png', 'uploads/slides/31_03_2019_23_53_00_800-300.png', 1, '2019-06-02 12:22:06', '2019-06-02 12:22:06');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(3, 'toan', 'toannguyen75641@gmail.com', NULL, '$2y$10$eBF/dvAW5QH/Mthqwi3yF.IAehIn7rp9JtTrEXWO/PTE9FSgP512S', NULL, '2019-05-29 03:44:29', '2019-05-31 10:24:14'),
(4, 'duc toan', 'toannd52@wru.vn', NULL, '$2y$10$TvTn5fmgXzh/6/jEdAdqvOD4OunuVymnm.0vD6iWQT8JAmerkVcMa', NULL, '2019-06-03 00:27:46', '2019-06-03 09:14:22');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `carts_product_id_foreign` (`product_id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Chỉ mục cho bảng `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Chỉ mục cho bảng `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customers_email_unique` (`email`);

--
-- Chỉ mục cho bảng `image_albums`
--
ALTER TABLE `image_albums`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Chỉ mục cho bảng `order_details`
--
ALTER TABLE `order_details`
  ADD KEY `order_id` (`order_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `permission_role`
--
ALTER TABLE `permission_role`
  ADD KEY `permission_id` (`permission_id`),
  ADD KEY `role_id` (`role_id`);

--
-- Chỉ mục cho bảng `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Chỉ mục cho bảng `product_details`
--
ALTER TABLE `product_details`
  ADD KEY `product_id` (`product_id`);

--
-- Chỉ mục cho bảng `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `role_user`
--
ALTER TABLE `role_user`
  ADD KEY `role_id` (`role_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Chỉ mục cho bảng `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Chỉ mục cho bảng `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`),
  ADD KEY `slider_id` (`slider_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `carts`
--
ALTER TABLE `carts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT cho bảng `customers`
--
ALTER TABLE `customers`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `image_albums`
--
ALTER TABLE `image_albums`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;

--
-- AUTO_INCREMENT cho bảng `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT cho bảng `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=219;

--
-- AUTO_INCREMENT cho bảng `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `slides`
--
ALTER TABLE `slides`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `carts`
--
ALTER TABLE `carts`
  ADD CONSTRAINT `carts_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
  ADD CONSTRAINT `carts_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Các ràng buộc cho bảng `contacts`
--
ALTER TABLE `contacts`
  ADD CONSTRAINT `contacts_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`);

--
-- Các ràng buộc cho bảng `image_albums`
--
ALTER TABLE `image_albums`
  ADD CONSTRAINT `image_albums_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Các ràng buộc cho bảng `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`);

--
-- Các ràng buộc cho bảng `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `order_details_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `order_details_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Các ràng buộc cho bảng `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_ibfk_1` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`),
  ADD CONSTRAINT `permission_role_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Các ràng buộc cho bảng `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);

--
-- Các ràng buộc cho bảng `product_details`
--
ALTER TABLE `product_details`
  ADD CONSTRAINT `product_details_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Các ràng buộc cho bảng `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  ADD CONSTRAINT `role_user_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Các ràng buộc cho bảng `sliders`
--
ALTER TABLE `sliders`
  ADD CONSTRAINT `sliders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Các ràng buộc cho bảng `slides`
--
ALTER TABLE `slides`
  ADD CONSTRAINT `slides_ibfk_1` FOREIGN KEY (`slider_id`) REFERENCES `sliders` (`id`),
  ADD CONSTRAINT `slides_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
