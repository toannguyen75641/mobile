<?php

use Faker\Generator as Faker;

$factory->define(App\Model\Order::class, function (Faker $faker) {
    return [
        'customer_id' => function() {
        	return App\Model\Customer::inRandomOrder()->first()->id;
        },
        'amount' => rand(10,1000),
        'state' => $faker->numberBetween($min = 0, $max = 2),
        'created_at' => new DateTime,
    ];
});
