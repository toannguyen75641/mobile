<?php

use Faker\Generator as Faker;

$factory->define(App\Model\OrderDetail::class, function (Faker $faker) {
	$price = rand(10,100);
	$quantity = rand(1,100);

    return [
        'order_id' => rand(101,150),
        'product_id' => function() {
        	return App\Model\Product::inRandomOrder()->first()->id;
        },
        'price' => $price,
        'quantity' => $quantity,
        'amount' => $price * $quantity,
    ];
});
