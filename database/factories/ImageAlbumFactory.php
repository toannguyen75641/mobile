<?php

use Faker\Generator as Faker;

$factory->define(App\Model\ImageAlbum::class, function (Faker $faker) {
    return [
        'product_id' => function() {
            return App\Product::create()->id;
        },
        'image' => $faker->imageUrl($width = 640, $height = 480),
        'url' => $faker->imageUrl($width = 640, $height = 480)
    ];
});