<?php

use Faker\Generator as Faker;

$factory->define(App\Model\Customer::class, function (Faker $faker) {

	$gender = $faker->randomElement(['male', 'female']);

    return [
        'name' => $faker->name($gender),
        'phone' => $faker->e164PhoneNumber,
        'email' => $faker->unique()->safeEmail,
        'gender' => $gender,
        'birthday' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'address' => $faker->address,
        'created_at' => new DateTime,
        'updated_at' => new DateTime
    ];
});
