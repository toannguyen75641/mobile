$('form').submit(function() {
	var error = 0;
	var order = $(".order");
	var title = $(".title");
	var description = $(".description");
	$('.required').remove();
	$.each(order, function(i, val){
		if($(val).val() == 0) {
			$(val).after('<p class="required">The order field is required.</p>');
			error = 1;
		}
	});

	if(error == 0) {
		return true;
	}
	return false;
});