function appendWidgetElement(current_cart_after_delete, totalQty = 0, totalPrice = 0, subtotal = 0) {
	$('.product-widget').remove();
	$('.cart-summary').empty();
	$('#quantity').empty();
	$.each(current_cart_after_delete, function(i, val) {
		totalQty += val.qty;
		totalPrice = val.price * val.qty;
		subtotal += totalPrice;
		var str_product = 	'<div class="product-widget">' +
								'<div class="product-img"><img src="' + val.image + '"></div>' +
								'<div class="product-body">' +
									'<h3 class="product-name"><a>' + val.name + '</a></h3>' +
									'<h4 class="product-price"><span class="qty">'+ val.qty +'x</span>'+ val.price +'$</h4>' +
								'</div>' +
								'<button class="delete" data-id='+ val.product_id +'><i class="fa fa-close"></i></button>' +
							'</div>';
		$('.cart-list').append(str_product);
	});		
	var str_total = '<small>'+ totalQty +' Item(s) selected</small>' +
				'<h5>SUBTOTAL: $'+ subtotal +'</h5>';
	$('#quantity').append(totalQty);
	$('.cart-summary').append(str_total);
}

function appendCartElement(current_cart_after_delete, totalQty = 0, totalPrice = 0, subtotal = 0) {
	$('.product-cart ').remove();
	$('.order-price').empty();
	$.each(current_cart_after_delete, function(i, val) {
		totalQty += val.qty;
		totalPrice = val.price * val.qty;
		subtotal += totalPrice;
		var str_product = 	'<div class="product product-cart">' +
							'<div class="col-md-2 image">' +
								'<img src="' + val.image + '">' +
							'</div>' +
							'<div class="col-md-10 product-body">' +
								'<div class="col-md-8">' +
									'<h3 class="product-name">' + val.name +'</h3>' +
									'<p class="action">' +
										'<a class="delete btn-item-delete btn-link" data-id="'+ val.product_id +'">Remove</a>' +
									'</p>' +
								'</div>' +
								'<div class="col-md-2">' +
									'<h4 class="product-price">$'+ val.price +'</h4>' +
								'</div>' +
								'<div class="col-md-2 box-qty">' +
									'<div class="input-number">' +
										'<span class="qty-up" data-id="'+ val.product_id +'">+</span>' +
										'<span class="qty-down" data-id="'+ val.product_id +'">-</span>' +
										'<input type="number" class="number-qty" value="'+ val.qty +'" disabled="disabled">' +
										'<input type="hidden" class="stock-qty" value="'+ val.stock_qty +'">' +
									'</div>' +
								'</div>' +
							'</div>' +
						'</div>';
		$('#cart').append(str_product);
	});
	if(typeof increaseQty != "undefined" && typeof reduceQty != "undefined") {
		increaseQty(current_cart_after_delete);
		reduceQty(current_cart_after_delete);
	}
	var str_total = '<strong>$'+ subtotal +'</strong>';
	$('.order-price').append(str_total);	
}

function deleleElement(id, cart, current_cart) {
	$.each(cart, function(i, val) {
		if(typeof val !== "undefined") {
			if(id == val.product_id) {
				cart.splice(i, 1);
			}
		}
	});
	if(!$.isEmptyObject(cart)) {
		if(typeof current_cart === "undefined" || current_cart === "") {
			var current_cart_after_delete = cart;
		}
		else {
			Cookies.set('cart', cart, { expires: 1 });
			var current_cart_after_delete = JSON.parse(Cookies.get('cart'));
		}
		console.log(current_cart_after_delete);
		appendWidgetElement(current_cart_after_delete);
		appendCartElement(current_cart_after_delete);
	}
	else {
		Cookies.remove('cart');
		current_cart_after_insert = new Array;
		//widget
		$('#quantity').remove();
		$('.cart-dropdown').remove();
		//cart
		$('#content').remove();
	}
}

$(document).ready(function() {
	$(document).on('click', '.add-to-cart-btn', function() {
		var id = $(this).attr('data-id');
		var name = $(this).attr('data-name');
		var price = $(this).attr('data-price');
		var image = $(this).attr('data-image');
		var stock_qty = $(this).attr('data-qty');
		var qty = $('#qty').val();
		var loader = $("<div>").addClass("loader");
		var loader_img = $("<div>").addClass('loader_img');

		var current_cart = Cookies.get('cart');
		if(typeof current_cart == 'undefined' || current_cart === "") {
			var cart = new Array;
			if(typeof current_cart_after_insert != "undefined") {
				cart = current_cart_after_insert;
			}
			else if(!$.isEmptyObject(cart_db)){
				cart = cart_db;
			}
		}
		else {
			var cart = JSON.parse(current_cart);
		}
		var position_key = false;
		$.each(cart, function(i, val) {
			if(val.product_id == id) {
				position_key = i;
				return(i =! position_key);
			}
		});

		if(position_key !== false) {
			if(cart[position_key]['qty'] < cart[position_key]['stock_qty']) {
				cart[position_key]['qty'] += parseInt(qty);
			}
		}
		else {
			cart.push({'product_id' : parseInt(id), 'image' : image, 'name' : name, 'price' : parseFloat(price) , 'qty' : parseInt(qty), 'stock_qty' : parseInt(stock_qty)});
		}
		console.log(cart);
		var totalQty = 0;
		var totalPrice = 0;
		var subtotal = 0;
		$.each(cart, function(i, val) {
			totalQty += val.qty;
			totalPrice = val.price * val.qty;
			subtotal += totalPrice;
		});

		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			type: "put",
			url: url,
			data: {'totalQty' : totalQty, 'subtotal' : subtotal, 'cart' : cart},
			beforeSend: function() {
				$("body").append(loader);
				$("body").append(loader_img);
			},
			success: function(res) {
		    	if($.isEmptyObject(res.cart)) {
		    		Cookies.set('cart', cart, { expires: 1 });
		    	}
		    	else {
					Cookies.set('cart', res.cart, { expires: 1 });
					current_cart_after_insert = res.cart;
		    		console.log('current_cart_after_insert', current_cart_after_insert);
				}
				$('div').removeClass("loader");
		    	$('div').removeClass("loader_img");
				$('.header-ctn').html(res.view);
			}
		});
	});

	$(document).on('click', '.delete', function() {
		var id = $(this).attr('data-id');
		var current_cart = Cookies.get('cart');
		var loader = $("<div>").addClass("loader");
		var loader_img = $("<div>").addClass('loader_img');

		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			type: "delete",
			url: urlDel,
			data: {'id': id},
			beforeSend: function() {
				$("body").append(loader);
				$("body").append(loader_img);
			},
			success: function(res) {
				if(typeof current_cart === "undefined" || current_cart === "") {
					deleleElement(id, res, current_cart);
				}
				else {
					var cart = JSON.parse(current_cart);
					deleleElement(id, cart, current_cart);
				}
				$('div').removeClass("loader");
		    	$('div').removeClass("loader_img");
			}
		});
	});
});