<?php

namespace App\Policies;

use App\Model\User;
use App\Model\Order;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderPolicy
{
    use HandlesAuthorization;

    public function getPermission($user, $permission_id) {
        foreach($user->roles as $role) {
            foreach($role->permissions as $permission) {
                if($permission->id == $permission_id) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Determine whether the user can view the order.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Model\Order  $order
     * @return mixed
     */
    public function view(User $user, Order $order)
    {
        //
    }

    /**
     * Determine whether the user can create orders.
     *
     * @param  \App\Model\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the order.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Model\Order  $order
     * @return mixed
     */
    public function update(User $user)
    {
        return $this->getPermission($user, 16);
    }

    /**
     * Determine whether the user can delete the order.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Model\Order  $order
     * @return mixed
     */
    public function delete(User $user)
    {
        return $this->getPermission($user, 17);
    }

    /**
     * Determine whether the user can restore the order.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Model\Order  $order
     * @return mixed
     */
    public function restore(User $user, Order $order)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the order.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Model\Order  $order
     * @return mixed
     */
    public function forceDelete(User $user, Order $order)
    {
        //
    }
}
