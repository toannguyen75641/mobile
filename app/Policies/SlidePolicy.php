<?php

namespace App\Policies;

use App\Model\User;
use App\Model\Slide;
use Illuminate\Auth\Access\HandlesAuthorization;

class SlidePolicy
{
    use HandlesAuthorization;

    public function getPermission($user, $slide, $permission_id) {
        foreach($user->roles as $role) {
            foreach($role->permissions as $permission) {
                if($permission->id == $permission_id) {
                    return true;
                }
            }
        }
        if($slide) {
            return $user->id == $slide->user_id;
        }
        else {
            return false;
        }
    }

    public function view(User $user, Slide $slide)
    {

    }

    public function create(User $user)
    {
        return $this->getPermission($user, null, 12);
    }

    public function update(User $user, Slide $slide)
    {
        return $this->getPermission($user, $slide, 13);
    }

    public function delete(User $user, Slide $slide)
    {
        return $this->getPermission($user, $slide, 14);
    }

    public function restore(User $user, Slide $slide)
    {
        //
    }

    public function forceDelete(User $user, Slide $slide)
    {
        //
    }
}
