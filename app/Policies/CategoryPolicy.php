<?php

namespace App\Policies;

use App\Model\User;
use App\Model\Category;
use Illuminate\Auth\Access\HandlesAuthorization;

class CategoryPolicy
{
    use HandlesAuthorization;

    public function getPermission($user, $permission_id) {
        foreach($user->roles as $role) {
            foreach($role->permissions as $permission) {
                if($permission->id == $permission_id) {
                    return true;
                }
            }
        }
        return false;
    }

    public function view(User $user, Category $category)
    {
        //
    }

    public function create(User $user)
    {
        return $this->getPermission($user, 1);
    }

    public function update(User $user)
    {
        return $this->getPermission($user, 2);
    }

    public function delete(User $user)
    {
        return $this->getPermission($user, 3);
    }

    public function restore(User $user, Category $category)
    {
        //
    }

    public function forceDelete(User $user, Category $category)
    {
        //
    }
}
