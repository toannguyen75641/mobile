<?php

namespace App\Policies;

use App\Model\User;
use App\Model\Role;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    public function getPermission($user, $permission_id) {
        foreach($user->roles as $role) {
            foreach($role->permissions as $permission) {
                if($permission->id == $permission_id) {
                    return true;
                }
            }
        }
        return false;
    }

    public function view(User $user, Role $role)
    {
        //
    }

    public function create(User $user)
    {
        return $this->getPermission($user, 9);
    }

    public function update(User $user)
    {
        return $this->getPermission($user, 10);
    }

    public function delete(User $user)
    {
        return $this->getPermission($user, 11);
    }

    public function restore(User $user, Role $role)
    {
        //
    }

    public function forceDelete(User $user, Role $role)
    {
        //
    }
}
