<?php

namespace App\Policies;

use App\Model\User;
use App\Model\Product;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;

    public function getPermission($user, $permission_id) {
        foreach($user->roles as $role) {
            foreach($role->permissions as $permission) {
                if($permission->id == $permission_id) {
                    return true;
                }
            }
        }
        return false;
    }

    public function view(User $user, Product $product)
    {
        //
    }

    public function create(User $user)
    {   
        return $this->getPermission($user, 4);
    }

    public function update(User $user)
    {
        return $this->getPermission($user, 5);
    }

    public function delete(User $user)
    {
        return $this->getPermission($user, 6);
    }

    public function restore(User $user, Product $product)
    {
        //
    }

    public function forceDelete(User $user, Product $product)
    {
        //
    }
}
