<?php

namespace App\Policies;

use App\Model\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function getPermission($user, $permission_id) {
        foreach($user->roles as $role) {
            foreach($role->permissions as $permission) {
                if($permission->id == $permission_id) {
                    return true;
                }
            }
        }
        return false;
    }

    public function view(User $user, User $model)
    {
        //
    }

    public function create(User $user)
    {
        
    }

    public function update(User $user)
    {
        return $this->getPermission($user, 7);
    }

    public function delete(User $user)
    {
        return $this->getPermission($user, 8);
    }

    public function restore(User $user, User $model)
    {
        //
    }

    public function forceDelete(User $user, User $model)
    {
        //
    }
}
