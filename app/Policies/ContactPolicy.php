<?php

namespace App\Policies;

use App\Model\User;
use App\Model\Contact;
use Illuminate\Auth\Access\HandlesAuthorization;

class ContactPolicy
{
    use HandlesAuthorization;

    public function getPermission($user, $permission_id) {
        foreach($user->roles as $role) {
            foreach($role->permissions as $permission) {
                if($permission->id == $permission_id) {
                    return true;
                }
            }
        }
        return false;
    }

    public function view(User $user, Contact $contact)
    {
        //
    }

    /**
     * Determine whether the user can create contacts.
     *
     * @param  \App\Model\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the contact.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Model\Contact  $contact
     * @return mixed
     */
    public function update(User $user, Contact $contact)
    {
        //
    }

    /**
     * Determine whether the user can delete the contact.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Model\Contact  $contact
     * @return mixed
     */
    public function delete(User $user)
    {
        return $this->getPermission($user, 15);
    }

    /**
     * Determine whether the user can restore the contact.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Model\Contact  $contact
     * @return mixed
     */
    public function restore(User $user, Contact $contact)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the contact.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Model\Contact  $contact
     * @return mixed
     */
    public function forceDelete(User $user, Contact $contact)
    {
        //
    }
}
