<?php

namespace App\Policies;

use App\Model\User;
use App\Model\Slider;
use Illuminate\Auth\Access\HandlesAuthorization;

class SliderPolicy
{
    use HandlesAuthorization;

    public function getPermission($user, $slider, $permission_id) {
        foreach($user->roles as $role) {
            foreach($role->permissions as $permission) {
                if($permission->id == $permission_id) {
                    return true;
                }
            }
        }
        if($slider) {
            return $user->id == $slider->user_id;
        }
        else {
            return false;
        }
    }

    public function view(User $user, Slider $slider)
    {
        //
    }

    public function create(User $user)
    {
        return $this->getPermission($user, null, 12);
    }

    public function update(User $user, Slider $slider)
    {
        return $this->getPermission($user, $slider, 13);
    }

    public function delete(User $user, Slider $slider)
    {
        return $this->getPermission($user, $slider, 14);
    }

    public function restore(User $user)
    {
        //
    }

    public function forceDelete(User $user, Slider $slider)
    {
        //
    }
}
