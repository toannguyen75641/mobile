<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductDetail extends Model
{
	public $timestamps = false;

    protected $fillable = [
        'product_id', 'feature', 'screen', 'camera', 'ram', 'rom', 'weight', 'battery'
    ];

    public function getByProduct($id) {
    	return $this->where('product_id', $id)->first();
    }
}
