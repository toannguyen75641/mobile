<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    protected $fillable = [
        'name', 'category_id', 'price', 'quantity', 'description', 'image', 'status', 'created_at', 'updated_at'
    ];

    public function customers() {
        return $this->belongsToMany('App\Model\Customer', 'carts');
    }

    public function getAll() {
        return $this->leftJoin('categories', 'category_id', '=', 'categories.id')
                    ->select('products.*', 'categories.name as category_name', 'categories.id as category_id');
    }

    public function getByCategory($id) {
        return $this->getAll()
                    ->orderBy('price', 'ASC')
                    ->where('products.category_id', $id);
    }

    public function getById($id) {
        return $this->getAll()
                    ->where('products.id', $id)
                    ->first();
    }

    public function getNewProducts() {
    	return $this->getAll()
                    ->where('status', 1)
                    ->where('quantity', '>', 0)
		            ->orderBy('created_at', 'DESC');
    }

    public function getTopProducts() {
       	return $this->leftJoin('order_details', 'order_details.product_id', '=', 'products.id')
                    ->leftJoin('categories', 'category_id', '=', 'categories.id')
		            ->select('products.*', 'categories.name as category_name', DB::raw('count(order_details.product_id) as count_product'))
                    ->where('status', 1)
                    ->where('products.quantity', '>', 0)
		            ->groupBy('product_id')
		            ->orderBy('count_product', 'DESC');
    }

}
