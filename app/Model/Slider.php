<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = [
    	'user_id', 'name', 'type'
    ];

    public function getAll() {
    	return $this->leftJoin('users', 'user_id', '=', 'users.id')
        			->select('sliders.*', 'users.name as user_name', 'sliders.id as slider_id');
    }

    public function getSliders() {
        return $this->leftJoin('slides', 'slider_id', '=', 'sliders.id')
		            ->select('sliders.*', 'order', 'url')
		            ->orderBy('slides.order', 'ASC')
		            ->get();
    }

    public function getById($id) {
    	return $this->where('id', $id)->first();
    }
}
