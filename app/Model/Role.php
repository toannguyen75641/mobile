<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
 	protected $fillable = [
        'name'
    ];

	public function permissions() {
		return $this->belongsToMany('App\Model\Permission');
	}

    public function getById($id) {
    	return $this->where('id', $id)->first();
    }
}
