<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    protected $fillable = [
    	'user_id', 'slider_id', 'order', 'image', 'url', 'title', 'description', 'status'
    ];

    public function getAll() {
    	return $this->leftJoin('users', 'user_id', '=', 'users.id')
		            ->leftJoin('sliders', 'slider_id', '=', 'sliders.id')
		            ->select('slides.*', 'users.name as user_name', 'sliders.id as slider_id', 'sliders.name as slider_name');
    }

    public function getById($id) {
    	return $this->where('id', $id)->first();
    }

    public function getBySlider($id) {
    	return $this->where('slider_id', $id)->get();
    }
}
