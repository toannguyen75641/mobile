<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ImageAlbum extends Model
{
    public $timestamps = false;

    protected $fillable = [
    	'product_id', 'image', 'url'
    ];

    public function getByProduct($id) {
    	return $this->where('product_id', $id)->get();
    }
}
