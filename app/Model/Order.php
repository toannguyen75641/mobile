<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{	
    protected $fillable = [
    	'customer_id', 'amount', 'state'
    ];

    public function getAll() {
    	return $this->leftJoin('customers', 'customer_id', '=', 'customers.id')
            		->select('orders.*', 'customers.name as customer_name', 'customers.id as customer_id');
    }

    public function getById($id) {
    	return $this->getAll()
    				->where('orders.id', $id);
    }

    public function getByCustomer($id) {
    	return $this->where('customer_id', $id)->get();
    }
}
