<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    
    protected $guard = 'admin';
    
    protected $fillable = [
        'name', 'email', 'password', 'permission'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles() {
        return $this->belongsToMany('App\Model\Role');
    }

    public function is($role_name) {
        foreach($this->roles()->get() as $role) {
            if($role->name == $role_name) {
                return true;
            }
        }
        return false;
    }

    public function getById($id) {
        return $this->where('id', $id)->first();
    }

    public function getMail() {
        return $this->select('email')->get()->toArray();
    }
}
