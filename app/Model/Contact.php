<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
	const UPDATED_AT = null;
	
    protected $fillable = [
    	'customer_id', 'name', 'email', 'subject', 'message'
    ];
}
