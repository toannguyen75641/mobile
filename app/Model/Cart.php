<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    const UPDATED_AT = null;

    protected $fillable = [
    	'customer_id', 'product_id', 'qty', 'price', 'image', 'name', 'stock_qty'
    ];

    public function getAll() {
        return $this->leftJoin('customers', 'carts.customer_id', '=', 'customers.id')
                    ->select('carts.*', 'customers.name as customer_name');
    }

    public function getByCustomer($id) {
    	return $this->getAll()
			        ->where('customer_id', $id)
			        ->get();
    }

    public function getColByCustomer($id) {
        return $this->select('product_id', 'image', 'name', 'price', 'qty', 'stock_qty')
                    ->where('customer_id', $id)
                    ->get();
    }
}
