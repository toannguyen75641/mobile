<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    public $timestamps = false;

    protected $fillable = [
    	'order_id', 'product_id', 'quantity', 'price', 'amount'
    ];

    public function getByOrderId($id) {
    	return $this->leftJoin('products', 'product_id', '=', 'products.id')
		            ->select('order_details.*', 'products.name as product_name', 'products.id as product_id')
		            ->where('order_id', $id);
    }
}
