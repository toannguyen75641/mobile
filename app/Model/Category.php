<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Category extends Model
{
    protected $fillable = [
        'name', 'description',
    ];

    public function getCategories() {
        return $this->select('categories.*', DB::raw('count(products.category_id) as count'))
		            ->leftJoin('products', 'categories.id', '=', 'products.category_id')
        			->where('products.status', 1)
                    ->where('products.quantity', '>', 0)
		            ->groupBy('products.category_id');
    }

    public function getById($id) {
    	return $this->where('id', $id)->first();
    }
}
