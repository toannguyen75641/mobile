<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    public function boot()
    {
        $this->registerPolicies();

        Gate::before(function ($user, $ability) {
            if ($user->is('SuperAdmin')) {
                return true;
            }
        });

        Gate::resource('products', 'App\Policies\ProductPolicy');

        Gate::resource('categories', 'App\Policies\CategoryPolicy');

        Gate::resource('users', 'App\Policies\UserPolicy');

        Gate::resource('roles', 'App\Policies\RolePolicy');

        Gate::resource('sliders', 'App\Policies\SliderPolicy');

        Gate::resource('slides', 'App\Policies\SlidePolicy');

        Gate::resource('contacts', 'App\Policies\ContactPolicy');

        Gate::resource('orders', 'App\Policies\OrderPolicy');


    }
}
