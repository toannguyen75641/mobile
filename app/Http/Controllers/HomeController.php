<?php

namespace App\Http\Controllers;

use App\Model\Category;
use App\Model\Product;
use App\Model\Slider;
use App\Model\Cart;
use App\Model\ImageAlbum;
use App\Model\ProductDetail;
use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    public function __construct(Category $category, Product $product, Slider $slider, ImageAlbum $imageAlbum, Cart $cart, ProductDetail $productDetail) {
        parent::__construct($category, $cart);
        $this->product = $product;
        $this->slider = $slider;
        $this->imageAlbum = $imageAlbum;
        $this->productDetail = $productDetail;
    }

    public function index()
    {
        $categories = $this->category->getCategories();
        $newProducts = $this->product->getNewProducts()->paginate(9);
        $topProducts = $this->product->getTopProducts()->paginate(9);
        $newGroupProducts = $newProducts->split(3);
        $topGroupProducts = $topProducts->split(3);
        $sliders = $this->slider->getSliders();
        $totalQty = $this->countCart();
        $cartData = $this->cookieData();
        $subtotal = $this->subtotal();
        $cart = $this->getCartByCustomer();
        return view('client.home', compact('newProducts', 'topProducts', 'topGroupProducts', 'newGroupProducts', 'sliders', 'totalQty', 'cartData', 'subtotal', 'categories', 'cart'));
    }

    public function store() 
    {
        $products = $this->product->getAll()->orderBy('price', 'ASC')->paginate(9);
        $categories = $this->category->getCategories()->get();
        $topProducts = $this->product->getTopProducts()->paginate(3);
        $totalQty = $this->countCart();
        $cartData = $this->cookieData();
        $subtotal = $this->subtotal();
        $cart = $this->getCartByCustomer();
        return view('client.store', compact('categories', 'products', 'topProducts', 'totalQty', 'cartData', 'subtotal', 'cart'));
    }

    public function category($id) 
    {
        $products = $this->product->getByCategory($id)->where('status', 1)->where('quantity', '>', 0)->paginate(9);
        $categories = $this->category->getCategories()->get();
        $topProducts = $this->product->getTopProducts()->paginate(3);
        $totalQty = $this->countCart();
        $cartData = $this->cookieData();
        $subtotal = $this->subtotal();
        $cart = $this->getCartByCustomer();
        return view('client.store', compact('categories', 'products', 'topProducts', 'totalQty', 'cartData', 'subtotal', 'id', 'cart'));
    }

    public function filter(Request $request) 
    {
         if($request->ajax()) {
            $keyword = $request['keyword'];
            $cate = $request['cate'];
            $id = $request['id'];
            $products = $this->getFilter($request);
            if(!empty($keyword)) {
                $products = $products->where('products.name', 'like', '%'.$keyword.'%');
            }
            if(!empty($cate)) {
                $products = $products->where('category_id', $cate);
            }
            if($id) {
                $products = $products->where('category_id', $id);
            }
            $products = $products->paginate(9);
            return view('client.filter', compact('products'));
        }
    }

    public function product(Request $request, $id) 
    {
        $categories = $this->category->all();
        $product = $this->product->getById($id);
        $imageAlbums = $this->imageAlbum->getByProduct($id);
        $totalQty = $this->countCart();
        $cartData = $this->cookieData();
        $subtotal = $this->subtotal();
        $cart = $this->getCartByCustomer();
        $productDetail = $this->productDetail->getByProduct($id);
        return view('client.product', compact('product', 'imageAlbums', 'totalQty', 'cartData', 'subtotal', 'categories', 'cart', 'productDetail'));
    }

    public function search(Request $request) {
        $keyword = $request['keyword'];
        $cate = $request['category'];
        $products = $this->product->getAll()->orderBy('price', 'ASC');
        if(!empty($keyword)) {
            $products = $products->where('products.name', 'like', '%'.$keyword.'%');
        }
        if(!empty($cate)) {
            $products = $products->where('category_id', $cate);
        }
        if(empty($cate) && empty($keyword)) {
            return redirect()->route('home');
        }
        $products = $products->paginate(9);
        $categories = $this->category->getCategories()->get();
        $topProducts = $this->product->getTopProducts()->paginate(3);
        $totalQty = $this->countCart();
        $cartData = $this->cookieData();
        $subtotal = $this->subtotal();
        $cart = $this->getCartByCustomer();
        return view('client.store', compact('categories', 'products', 'topProducts', 'totalQty', 'cartData', 'subtotal', 'keyword', 'cate', 'cart'));
    }

}
