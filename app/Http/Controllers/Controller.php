<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct($category, $cart) {
        $this->category = $category;
        $this->cart = $cart;
    }

    public function getFilter(Request $request) {
        // dd($_POST);
    	$sort = $request['sort'];
        $priceMax = $request['priceMax'];
        $priceMin = $request['priceMin'];
        $collection  = $this->product->getAll();
        if(isset($sort)) {
            $collection = $collection->orderBy('price', $sort);
        }
        if(!empty($priceMin) || !empty($priceMax)) {
            if($priceMax < $priceMin) {
                $temp = $priceMin;
                $priceMin = $priceMax;
                $priceMax = $temp;
            }
            $collection = $collection->whereBetween('price', [$priceMin, $priceMax]);
        }
        return $collection;
    }

    public function cookieData() {
        $cartData = null;
        if(isset($_COOKIE['cart']) && !Auth::guard()->check()) {
            $cartData = json_decode($_COOKIE['cart'], true);
        }
        if(Auth::guard()->check()) {
            $id = Auth::user()->id;
            $cartData = $this->cart->getByCustomer($id)->toArray();
        }
        // dd($cartData);
        return $cartData;
    }

    public function countCart() {
        $totalQty = null;
        if(isset($_COOKIE['cart']) || Auth::guard()->check()) {
            $cartData = $this->cookieData();
            foreach($cartData as $cartItem) {
                $totalQty += $cartItem['qty'];
            }
        }
        return $totalQty;
    }

    public function subtotal() {
        $totalQty = null;
        $totalPrice = null;
        $subtotal = null;
        if(isset($_COOKIE['cart']) || Auth::guard()->check()) {
            $cartData = $this->cookieData();
            foreach($cartData as $cartItem) {
                $totalQty += $cartItem['qty'];
                $totalPrice = $cartItem['price'] * $cartItem['qty'];
                $subtotal += $totalPrice;
            }
        }
        return $subtotal;
    }
    
    public function getCartByCustomer() {
        if(Auth::guard()->check()) {
            $id = Auth::user()->id;
            $cart = $this->cart->getColByCustomer($id);
        }
        else {
            $cart = json_encode(array());
        }
        return $cart;
    }   
}
