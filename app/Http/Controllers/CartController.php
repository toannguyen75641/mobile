<?php

namespace App\Http\Controllers;

use App\Model\Category;
use App\Model\Cart;
use App\Model\Product;
use Illuminate\Http\Request;
use Auth;

class CartController extends Controller
{
    public function __construct(Category $category, Cart $cart, Product $product) {
        parent::__construct($category, $cart);
        $this->product = $product;
    }

    public function index() {
        $categories = $this->category->getCategories();
        $cartData = $this->cookieData();
    	$totalQty = $this->countCart();
        $subtotal = $this->subtotal();
        return view('client.cart.index', compact('totalQty', 'cartData', 'subtotal', 'categories'));
    }

    public function add(Request $request) {
        if($request->ajax()) {
            $totalQty = $request['totalQty'];
            $cartData = $request['cart'];
            $subtotal = $request['subtotal'];

            if(Auth::guard()->check()) {
                $id = Auth::user()->id;
                $cart['customer_id'] = $id;

                foreach($cartData as $cartItem) {
                    $this->cart->where('customer_id', $id)->where('product_id', $cartItem['product_id'])->delete();
                    $cart['product_id'] = $cartItem['product_id'];
                    $cart['name'] = $cartItem['name'];
                    $cart['image'] = $cartItem['image'];
                    $cart['qty'] = $cartItem['qty'];
                    $cart['stock_qty'] = $cartItem['stock_qty'];
                    $cart['price'] = $cartItem['price'];
                    $this->cart->create($cart);
                }

                $cart = $this->cart->getColByCustomer($id);
            }
            else {
                $cart = array();
            }
            return response()->json([
                'view' => view('client.cart.add', compact('totalQty', 'cartData', 'subtotal'))->render(),
                'cart' => $cart
            ]);
        }
    }

    public function update(Request $request) {
        if($request->ajax()) {
            if(Auth::guard()->check()) {
                $product_id = $request['id'];
                $newQty = $request['new_qty'];
                $customer_id = Auth::user()->id;
                $this->cart->where('customer_id', $customer_id)->where('product_id', $product_id)->update(['qty' => $newQty]);
                $cart = $this->cart->getByCustomer($customer_id);
            }
            else {
                $cart = null;
            }
            return response()->json($cart);
        }
    }

    public function delete(Request $request) {
        if($request->ajax()) {
            if(Auth::guard()->check()) {
                $product_id = $request['id'];
                $customer_id = Auth::user()->id;
                $this->cart->where('customer_id', $customer_id)->where('product_id', $product_id)->delete();
                $cart = $this->cart->getByCustomer($customer_id);
                return response()->json($cart);
            }
        }
    }
}
