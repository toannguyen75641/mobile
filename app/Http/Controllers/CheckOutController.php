<?php

namespace App\Http\Controllers;

use App\Model\Customer;
use App\Model\Product;
use App\Model\Category;
use App\Model\Cart;
use App\Model\Order;
use App\Model\OrderDetail;
use Cookie;
use App\Http\Requests\CustomerRequest;
use Auth;

class CheckOutController extends Controller
{
    public function __construct(Product $product, Category $category, Order $order, OrderDetail $oderDetail, Customer $customer, Cart $cart) {
        parent::__construct($category, $cart);
        $this->customer = $customer;
        $this->order = $order;
        $this->oderDetail = $oderDetail;
        $this->product = $product;
    }

    public function index()
    {   
        $categories = $this->category->getCategories();
        $totalQty = $this->countCart();
        $cartData = $this->cookieData();
        $subtotal = $this->subtotal();
        $user = null;
        if(Auth::guard()->check()) {
            $user = Auth::user();
        }
        return view('client.checkout.index', compact('totalQty', 'cartData', 'subtotal', 'categories', 'user'));
    }

    public function checkout(CustomerRequest $request) {
        $customer = $request->except('_token');
        // dd($customer); 
        $cartData = $this->cookieData();
        $subtotal = $this->subtotal();
        $check = false;
        
        if(Auth::guard()->check()) {
            $customer_id = Auth::user()->id;
            $this->customer->where('id', $customer_id)->update($customer);
        }
        else {
            $customer['password'] = bcrypt($request['password']);
            $customer_id = $this->customer->create($customer)->id;
        }
        if($customer_id) {
            $order['customer_id'] = $customer_id;
            $order['state'] = 0;
            $order['amount'] = $subtotal;
            $order_id = $this->order->create($order)->id;
            if($order_id) {
                foreach($cartData as $key => $cartItem) {
                    $amount = $cartItem['price'] * $cartItem['qty'];
                    $orderDetails[$key]['order_id'] = $order_id;
                    $orderDetails[$key]['product_id'] = $cartItem['product_id'];
                    $orderDetails[$key]['quantity'] = $cartItem['qty'];
                    $orderDetails[$key]['price'] = $cartItem['price'];
                    $orderDetails[$key]['amount'] = $amount;
                }
                foreach($orderDetails as $orderDetail) {
                    $this->oderDetail->create($orderDetail);
                    $check = true;
                }
                if($check == true) {
                    foreach($cartData as $cartItem) {
                        $new_qty = $cartItem['stock_qty'] - $cartItem['qty'];
                        $this->product->where('id' ,$cartItem['product_id'])->update(['quantity' => $new_qty]);
                    }
                    $this->cart->where('customer_id', $customer_id)->delete();
                    return redirect('/')->withCookie(Cookie::forget('cart'))->with('success', 'Order Success. Thanks for buying');
                }
            }
        }
    }
}
