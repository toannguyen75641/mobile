<?php

namespace App\Http\Controllers\Admin;

use App\Model\Slide;
use App\Model\Slider;
use App\Http\Requests\SlideRequest;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class SlideController extends Controller
{
    public function __construct(Slide $slide, Slider $slider) {
        $this->slide = $slide;
        $this->slider = $slider;
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $slides = $this->slide->getAll()->get();
        return view('admin.slide.index', compact('slides'));
    }
    public function create()
    {
        if(Auth::user()->can('sliders.create')) {
            $sliders = $this->slider->all();
            return view('admin.slide.create', compact('sliders'));
        }
        return redirect()->route('slide.index');
    }

    public function store(SlideRequest $request)
    {
        if($request->isMethod('post')) {
            $slide = $request->except('_token');
            $slide['user_id'] = Auth::user()->id;
            if($request->hasFile('image')) {
                $image = $request['image'];
                $imageName = $image->getClientOriginalName();
                $url = 'uploads/slides/'.$image->getClientOriginalName();
                if($image->move('uploads/slides', $imageName)) {
                    $slide['image'] = $imageName;
                    $slide['url'] = $url;
                }
                $this->slide->create($slide);
                return redirect()->route('slide.index')->with('success', 'The slide has been saved!');
            }
            return redirect()->route('slide.create')->with('fail', 'The slide has been failed!');
        }
    }

    public function show(Slide $slide)
    {
        //
    }

    public function edit($id)
    {
        $slide = $this->slide->getById($id);
        if(Auth::user()->can('slides.update', $slide)) {
            return view('admin.slide.edit', compact('slide'));
        }
        return redirect()->route('slide.index');
    }

    public function update(SlideRequest $request, $id)
    {
        if($request->isMethod('post')) {
            $slide = $request->except('_token');

            if($request->hasFile('image')) {
                $image = $request['image'];
                $imageName = $image->getClientOriginalName();
                $url = 'uploads/slides/'.$image->getClientOriginalName();
                if($image->move('uploads/slides', $imageName)) {
                    $slide['image'] = $imageName;
                    $slide['url'] = $url;
                }
            }

            if(isset($request['status'])) {
                if($request['status'] == 0) {
                    $slide['status'] = 1;
                }
            }
            else $slide['status'] = 0;
            
            $this->slide->where('id', $id)->update($slide);
            return redirect()->route('slide.index')->with('success', 'The slide has been saved!');
        }
    }

    public function destroy($id)
    {   
        $slide = $this->slide->getById($id);
        if(Auth::user()->can('slides.delete', $slide)) {
            $this->slide->where('id', $id)->delete();
            return redirect()->route('slide.index')->with('success', 'The slide has been deleted!');
        }
        return redirect()->route('slide.index');
    }
}
