<?php

namespace App\Http\Controllers\Admin;

use App\Model\User;
use App\Model\Role;
use App\Model\RoleUser;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function __construct(User $user, Role $role, RoleUser $roleUser) {
        $this->user = $user;
        $this->role = $role;
        $this->roleUser = $roleUser;
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $id = Auth::user()->id;
        $users = $this->user->all();
        return view('admin.user.index', compact('users', 'id'));
    }

    public function show($id)
    {
        $user = $this->user->getById($id);
        $role_users = $user->roles;
        return view('admin.user.show', compact('user', 'role_users'));
    }

    public function edit($id)
    {
        if(Auth::user()->can('users.edit')) {
            $user = $this->user->getById($id);
            $role_users = $user->roles;
            $roles = $this->role->all();
            return view('admin.user.edit', compact('user', 'role_users', 'roles'));
        }
        return redirect()->route('user.index');
    }

    public function update(UserRequest $request, $id)
    {
        if($request->isMethod('post')) {
            $user = $request->except('_token', 'role');

            $roleId = $request['role'];
            $roleUser['user_id'] = $id;
            $roleUser['role_id'] = $roleId;
        
            if($this->user->where('id', $id)->update($user)) {
                $role = $this->roleUser->where('user_id', $id)->first();
                if(empty($role)) {
                    $this->roleUser->create($roleUser);
                }
                else {
                    $this->roleUser->where('user_id', $id)->update($roleUser);
                }
                return redirect()->route('user.index')->withsuccess('The user has been saved!');
            }
        }
    }

    public function destroy($id)
    {
        $user_id = Auth::user()->id;
        if(Auth::user()->can('users.delete')) {
            if($id != $user_id) {
                $this->roleUser->where('user_id', $id)->delete();
                $this->user->where('id', $id)->delete();
                return redirect()->route('user.index')->withsuccess('The user has been delete!');
            }
        }
        return redirect()->route('user.index');
    }
}
