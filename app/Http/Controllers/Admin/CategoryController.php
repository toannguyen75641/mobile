<?php

namespace App\Http\Controllers\Admin;

use App\Model\Category;
use App\Model\Product;
use App\Http\Requests\CategoryRequest;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;


class CategoryController extends Controller
{
    public function __construct(Category $category, Product $product){
        $this->category = $category;
        $this->product = $product;
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $categoires = $this->category->all();
        return view('admin.category.index', compact('categoires'));
    }

    public function create()
    {
        if(Auth::user()->can('categories.create')) {
            return view('admin.category.create');
        }
        return redirect()->route('category.index');
    }

    public function store(CategoryRequest $request)
    {
        if($request->isMethod('post')) {
            $category = $request->except('_token');
            if($this->category->create($category)) {
                return redirect()->route('category.index')->with('success', 'The category has been saved!');
            }
            return redirect()->route('category.create')->with('fail', 'The category has been failed!');
        }
    }

    public function show($id)
    {   
        $category = $this->category->getById($id);
        $products = $this->product->getByCategory($id)->get();
        return view('admin.category.show', compact('category', 'products'));
    }

    public function edit($id)
    {
        if(Auth::user()->can('categories.update')) {
            $category = $this->category->getById($id);
            
            return view('admin.category.edit', ['category' => $category]);
        }
        return redirect()->route('category.index');
    }

    public function update(CategoryRequest $request, $id)
    {
        if($request->isMethod('post')) {
            $category = $request->except('_token', 'album');

            $item = $this->category->getById($id);

            if($request['description'] == null) {
                $category['description'] = $item['description'];
            }
        
            if($this->category->where('id', $id)->update($category)) {
                return redirect()->route('category.show', $id)->withsuccess('The category has been saved!');
            }
        }
    }

    public function destroy($id)
    {
        if(Auth::user()->can('categories.delete')) {
            if($this->category->where('id', $id)->delete()) {
                return redirect()->route('category.index')->withsuccess('The category has been deleted!');
            }
        }
        return redirect()->route('category.index');
    }
}
