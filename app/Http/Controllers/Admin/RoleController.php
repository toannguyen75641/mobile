<?php

namespace App\Http\Controllers\Admin;

use App\Model\Role;
use App\Model\RoleUser;
use App\Model\Permission;
use App\Http\Requests\RoleRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
    public function __construct(Role $role, Permission $permission, RoleUser $roleUser) {
        $this->role = $role;
        $this->permission = $permission;
        $this->roleUser = $roleUser;
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $roles = $this->role->all()->except(['id' => 1]);
        return view('admin.role.index', compact('roles'));
    }

    public function create()
    {
        if(Auth::user()->can('roles.create')) {
            $permissions = $this->permission->all();
            return view('admin.role.create', compact('permissions'));
        }
        return redirect()->route('role.index');
    }

    public function store(RoleRequest $request)
    {
        if($request->isMethod('post')) {
            $role = $request->except('_token', 'permission');
            $permissions = $request['permission'];
            $role_id = $this->role->create($role)->id;
            $permission_role = $this->role->getById($role_id);

            if($role_id) {
                $permission_role->permissions()->attach($permissions);
                return redirect()->route('role.index')->withsuccess('The role has been save!');
            }
        }
    }

    public function show($id)
    {
        $role = $this->role->getById($id);
        return view('admin.role.show', compact('role'));
    }

    public function edit($id)
    {
        if(Auth::user()->can('roles.update')) {
            $role = $this->role->getById($id);
            $permissions = $this->permission->all();
            return view('admin.role.edit', compact('role', 'permissions'));
        }
        return redirect()->route('role.index');
    }

    public function update(Request $request, $id)
    {  
        if($request->isMethod('post')) {
            $this->validate($request, [
                'name' => 'required'
            ]);
            $role = $request->except('_token', 'permission');
            $permissions = $request['permission'];
            $permission_role = $this->role->getById($id);

            if($this->role->where('id', $id)->update($role)) {
                $permission_role->permissions()->sync($permissions);
                return redirect()->route('role.index')->withsuccess('The role has been save!');
            }
        }
    }

    public function destroy($id) {
        if(Auth::user()->can('roles.delete')) {
            $this->roleUser->where('role_id', $id)->delete();
            $this->role->where('id', $id)->delete();
            return redirect()->route('role.index')->withsuccess('The role has been deleted!');
        }
    }
}
