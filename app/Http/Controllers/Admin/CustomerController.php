<?php

namespace App\Http\Controllers\Admin;

use App\Model\Customer;
use App\Model\Order;
use App\Model\Cart;
use App\Model\OrderDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class CustomerController extends Controller
{
    public function __construct(Customer $customer, Order $order, OrderDetail $orderDetail, Cart $cart) {
        $this->customer = $customer;
        $this->order = $order;
        $this->orderDetail = $orderDetail;
        $this->cart = $cart;
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $customers = $this->customer->all();
        return view('admin.customer.index', compact('customers'));
    }

    public function show($id)
    {
        $customer = $this->customer->getById($id);
        $orders = $this->order->getByCustomer($id);
        return view('admin.customer.show', compact('customer', 'orders'));
    }

    public function destroy($id)
    {
        $orders = $this->order->getByCustomer($id);
        foreach($orders as $order) {
            $this->orderDetail->where('order_id', $order->id)->delete();
        }
        $this->order->where('customer_id', $id)->delete();
        $this->cart->where('customer_id', $id)->delete();
        $this->customer->where('id', $id)->delete();
        return redirect()->route('customer.index')->with('success', 'The customer has been deleted!');
    }
}
