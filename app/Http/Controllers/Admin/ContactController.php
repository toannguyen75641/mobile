<?php

namespace App\Http\Controllers\Admin;

use App\Model\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class ContactController extends Controller
{
    public function __construct(Contact $contact) {
        $this->contact = $contact;
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $contacts = $this->contact->all();
        return view('admin.contact.index', compact('contacts'));
    }
	public function destroy($id)
    {
        if(Auth::user()->can('contacts.delete')) {
            if($this->contact->where('id', $id)->delete()) {
                return redirect()->route('contacts.index')->withsuccess('The contact has been deleted!');
            }
        }
        return redirect()->route('contact.index');
    }
}
