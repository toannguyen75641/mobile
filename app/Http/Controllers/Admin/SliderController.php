<?php

namespace App\Http\Controllers\Admin;

use App\Model\Slider;
use App\Model\Slide;
use App\Http\Requests\SliderRequest;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class SliderController extends Controller
{
    public function __construct(Slider $slider, Slide $slide) {
        $this->slider = $slider;
        $this->slide = $slide;
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $sliders = $this->slider->getAll()->get();
        return view('admin.slider.index', compact('sliders'));
    }
    public function create()
    {
        if(Auth::user()->can('sliders.create')) {
            $bannerNew = $this->slide
                    ->leftJoin('sliders', 'slider_id', '=', 'sliders.id')
                    ->where('sliders.type', 1)
                    ->get();
            $countBannerNew = count($bannerNew);
            return view('admin.slider.create', compact('countBannerNew'));
        }
    }

    public function store(SliderRequest $request)
    {
        if($request->isMethod('post')) {
            $slider = $request->except('_token', 'slides', 'order', 'title', 'description', 'status');
            $slider['user_id'] = Auth::user()->id;
            if($request->hasFile('slides')) {
                $slideImages = $request['slides'];
                $slideOrders = $request['order'];
                $slideTitles = $request['title'];
                $slideDescriptions = $request['description'];
                $slides = [];
                $id = $this->slider->create($slider)->id;
                $check = false;

                if($id) {
                    foreach($slideOrders as $key => $slideOrder) {
                        $slides[$key]['order'] = $slideOrder;
                        $slides[$key]['user_id'] = Auth::user()->id;
                        $slides[$key]['slider_id'] = $id;
                    }

                    foreach($slideTitles as $key => $slideTitle) {
                        $slides[$key]['title'] = $slideTitle;
                    }

                    foreach($slideDescriptions as $key => $slideDescription) {
                        $slides[$key]['description'] = $slideDescription;
                    }

                    if(isset($request['status'])) {
                        $slideStatuses = $request['status'];
                        foreach($slideStatuses as $key => $slideStatus) {
                            $slides[$key]['status'] = $slideStatus;
                        }
                    }

                    foreach($slideImages as $key => $slideImage) {
                        $imageName = $slideImage->getClientOriginalName();
                        $url = 'uploads/slides/'.$imageName;
                        if($slideImage->move('uploads/slides', $imageName)) {
                            $slides[$key]['image'] = $imageName;
                            $slides[$key]['url'] = $url;
                        }
                    }

                    foreach($slides as $slide) {
                        $this->slide->create($slide);
                        $check = true;
                    }
                    if($check == true) {
                        return redirect()->route('slider.index')->with('success', 'The slider has been saved!');
                    }
                }
            }
            return redirect()->route('slider.create')->with('fail', 'The slider has been failed!');
        }
    }

    public function show($id)
    {
        $slider = $this->slider->getById($id);
        $slides = $this->slide->getBySlider($id);
        return view('admin.slider.show', compact('slider', 'slides'));
    }

    public function edit($id)
    {
        $slider = $this->slider->getById($id);
        if(Auth::user()->can('sliders.update', $slider)) {
            $slides = $this->slide->getBySlider($id);
            // $width = preg_split('/(?<=[0-9])(?=[^0-9]+)/i', $slider->width);
            // $height = preg_split('/(?<=[0-9])(?=[^0-9]+)/i', $slider->height);
            return view('admin.slider.edit', compact('slider', 'slides'));
        }
    }

    public function update(SliderRequest $request, $id)
    {
        if($request->isMethod('post')) {
            $slider = $request->except('_token', 'slides', 'order', 'title', 'description', 'status', 'checkImage');

            if($request->hasFile('slides')) {
                $slideImages = $request['slides'];
                $slideOrders = $request['order'];
                $slideTitles = $request['title'];
                $slideDescriptions = $request['description'];
                $slidesInsert = [];

                foreach($slideOrders as $key => $slideOrder) {
                    $slidesInsert[$key]['order'] = $slideOrder;
                }

                foreach($slideTitles as $key => $slideTitle) {
                    $slidesInsert[$key]['title'] = $slideTitle;
                }

                foreach($slideDescriptions as $key => $slideDescription) {
                    $slidesInsert[$key]['description'] = $slideDescription;
                }

                if(isset($request['status'])) {
                    $slideStatuses = $request['status'];
                    foreach($slideStatuses as $key => $slideStatus) {
                        $slidesInsert[$key]['status'] = $slideStatus;
                    }
                }
                
                foreach($slideImages as $key => $slideImage) {
                    $slidesInsert[$key]['user_id'] = Auth::user()->id;
                    $slidesInsert[$key]['slider_id'] = $id;
                    $imageName = $slideImage->getClientOriginalName();
                    $url = 'uploads/slides/'.$imageName;
                    if($slideImage->move('uploads/slides', $imageName)) {
                        $slidesInsert[$key]['image'] = $imageName;
                        $slidesInsert[$key]['url'] = $url;
                    }
                }

                foreach($slidesInsert as $slideInsert) {
                    $this->slide->create($slideInsert);
                }
            }
            if(!empty($request['checkImage'])) {
                $slidesId = $request['checkImage'];
                foreach($slidesId as $slideId) {
                    $this->slide->where('id', $slideId)->delete();
                }
            }

            if($this->slider->where('id', $id)->update($slider)) {
                return redirect()->route('slider.show', $id)->with('success', 'The slider has been saved!');
            }
        }
    }

    public function destroy($id)
    {
        $slider = $this->slider->getById($id);
        if(Auth::user()->can('sliders.delete', $slider)) {
            $this->slide->where('slider_id', $id)->delete();
            $this->slider->where('id', $id)->delete();
            return redirect()->route('slider.index')->with('success', 'The slider has been deleted!');
        }
    }
}
