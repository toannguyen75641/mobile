<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Requests\SignupRequest;
use Auth;
use App\Model\User;
use App\Model\RoleUser;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/admin';

    public function __construct(User $user, RoleUser $roleUser) {
        $this->user = $user;
        $this->roleUser = $roleUser;
    	$this->middleware('guest:admin')->except('logout');
    }

    public function showLoginForm() {
    	return view('admin.auth.login');
    }

    public function login(Request $request) {
        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            return redirect()->intended(route('admin'));
        }
       	return back()->withErrors(['email' => 'Email or password are wrong.'])->withInput($request->only('email', 'remember'));	
    }

   	public function logout(Request $request) {
        Auth::guard('admin')->logout();
        return redirect('/admin/login');
    }

    public function showRegistrationForm()
    {
        return view('admin.auth.register');
    }

    public function register(SignupRequest $request) {
        $user = $request->except('_token', 'password_confirmation');
        $user['password'] = Hash::make($request['password']);
        $user_id = $this->user->create($user)->id;
        if($user_id) {
            $this->login($request);
            return redirect()->intended(route('admin'))->with('success', 'Successful Registration');
        }

    }
}
