<?php

namespace App\Http\Controllers\Admin;

use App\Model\Cart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CartController extends Controller
{
    public function __construct(Cart $cart){
        $this->cart = $cart;
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $carts = $this->cart->getAll()->get();

        return view('admin.cart.index', compact('carts'));
    }
}
