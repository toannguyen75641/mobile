<?php

namespace App\Http\Controllers\Admin;

use App\Model\Product;
use App\Model\Category;
use App\Model\ImageAlbum;
use App\Model\ProductDetail;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\ProductRequest;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function __construct(Product $product, Category $category, ImageAlbum $imageAlbum, ProductDetail $productDetail)
    {
        $this->product = $product;
        $this->category = $category;
        $this->imageAlbum = $imageAlbum;
        $this->productDetail = $productDetail;
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $products = $this->product->getAll()->get();
        return view('admin.product.index', ['products' => $products]);

    }

    public function create()
    {   
        if(Auth::user()->can('products.create')) {
            $categories = $this->category->all();
            return view('admin.product.create', ['categories' => $categories]);
        }
        return redirect()->route('product.index');
    }

    public function store(ProductRequest $request)
    {
        if($request->isMethod('post')) {
            $product = $request->except('_token', 'album', 'feature', 'screen', 'camera', 'ram', 'rom', 'weight', 'battery');
            $detail = $request->except('_token', 'album', 'image', 'name', 'category_id', 'price', 'quantity', 'description', 'status');
            if($request->hasFile('image')) {
                $image = $request['image'];
                $imageName = 'uploads/'.$image->getClientOriginalName();
                if($image->move('uploads', $imageName)) {
                    $product['image'] = $imageName;
                }
            }
            
            if(isset($request['status'])) {
                $product['status'] = $request['status'];
            }

            $id = $this->product->create($product)->id;
            if($id) {
                if($request->hasFile('album')) {
                    $albums = $request['album'];
                    $image = [];
                    $imageAlbums = [];
                    $image['product_id'] = $id;
                    foreach($albums as $album) {
                        $imageAlbumName = $album->getClientOriginalName();
                        $imageAlbumUrl = 'uploads/'.$imageAlbumName;
                        if($album->move('uploads', $imageAlbumName)) {
                            $image['image'] = $imageAlbumName;
                            $image['url'] = $imageAlbumUrl;
                        }
                        $imageAlbums[] = $image;
                    }
                    foreach($imageAlbums as $imageAlbum) {
                        $this->imageAlbum->create($imageAlbum);
                    }
                }
                $detail['product_id'] = $id;
                $this->productDetail->create($detail);
                return redirect()->route('product.index')->with('success', 'The product has been saved!');
            }
        }
    }

    public function show($id)
    {
        $product = $this->product->getById($id);
        $imageAlbums = $this->imageAlbum->getByProduct($id);
        $productDetail = $this->productDetail->getByProduct($id);
        return view('admin.product.show', compact('product', 'imageAlbums', 'productDetail'));
    }

    public function edit($id)
    {   
        if(Auth::user()->can('products.update')) {
            $category = $this->category->all();
            $product = $this->product->getById($id);
            $imageAlbums = $this->imageAlbum->getByProduct($id);
            $productDetail = $this->productDetail->getByProduct($id);
            return view('admin.product.edit', compact('category', 'product', 'imageAlbums', 'productDetail'));
        }
        return redirect()->route('product.index');
    }

    public function update(ProductRequest $request, $id)
    {
        if($request->isMethod('post')) {
            $product = $request->except('_token', 'album', 'checkImage', 'feature', 'screen', 'camera', 'ram', 'rom', 'weight', 'battery');
            $detail = $request->except('_token', 'album', 'checkImage', 'image', 'name', 'category_id', 'price', 'quantity', 'description', 'status');

            if($request->hasFile('image')) {
                $image = $request['image'];
                $imageName = 'uploads/'.$image->getClientOriginalName();
                if($image->move('uploads', $imageName)) {
                    $product['image'] = $imageName;
                }
            }
            $item = $this->product->getById($id);
            if($request['description'] == null) {
                $product['description'] = $item['description'];
            }

            if(isset($request['status'])) {
                if($request['status'] == 0) {
                    $product['status'] = 1;
                }
            }
            else $product['status'] = 0;

            if($request->hasFile('album')) {
                $albums = $request['album'];
                $image = [];
                $imageAlbums = [];
                foreach($albums as $album) {
                    $image['product_id'] = $id;
                    $imageAlbumName = $album->getClientOriginalName();
                    $imageAlbumUrl = 'uploads/'.$imageAlbumName;
                    if($album->move('uploads', $imageAlbumName)) {
                        $image['image'] = $imageAlbumName;
                        $image['url'] = $imageAlbumUrl;
                    }
                    $imageAlbums[] = $image;
                }
                foreach($imageAlbums as $imageAlbum) {
                    $this->imageAlbum->create($imageAlbum);
                }
            }

            $detail['product_id'] = $id;
            $productDetail = $this->productDetail->where('product_id', $id)->first();
            if(!empty($productDetail)) {
                $this->productDetail->where('product_id', $id)->update($detail);
            }
            else {
                $this->productDetail->create($detail);
            }
            if(!empty($request['checkImage'])) {
                $imagesId = $request['checkImage'];
                foreach($imagesId as $imageId) {
                    $this->imageAlbum->where('id', $imageId)->delete();
                }
            }
            $this->product->where('id', $id)->update($product);
            return redirect()->route('product.show', $id)->with('success', 'The product has been saved!');
        }
    }

    public function destroy($id)
    {
        if(Auth::user()->can('products.update')) {
            $this->imageAlbum->where('product_id', $id)->delete();
            $this->productDetail->where('product_id', $id)->delete();
            $this->product->where('id', $id)->delete();
            return redirect()->route('product.index')->with('success', 'The product has been deleted!');
        }
        return redirect()->route('product.index');
    }
}
