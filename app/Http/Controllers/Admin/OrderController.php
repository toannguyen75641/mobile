<?php

namespace App\Http\Controllers\Admin;

use App\Model\Order;
use App\Model\OrderDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function __construct(Order $order, OrderDetail $orderDetail) {
        $this->order = $order;
        $this->orderDetail = $orderDetail;
        $this->middleware('auth:admin');
    }
  
    public function index()
    {
        $orders = $this->order->getAll()->get();
        return view('admin.order.index', compact('orders'));
    }

    public function show($id)
    {
        $order = $this->order->getById($id)->first();
        // dd($order);
        $orderDetails = $this->orderDetail->getByOrderId($id)->get();
        return view('admin.order.show', compact('order', 'orderDetails'));
    }

    public function edit($id) {
        if(Auth::user()->can('contacts.delete')) {
            $order = $this->order->getAll()->first();
            return view('admin.order.edit', compact('order'));
        }
        return redirect()->route('order.index');
    }

    public function update(Request $request, $id) {
        $order = $request->except('_token');
        if($this->order->where('id', $id)->update($order)) {
            return redirect()->route('order.show', $id)->with('success', 'The order has been saved!');
        }
    }

    public function destroy($id)
    {
        if(Auth::user()->can('contacts.delete')) {
            $this->orderDetail->where('order_id', $id)->delete();
            $this->order->where('id', $id)->delete();
            return redirect()->route('order.index')->with('success', 'The order has been deleted!');
        }
        return redirect()->route('order.index');
    }
}
