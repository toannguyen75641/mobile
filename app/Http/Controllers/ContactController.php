<?php

namespace App\Http\Controllers;

use App\Model\Contact;
use App\Model\Cart;
use App\Model\Category;
use App\Model\User;
use App\Http\Requests\ContactRequest;
use Auth;
use Mail;
use App\Mail\SendMail;

class ContactController extends Controller
{
    public function __construct(Contact $contact, Category $category, Cart $cart, User $user) {
        parent::__construct($category, $cart);
        $this->contact = $contact;
        $this->user = $user;
    }

    public function index()
    {
        if(Auth::guard()->check()) {
            $categories = $this->category->getCategories();
            $user = null;
            if(Auth::guard()->check()) {
                $user = Auth::user();
            }
            return view('client.contact.index', compact('categories', 'user'));
        }
        return redirect()->route('login');
    }

    public function sendMail(ContactRequest $request) 
    {
        if(Auth::guard()->check()) {
            $mailUser = $this->user->getMail();
            $contact = $request->except('_token');
            $contact['customer_id'] = Auth::user()->id;

            $subject = $request->subject;
            $email = $request->email;
            $name = $request->name;
            $message = $request->message;

            Mail::to($mailUser)->send(new SendMail($name, $email, $subject, $message));
            $this->contact->create($contact);
            return redirect()->route('contact.index')->with('success', 'Your email sent!');
        }
    }

}
