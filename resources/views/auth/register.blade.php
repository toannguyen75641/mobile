@extends('layouts.clientLogin')

@section('content')
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-50">
                <span class="login100-form-title p-b-33">
                    Account Register
                </span>
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                    <div class="wrap-input100 validate-input">
                        <input id="name" type="text" class="input100" name="name" value="{{ old('name') }}" placeholder="Full Name" autofocus>
                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong style="color: red">{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                        <span class="focus-input100-1"></span>
                        <span class="focus-input100-2"></span>
                    </div>

                    <div class="wrap-input100 validate-input">
                        <input id="email" type="email" class="input100" name="email" value="{{ old('email') }}" placeholder="Email" autofocus>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong style="color: red">{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        <span class="focus-input100-1"></span>
                        <span class="focus-input100-2"></span>
                    </div>

                    <div class="wrap-input100 rs1 validate-input">
                        <input id="password" type="password" class="input100" name="password" placeholder="Password">

                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong style="color: red">{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                        <span class="focus-input100-1"></span>
                        <span class="focus-input100-2"></span>
                    </div>

                    <div class="wrap-input100 rs1 validate-input">
                        <input id="password-confirm" type="password" class="input100" placeholder="Confirm Password" name="password_confirmation">
                        <span class="focus-input100-1"></span>
                        <span class="focus-input100-2"></span>
                    </div>

                    <div class="container-login100-form-btn m-t-20">
                        <button class="login100-form-btn">
                            Sign up
                        </button>
                    </div>

                    <div class="text-center p-t-45 p-b-4">
                        <span class="txt1">
                            Already has account!
                        </span>

                        <a href="{{ route('login') }}" class="txt2 hov1">
                            Sign in
                        </a>
                    </div>

                    <div class="text-center">
                        <span class="txt1">
                            Forgot
                        </span>

                        <a href="{{ route('password.request') }}" class="txt2 hov1">
                            Password?
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
