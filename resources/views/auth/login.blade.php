@extends('layouts.clientLogin')

@section('content')
<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-50">
            <span class="login100-form-title p-b-33">
                Account Login
            </span>
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="wrap-input100 validate-input">
                    <input id="email" type="email" class="input100" name="email" value="{{ old('email') }}" placeholder="Email" autofocus>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong style="color: red">{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                    <span class="focus-input100-1"></span>
                    <span class="focus-input100-2"></span>
                </div>

                <div class="wrap-input100 rs1 validate-input">
                    <input id="password" type="password" class="input100" name="password" placeholder="Password">

                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong style="color: red">{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                    <span class="focus-input100-1"></span>
                    <span class="focus-input100-2"></span>
                </div>

                <div class="container-login100-form-btn m-t-20">
                    <button class="login100-form-btn">
                        Sign in
                    </button>
                </div>

                <div class="text-center p-t-45 p-b-4">
                    <span class="txt1">
                        Create an account?
                    </span>

                    <a href="{{ route('register') }}" class="txt2 hov1">
                        Sign up
                    </a>
                </div>

                <div class="text-center">
                    <span class="txt1">
                        Forgot
                    </span>

                    <a href="{{ route('password.request') }}" class="txt2 hov1">
                        Password?
                    </a>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection
