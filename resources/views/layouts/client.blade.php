@include('includes.client.head')

@include('includes.client.header')

@include('includes.client.navigation')

@include('includes.client.noti')

@yield('content')

@include('includes.client.footer')