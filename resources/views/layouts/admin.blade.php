@include('includes.admin.head')

@include('includes.admin.header')

@include('includes.admin.sidebar')

<div class="content-wrapper">
	@include('includes.admin.noti')

	@yield('content')
</div>

@include('includes.admin.footer')

