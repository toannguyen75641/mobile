<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login V19</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="{{asset('css/util.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}">
  	<link rel="stylesheet" href="{{asset('css/bootstrap/dist/css/bootstrap.min.css')}}">
  	<link rel="stylesheet" href="{{asset('css/font-awesome/css/font-awesome.min.css')}}">
  	<link rel="stylesheet" href="{{asset('css/Ionicons/css/ionicons.min.css')}}">
<!--===============================================================================================-->
</head>
<body>
	@yield('content')
</body>
</html>