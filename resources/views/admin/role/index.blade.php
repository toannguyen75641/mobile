@extends('layouts.admin')
@section('title', 'Role Manager')
@section('content')
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Role Table</h3>
            </div>
            <div class="box-body">
              <table id="table" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Created_at</th>
                        <th>Updated_at</th>
                        @can('roles.update')
                            <th>Edit</th>
                        @endcan
                        @can('roles.delete')
                            <th>Delete</th>
                        @endcan
                    </tr>
                </thead>
                <tbody>
                    @foreach($roles as $role)
                    <tr>
                        <td><a href="{{ Route('role.show', $role->id) }}">{{ $role->name }}</a></td>
                        <td>{{ $role->created_at }}</td>
                        <td>{{ $role->updated_at }}</td>
                        @can('roles.update')
                            <td>
                                <a href="{{ Route('role.edit', $role->id) }}"><i class="fas fa-pen-square" style="font-size: 20px"></i></a>
                            </td>
                        @endcan
                        @can('roles.delete')
                            <td>
                                <a href="{{ Route('role.destroy', $role->id) }}"><i class="fas fa-trash" style="font-size: 20px"></i></a>
                            </td>
                        @endcan
                    </tr> 
                    @endforeach 
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
<script type="text/javascript">
    $(document).ready(function() {
        $('.side5').addClass('active');
    });
</script>
  
@endsection