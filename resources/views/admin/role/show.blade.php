@extends('layouts.admin')
@section('title', 'Create Role')
@section('content')
	<div class="form large-9 medium-8 columns content">
		<div class="box box-primary">
            <div class="box-header with-border">
              	<h3 class="box-title">{{ $role->name }}</h3>
            </div>
            <div class="box-body">
                <div class="row">
  	              	<div class="col-md-4">
      	              	<label for="name">Category Permissions</label>
      	              	@foreach($role->permissions as $permission)
      	              		@if($permission->for == 'category')
		      	              	<div class="checkbox">
		      	              		<label>
				                    	{{ $permission->name }}
				                  	</label>
		      	              	</div>
	      	              	@endif
      	              	@endforeach
  	              	</div>

  	              	<div class="col-md-4">
      	              	<label for="name">Product Permissions</label>
      	              	@foreach($role->permissions as $permission)
      	              		@if($permission->for == 'product')
		      	              	<div class="checkbox">
		      	              		<label>
				                    	{{ $permission->name }}
				                  	</label>
		      	              	</div>
	      	              	@endif
      	              	@endforeach
  	              	</div>

  	              	<div class="col-md-4">
      	              	<label for="name">Role Permissions</label>
      	              	@foreach($role->permissions as $permission)
      	              		@if($permission->for == 'role')
		      	              	<div class="checkbox">
		      	              		<label>
				                    	{{ $permission->name }}
				                  	</label>
		      	              	</div>
	      	              	@endif
      	              	@endforeach
  	              	</div>

  	              	<div class="col-md-4">
      	              	<label for="name">Slider Permissions</label>
      	              	@foreach($role->permissions as $permission)
      	              		@if($permission->for == 'slider')
		      	              	<div class="checkbox">
		      	              		<label>
				                    	{{ $permission->name }}
				                  	</label>
		      	              	</div>
	      	              	@endif
      	              	@endforeach
  	              	</div>

  	              	<div class="col-md-4">
      	              	<label for="name">User Permissions</label>
      	              	@foreach($role->permissions as $permission)
      	              		@if($permission->for == 'user')
		      	              	<div class="checkbox">
		      	              		<label>
				                    	{{ $permission->name }}
				                  	</label>
		      	              	</div>
	      	              	@endif
      	              	@endforeach
  	              	</div>
  	            </div>

          	</div>
        </div>
	</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.side5').addClass('active');
    });
</script>
@endsection