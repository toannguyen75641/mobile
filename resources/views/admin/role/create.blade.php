@extends('layouts.admin')
@section('title', 'Create Role')
@section('content')
	<div class="form large-9 medium-8 columns content">
		<div class="box box-primary">
            <div class="box-header with-border">
              	<h3 class="box-title">Create Role</h3>
            </div>
            <form action="{{ Route('role.store') }}" method="post" enctype="multipart/form-data">
            	@csrf
	            <div class="box-body">
	                <div class="form-group">
	                  	<label>Name</label>
	                  	<input type="text" class="form-control" placeholder="Role Name" name="name">
	                  	@if($errors->has('name'))
	                  		<p style="color: red">{{ $errors->first('name') }}</p>
	                  	@endif
	                </div>
	                <div class="row">
      	              	<div class="col-md-4">
	      	              	<label for="name">Category Permissions</label>
	      	              	@foreach($permissions as $permission)
	      	              		@if($permission->for == 'category')
			      	              	<div class="checkbox">
			      	              		<label>
					                    	<input type="checkbox" name="permission[]" value="{{ $permission->id }}">{{ $permission->name }}
					                  	</label>
			      	              	</div>
		      	              	@endif
	      	              	@endforeach
      	              	</div>

      	              	<div class="col-md-4">
	      	              	<label for="name">Product Permissions</label>
	      	              	@foreach($permissions as $permission)
	      	              		@if($permission->for == 'product')
			      	              	<div class="checkbox">
			      	              		<label>
					                    	<input type="checkbox" name="permission[]" value="{{ $permission->id }}">{{ $permission->name }}
					                  	</label>
			      	              	</div>
		      	              	@endif
	      	              	@endforeach
      	              	</div>

      	              	<div class="col-md-4">
	      	              	<label for="name">Role Permissions</label>
	      	              	@foreach($permissions as $permission)
	      	              		@if($permission->for == 'role')
			      	              	<div class="checkbox">
			      	              		<label>
					                    	<input type="checkbox" name="permission[]" value="{{ $permission->id }}">{{ $permission->name }}
					                  	</label>
			      	              	</div>
		      	              	@endif
	      	              	@endforeach
      	              	</div>

      	              	<div class="col-md-4">
	      	              	<label for="name">Slider Permissions</label>
	      	              	@foreach($permissions as $permission)
	      	              		@if($permission->for == 'slider')
			      	              	<div class="checkbox">
			      	              		<label>
					                    	<input type="checkbox" name="permission[]" value="{{ $permission->id }}">{{ $permission->name }}
					                  	</label>
			      	              	</div>
		      	              	@endif
	      	              	@endforeach
      	              	</div>

      	              	<div class="col-md-4">
	      	              	<label for="name">User Permissions</label>
	      	              	@foreach($permissions as $permission)
	      	              		@if($permission->for == 'user')
			      	              	<div class="checkbox">
			      	              		<label>
					                    	<input type="checkbox" name="permission[]" value="{{ $permission->id }}">{{ $permission->name }}
					                  	</label>
			      	              	</div>
		      	              	@endif
	      	              	@endforeach
      	              	</div>

      	              	<div class="col-md-4">
	      	              	<label for="name">Contact Permissions</label>
	      	              	@foreach($permissions as $permission)
	      	              		@if($permission->for == 'contact')
			      	              	<div class="checkbox">
			      	              		<label>
					                    	<input type="checkbox" name="permission[]" value="{{ $permission->id }}">{{ $permission->name }}
					                  	</label>
			      	              	</div>
		      	              	@endif
	      	              	@endforeach
      	              	</div>
      	            </div>

              	</div>
              	<div class="box-footer">
                	<button type="submit" class="btn btn-primary" value="Submit">Create</button>
              	</div>
            </form>
        </div>
	</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.side5').addClass('active');
    });
</script>
@endsection