@extends('layouts.admin')
@section('title', 'Category Manager')
@section('content')
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Category Table</h3>
            </div>
            <div class="box-body">
              <table id="table" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Created_at</th>
                        <th>Updated_at</th>
                        @can('categories.update')
                            <th>Edit</th>
                        @endcan
                        @can('categories.delete')
                            <th>Delete</th>
                        @endcan
                    </tr>
                </thead>
                <tbody>
                    @foreach($categoires as $category)
                    <tr>
                        <td><a href="{{ Route('category.show', $category->id) }}">{{ $category->name }}</a></td>
                        <td>{{ $category->description }}</td>
                        <td>{{ $category->created_at }}</td>
                        <td>{{ $category->updated_at }}</td>
                        @can('categories.update')
                            <td>
                                <a href="{{ Route('category.edit', $category->id) }}"><i class="fas fa-pen-square" style="font-size: 20px"></i></a>
                            </td>
                        @endcan
                        @can('categories.delete')
                            <td>
                                <a href="{{ Route('category.destroy', $category->id) }}" onclick="return confirm('Are you sure?');"><i class="fas fa-trash" style="font-size: 20px"></i></a>
                            </td>
                        @endcan
                    </tr> 
                    @endforeach 
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
  
<script type="text/javascript">
    $(document).ready(function() {
        $('.side0').addClass('active');
    });
</script>
@endsection