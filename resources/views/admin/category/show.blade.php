@extends('layouts.admin')
@section('title', 'Category Details')
@section('content')
<div class="form large-9 medium-8 columns content">
	<div class="box box-primary">
        <div class="box-header with-border">
          	<h3 class="box-title">{{ $category->name }}</h3>
          	<a href="{{ Route('category.edit', $category->id) }}"><i class="fas fa-pen-square" style="font-size: 18px"></i></a>
        </div>
	    <table class="table table-bordered table-hover">
	        <tr>
	            <th scope="row">Description</th>
	            <td>{{ $category->description }}</td>
	        </tr>
	        <tr>
	            <th scope="row">Created_at</th>
	            <td>{{ $category->created_at }}</td>
	        </tr>
	    </table>
	    <div class="box-body">
	    	<h4>Ralated Products</h4>
	    	<table id="table" class="table table-bordered table-hover">
	    		<thead>
		    		<tr>
		    			<th>Product Name</th>
		    			<th>Price</th>
		    			<th>Quantity</th>
		    			<th>Image</th>
		    			<th>Status</th>
		    			<th>Created_at</th>
		    			<th>Updated_at</th>
                        @can('products.update')
                            <th>Edit</th>
                        @endcan
                        @can('products.delete')
                            <th>Delete</th>
                        @endcan
		    		</tr>
	    		</thead>
	    		@foreach($products as $product)
	    			<tr>
	    				<td><a href="{{ Route('product.show', $product->id) }}">{{ $product->name }}</a></td>
	    				<td>{{ $product->price }}</td>
	    				<td>{{ $product->quantity }}</td>
	    				<td><img src="{{ asset($product->image) }}" style="width: 100px; height: 100px;" class="img-thumbnail"></td>
	    				<td>
	    					@if($product->status == 0)
                                Unpublish
                            @elseif($product->status == 1)
                                Publish
                            @endif
	    				</td>
	    				<td>{{ $product->created_at }}</td>
	    				<td>{{ $product->updated_at }}</td>
	                    @can('products.update')
	                        <td>
	                            <a href="{{ Route('product.edit', $product->id) }}"><i class="fas fa-pen-square" style="font-size: 20px"></i></a>
	                        </td>
	                    @endcan
	                    @can('products.delete')
	                        <td>
	                            <a href="{{ Route('product.destroy', $product->id) }}" onclick="return confirm('Are you sure?');"><i class="fas fa-trash" style="font-size: 20px"></i></a>
	                        </td>
	                    @endcan
	    			</tr>
	    		@endforeach
	    	</table>
	    </div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('.side0').addClass('active');
	});
</script>
@endsection