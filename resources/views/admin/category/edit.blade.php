@extends('layouts.admin')
@section('title', 'Update Category')
@section('content')
	<div class="form large-9 medium-8 columns content">
		<div class="box box-primary">
            <div class="box-header with-border">
              	<h3 class="box-title">Update Category</h3>
            </div>
            <form action="{{ Route('category.update', $category->id) }}" method="post" enctype="multipart/form-data">
            	@csrf
	            <div class="box-body">
	                <div class="form-group">
	                  	<label>Name</label>
	                  	<input type="text" class="form-control" placeholder="Category Name" name="name" value="{{ $category->name }}">
	                  	@if($errors->has('name'))
	                  		<p style="color: red">{{ $errors->first('name') }}</p>
	                  	@endif
	                </div>
	                <div class="form-group">
	                	<label>Description</label>
	                	<textarea rows="6" cols="132" placeholder="Write something..." class="form-control" name="description"></textarea>
	            	</div>
              	</div>
              	<div class="box-footer">
                	<button type="submit" class="btn btn-primary" value="submit">Update</button>
              	</div>
            </form>
        </div>
	</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('.side0').addClass('active');
	});
</script>
@endsection