@extends('layouts.admin')
@section('title', 'Create Category')
@section('content')
	<div class="form large-9 medium-8 columns content">
		<div class="box box-primary">
            <div class="box-header with-border">
              	<h3 class="box-title">Create Category</h3>
            </div>
            <form action="{{ Route('category.store') }}" method="post" enctype="multipart/form-data">
            	@csrf
	            <div class="box-body">
	                <div class="form-group">
	                  	<label>Name</label>
	                  	<input type="text" class="form-control" placeholder="Product Name" name="name">
	                  	@if($errors->has('name'))
	                  		<p style="color: red">{{ $errors->first('name') }}</p>
	                  	@endif
	                </div>
	                <div class="form-group">
	                	<label>Description</label>
	                	<textarea rows="6" cols="132" placeholder="Write something..." class="form-control" name="description"></textarea>
	            	</div>
              	</div>
              	<div class="box-footer">
                	<button type="submit" class="btn btn-primary" value="Submit">Create</button>
              	</div>
            </form>
        </div>
	</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('.side0').addClass('active');
	});
</script>
@endsection