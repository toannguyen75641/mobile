@extends('layouts.admin')
@section('title', 'Contact Manager')
@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Contact Table</h3>
        </div>
        <div class="box-body">
          <table id="table" class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Subject</th>
                    <th>Message</th>
                    <th>Created_at</th>
                    @can('contacts.delete')
                        <th>Delete</th>
                    @endcan
                </tr>
            </thead>
            <tbody>
                @foreach($contacts as $contact)
                <tr>
                    <td><a href="{{ Route('customer.show', $contact->customer_id) }}">{{ $contact->name }}</a></td>
                    <td>{{ $contact->email }}</td>
                    <td>{{ $contact->subject }}</td>
                    <td>{{ $contact->message }}</td>
                    <td>{{ $contact->created_at }}</td>
                    @can('contacts.delete')
                        <td>
                            <a href="{{ Route('contacts.destroy', $contact->id) }}" onclick="return confirm('Are you sure?');"><i class="fas fa-trash" style="font-size: 20px"></i></a>
                        </td>
                    @endcan
                </tr> 
                @endforeach 
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
  
<script type="text/javascript">
    $(document).ready(function() {
        $('.side9').addClass('active');
    });
</script>
@endsection