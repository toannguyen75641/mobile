@extends('layouts.admin')
@section('title', 'Customer Details')
@section('content')
<div class="form large-9 medium-8 columns content">
	<div class="box box-primary">
        <div class="box-header with-border">
          	<h3 class="box-title">{{ $customer->name }}</h3>
        </div>
	    <table class="table table-bordered table-hover">
	        <tr>
	            <th scope="row">Phone</th>
	            <td>{{ $customer->phone }}</td>
	        </tr>
	        <tr>
	            <th scope="row">Email</th>
	            <td>{{ $customer->email }}</td>
	        </tr>
	        <tr>
	            <th scope="row">Gender</th>
	            <td>{{ $customer->gender }}</td>
	        </tr>
	        <tr>
	            <th scope="row">Day Of Birth</th>
	            <td>{{ $customer->birthday }}</td>
	        </tr>
	        <tr>
	            <th scope="row">Address</th>
	            <td>{{ $customer->address }}</td>
	        </tr>
	        <tr>
	            <th scope="row">Created_at</th>
	            <td>{{ $customer->created_at }}</td>
	        </tr>
	    </table>
	    <div class="box-body">
	    	<h4>Relate Orders</h4>
	    	<table id="table" class="table table-bordered table-hover">
	    		<thead>
		    		<tr>
		    			<th>Order ID</th>
		    			<th>Amount</th>
		    			<th>State</th>
		    			<th>Created_at</th>
		    		</tr>
		    	</thead>
				@foreach($orders as $order)
		    		<tr>
	    				<td><a href="{{ Route('order.show', $order->id) }}">{{ $order->id }}</a></td>
	    				<td>{{ $order->amount }}</td>
	    				<td>
	    					@switch($order->state)
	    						@case(1)
	    							Confirmed
	    							@break
	    						@case(2)
	    							Paid
	    							@break
	    						@case(3)
	    							Delivering
	    							@break
	    						@case(4)
	    							Delivered
	    							@break
							    @default
							        Order
							@endswitch
                        </td>
	    				<td>{{ $order->created_at }}</td>
		    		</tr>
    			@endforeach
	    	</table>
	    </div>
	</div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.side2').addClass('active');
    });
</script>
@endsection