@extends('layouts.admin')
@section('title', 'Customer Manager')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Customer Table</h3>
                    </div>
                    <div class="box-body">
                        <table id="table" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Gender</th>
                                    <th>Address</th>
                                    <th>Created_at</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($customers as $customer)
                                <tr>
                                    <td><a href="{{ Route('customer.show', $customer->id) }}">{{ $customer->name }}</a></td>
                                    <td>{{ $customer->phone }}</td>
                                    <td>{{ $customer->gender }}</td>
                                    <td>{{ $customer->address }}</td>
                                    <td>{{ $customer->created_at }}</td>
                                    <td>
                                        <a href="{{ Route('customer.destroy', $customer->id) }}" onclick="return confirm('Are you sure?');"><i class="fas fa-trash" style="font-size: 20px"></i></a>
                                    </td>
                                </tr> 
                                @endforeach 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
<script type="text/javascript">
    $(document).ready(function() {
        $('.side2').addClass('active');
    });
</script>
  
@endsection