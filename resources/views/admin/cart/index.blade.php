@extends('layouts.admin')
@section('title', 'Cart Manager')
@section('content')
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Cart Table</h3>
            </div>
            <div class="box-body">
              <table id="table" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Customer</th>
                        <th>Product</th>
                        <th>Image</th>
                        <th>Quantity</th>
                        <th>Stock Quantity</th>
                        <th>Price</th>
                        <th>Created_at</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($carts as $cart)
                    <tr>
                        <td><a href="{{ Route('customer.show', $cart->customer_id) }}">{{ $cart->customer_name }}</a></td>
                        <td><a href="{{ Route('product.show', $cart->product_id) }}">{{ $cart->name }}</a></td>
                        <td><img src="{{ asset($cart->image) }}" style="width: 100px; height: 100px"></td>
                        <td>{{ $cart->qty }}</td>
                        <td>{{ $cart->stock_qty }}</td>
                        <td>${{ $cart->price }}</td>
                        <td>{{ $cart->created_at }}</td>
                    </tr> 
                    @endforeach 
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
  
<script type="text/javascript">
    $(document).ready(function() {
        $('.side8').addClass('active');
    });
</script>
@endsection