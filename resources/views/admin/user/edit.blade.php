@extends('layouts.admin')
@section('title', 'Update User')
@section('content')
	<div class="form large-9 medium-8 columns content">
		<div class="box box-primary">
            <div class="box-header with-border">
              	<h3 class="box-title">Update User</h3>
            </div>
            <form action="{{ Route('user.update', $user->id) }}" method="post">
            	@csrf
	            <div class="box-body">
	                <div class="form-group">
	                  	<label>Name</label>
	                  	<input type="text" class="form-control" placeholder="User Name" name="name" value="{{ $user->name }}">
	                  	@if($errors->has('name'))
	                  		<p style="color: red">{{ $errors->first('name') }}</p>
	                  	@endif
	                </div>
	                <div class="form-group">
	                  	<label>Role</label>
	                  	<select name="role" class="form-control">
	                  		@isset($roles)
	                  			@foreach($roles as $role)
	                  				<option value="{{ $role->id }}"
	                  					@if(!$role_users->isEmpty())
	                  						@foreach($role_users as $role_user)
			                  					@if($role->id == $role_user->id)
			                  						selected="selected"
			                  					@endif
			                  				@endforeach
		                  				@endif>
	                  					{{ $role->name }}
	                  				</option>
	                  			@endforeach
	                  		@endisset
	                  	</select>
	                </div>

              	</div>
              	<div class="box-footer">
                	<button type="submit" class="btn btn-primary" value="submit">Update</button>
              	</div>
            </form>
        </div>
	</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.side4').addClass('active');
    });
</script>
@endsection