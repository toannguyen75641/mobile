@extends('layouts.admin')
@section('title', 'User Details')
@section('content')
<div class="form large-9 medium-8 columns content">
	<div class="box box-primary">
        <div class="box-header with-border">
          	<h3 class="box-title">{{ $user->name }}</h3>
          	<a href="{{ Route('user.edit', $user->id) }}"><i class="fas fa-pen-square" style="font-size: 18px"></i></a>
        </div>
	    <table class="table table-bordered table-hover">
	        <tr>
	            <th scope="row">Email</th>
	            <td>{{ $user->email }}</td>
	        </tr>
	        <tr>
	            <th scope="row">Role</th>
	            <td>
	            	@if(!$role_users->isEmpty())
	            		@foreach($role_users as $role_user)
	            			{{ $role_user->name }}
	            		@endforeach
	            	@endif
	            </td>
	        </tr>
	        <tr>
	            <th scope="row">Created_at</th>
	            <td>{{ $user->created_at }}</td>
	        </tr>
	    </table>
	</div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.side4').addClass('active');
    });
</script>
@endsection