@extends('layouts.admin')
@section('title', 'User Manager')
@section('content')
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">User Table</h3>
            </div>
            <div class="box-body">
              <table id="table" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Created_at</th>
                        <th>Updated_at</th>
                        @can('users.update', App\User::class)
                            <th>Edit</th>
                        @endcan
                        @can('users.delete', App\User::class)    
                            <th>Delete</th>
                        @endcan
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>{{ $user->name }}</td>
                        <td><a href="{{ Route('user.show', $user->id) }}">{{ $user->email }}</a></td>
                        <td>{{ $user->created_at }}</td>
                        <td>{{ $user->updated_at }}</td>
                        @can('users.update', App\User::class)
                            <td>
                                <a href="{{ Route('user.edit', $user->id) }}"><i class="fas fa-pen-square" style="font-size: 20px"></i></a>
                            </td>
                        @endcan
                        @can('users.delete', App\User::class)
                            <td>
                                @if($user->id !== $id)
                                    <a href="{{ Route('user.destroy', $user->id) }}" onclick="return confirm('Are you sure?');"><i class="fas fa-trash" style="font-size: 20px"></i></a>
                                @endif
                            </td>
                        @endcan
                    </tr> 
                    @endforeach 
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
<script type="text/javascript">
    $(document).ready(function() {
        $('.side4').addClass('active');
    });
</script>
  
@endsection