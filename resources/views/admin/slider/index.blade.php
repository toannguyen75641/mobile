@extends('layouts.admin')
@section('title', 'Slider Manager')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Slider Table</h3>
                    </div>
                    <div class="box-body">
                        <table id="table" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th>Created_by</th>
                                    <th>Created_at</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($sliders as $slider)
                                <tr>
                                    <td><a href="{{ Route('slider.show', $slider->id) }}">{{ $slider->name }}</a></td>
                                    <td>
                                        @if($slider->type == 0) 
                                            Slide Show
                                        @elseif($slider->type == 1)
                                            Banner News
                                        @elseif($slider->type == 2)
                                            Sidebar Right
                                        @elseif($slider->type == 3)
                                            Sidebar Left
                                        @endif
                                    </td>
                                    <td><a href="{{ Route('user.show', $slider->user_id) }}">{{ $slider->user_name }}</a></td>
                                    <td>{{ $slider->created_at }}</td>
                                    <td>
                                        @can('sliders.update', $slider)
                                            <a href="{{ Route('slider.edit', $slider->id) }}"><i class="fas fa-pen-square" style="font-size: 20px"></i></a>
                                        @endcan
                                    </td>
                                    <td>
                                        @can('sliders.delete', $slider)
                                            <a href="{{ Route('slider.destroy', $slider->id) }}" onclick="return confirm('Are you sure?');"><i class="fas fa-trash" style="font-size: 20px"></i></a>
                                        @endcan
                                    </td>
                                </tr> 
                                @endforeach 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
<script type="text/javascript">
    $(document).ready(function() {
        $('.side6').addClass('active');
    });
</script>
  
@endsection