@extends('layouts.admin')
@section('title', 'Create Slider')
@section('content')
	<div class="form large-9 medium-8 columns content">
		<div class="box box-primary">
        <div class="box-header with-border">
      		<h3 class="box-title">Create Slider</h3>
        </div>
        <form action="{{ Route('slider.store') }}" method="post" enctype="multipart/form-data">
        	@csrf
          <div class="box-body">
              <div class="form-group">
                	<label>Name</label>
                	<input type="text" name="name" class="form-control" placeholder="Name">
                	@if($errors->has('name'))
                		<p style="color: red;">{{ $errors->first('name') }}</p>
                	@endif
              </div>

              <div class="form-group">
                	<label>Type</label>
                	<select name="type" class="form-control" id="type">
                		<option value="0">Slide Show</option>
                		<option value="1">Banner News</option>
                		<option value="2">Sidebar Right</option>
                		<option value="3">Sidebar Left</option>
                	</select>
              </div>

              <div class="form-group">
                	<label>Image</label>
                	<input type="file" name="slides[]" multiple="multiple" class="form-control" id="inputMultiple">
                	@if($errors->has('slides.*'))
                		<p style="color: red;">{{ $errors->first('slides.*') }}</p>
                	@endif
              </div>

              <div class="col-sm-12 slides" id="slides"></div>
        	</div>

        	<div class="box-footer">
          	<button type="submit" class="btn btn-primary" value="Submit">Create</button>
        	</div>
        </form>
    </div>
	</div>
<script type="text/javascript" src="{{asset('js/slideValidate.js')}}"></script>
<script type="text/javascript">
	var countBannerNew = {!! $countBannerNew !!}
    function renderFile(file) {
		var fileReader = new FileReader();
    	fileReader.onload = function(event) {
    		var str = '<label>Order</label>' +
    				'<input type="number" name="order[]" class="order form-control" placeholder="Order">' +
    				'<label><input type="checkbox" name="status[]" value="1">Status</label>';
    		var col_sm_3 = $('<div>').addClass('col-sm-3');
    		if(file.type.match('image.*')) {
        		var img = $('<img>').addClass('img-thumbnail').css('width', '100%').css('height', '130px');
        		var imgSrc = event.target.result;
        		img.attr('src', imgSrc);
        		col_sm_3.append(img);
        	}
    		col_sm_3.append(str);
    		$('#slides').append(col_sm_3);
    	}
    	fileReader.readAsDataURL(file);
	}

    $(document).ready(function() {
        $('.side6').addClass('active');

		$('#inputMultiple').change(function () {
			$('.col-sm-3').remove();
        	var files = $(this)[0].files;
        	for(var i = 0; i < files.length; i++) {
        		renderFile(files[i]);
        	}
        });

   //      $('form').submit(function(event) {
   //      	var type = $('#type').val();
   //      	if(type == 1) {
   //      		if(countBannerNew = 3) {
   //                  return false;
   //      		}
   //      	}
			// return true;
   //      });
    });
</script>
@endsection