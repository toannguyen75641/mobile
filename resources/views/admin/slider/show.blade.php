@extends('layouts.admin')
@section('title', 'Slider Details')
@section('content')
<div class="form large-9 medium-8 columns content">
	<div class="box box-primary">
        <div class="box-header with-border">
          	<h3 class="box-title">{{ $slider->name }}</h3>
          	<a href="{{ Route('slider.edit', $slider->id) }}"><i class="fas fa-pen-square" style="font-size: 18px"></i></a>
        </div>
	    <table class="table table-bordered table-hover">
	        <tr>
	            <th scope="row">Type</th>
	            <td>
	            	@if($slider->type == 0) 
                        Slide Show
                    @elseif($slider->type == 1)
                        Banner News
                    @elseif($slider->type == 2)
                        Sidebar Right
                    @elseif($slider->type == 3)
                        Sidebar Left
                    @endif
	            </td>
	        </tr>
	        <tr>
	            <th scope="row">Created_at</th>
	            <td>{{ $slider->created_at }}</td>
	        </tr>
	        <tr>
	            <th scope="row">Updated_at</th>
	            <td>{{ $slider->updated_at }}</td>
	        </tr>
	    </table>
	    <div class="box-body">
	    	<div class="col-sm-12">
	    		@foreach($slides as $slide)
	    			<div class="col-sm-3">
	    				<img src="{{ asset($slide->url) }}" class="img-thumbnail img" data-id="{{ $slide->id }}">
	    				<label>Order</label>
	    				<span>{{ $slide->order }}</span>
	    				<label>Title</label>
	    				<span>{{ $slide->title }}</span>
	    				<label>Description</label>
	    				<span>{{ $slide->description }}</span>
	    				<label>Status</label>
	    				@if($slide->status == 1)
	    					<i class="fas fa-check-square"></i>
	    				@else
	    					<i class="fas fa-window-close"></i>
	    				@endif
	    			</div>
	    		@endforeach
	    	</div>
	    </div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('.side6').addClass('active');

		$('.img').click(function() {
        	var slide_id = $(this).attr('data-id');
        	var url = "{{ Route('slide.edit', ':id') }}";
        	url = url.replace(':id', slide_id);
        	document.location.href = url;
        });
	});
</script>
@endsection