@extends('layouts.admin')
@section('title', 'Update Slider')
@section('content')
	<div class="form large-9 medium-8 columns content">
		<div class="box box-primary">
            <div class="box-header with-border">
          		<h3 class="box-title">Update Slider</h3>
            </div>
            <form action="{{ Route('slider.update', $slider->id) }}" method="post" enctype="multipart/form-data">
            	@csrf
	            <div class="box-body">
	                <div class="form-group">
	                  	<label>Name</label>
	                  	<input type="text" name="name" class="form-control" placeholder="Name" value="{{ $slider->name }}">
	                </div>
	                <div class="form-group">
	                  	<label>Type</label>
	                  	<select name="type" class="form-control">
	                  		<option value="0" 
	                  		@if($slider->type == 0)
	                  			selected = 'selected'
	                  		@endif
	                  		>Banner</option>
	                  		<option value="1"
	                  		@if($slider->type == 1)
	                  			selected = 'selected'
	                  		@endif
	                  		>Banner News</option>
	                  		<option value="2"
	                  		@if($slider->type == 2)
	                  			selected = 'selected'
	                  		@endif
	                  		>Side Right</option>
	                  		<option value="3"
	                  		@if($slider->type == 3)
	                  			selected = 'selected'
	                  		@endif
	                  		>Sidebar Left</option>
	                  	</select>
	                </div>
	                <div class="form-group">
	                  	<label>Image</label>
	                  	<input type="file" name="slides[]" multiple="multiple" class="form-control" id="inputMultiple">
	                </div>
	                <p>New Slide</p>
	                <div class="col-sm-12 slides" id="slides"></div><br>
	                <p>Old Slide(Click checkbox to delete)</p>
	                <div class="col-sm-12 slides">
			    		@foreach($slides as $slide)
			    			<div class="col-sm-3">
			    				<img src="{{ asset($slide->url) }}" class="img-thumbnail img" data-id="{{ $slide->id }}">
			    				@can('slides.delete', $slide)
				    				<div class="remove image-action">
				    					<input type="checkbox" name="checkImage[]" value="{{ $slide->id }}" onclick="return confirm('Are you sure? Delete slide {{ $slide->id }}')">
				    				</div>
				    			@endcan
			    				<label>Order</label>
			    				<span>{{ $slide->order }}</span>
			    				<label>Title</label>
			    				<span>{{ $slide->title }}</span>
			    				<label>Description</label>
			    				<span>{{ $slide->description }}</span>
			    				<label>Status</label>
			    				@if($slide->status == 1)
			    					<i class="fas fa-check-square"></i>
			    				@else
			    					<i class="fas fa-window-close"></i>
			    				@endif
			    			</div>
	    				@endforeach
	    			</div>

              	</div>
              	<div class="box-footer">
                	<button type="submit" class="btn btn-primary" value="Submit">Update</button>
              	</div>
            </form>
        </div>
	</div>
	<script type="text/javascript" src="{{asset('js/slideValidate.js')}}"></script>
<script type="text/javascript">
    function renderFile(file) {
		var fileReader = new FileReader();
    	fileReader.onload = function(event) {
    		var str = '<label>Order</label>' +
    				'<input type="number" name="order[]" class="order form-control" placeholder="Order">' +
    				'<label><input type="checkbox" name="status[]" value="1">Status</label>';
    		var col_sm_3 = $('<div>').addClass('col-sm-3 create');
    		if(file.type.match('image.*')) {
        		var img = $('<img>').addClass('img-thumbnail').css('width', '100%').css('height', '130px');
        		var imgSrc = event.target.result;
        		img.attr('src', imgSrc);
        		col_sm_3.append(img);
        	}
    		col_sm_3.append(str);
    		$('#slides').append(col_sm_3);
    	}
    	fileReader.readAsDataURL(file);
	}
	
    $(document).ready(function() {
        $('.side6').addClass('active');

		$('#inputMultiple').change(function () {
			$('.create').remove();
        	var files = $(this)[0].files;
        	for(var i = 0; i < files.length; i++) {
        		renderFile(files[i]);
        	}
        });

        $('.img').click(function() {
        	var slide_id = $(this).attr('data-id');
        	var url = "{{ Route('slide.edit', ':id') }}";
        	url = url.replace(':id', slide_id);
        	document.location.href = url;
        });
    });
</script>
@endsection