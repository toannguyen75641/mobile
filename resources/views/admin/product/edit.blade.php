@extends('layouts.admin')
@section('title', 'Update Product')
@section('content')
	<div class="form large-9 medium-8 columns content">
		<div class="box box-primary">
            <div class="box-header with-border">
              	<h3 class="box-title">Update Product</h3>
            </div>
            <form action="{{ Route('product.update', $product->id) }}" method="post" enctype="multipart/form-data">
            	@csrf
	            <div class="box-body">
	                <div class="form-group">
	                  	<label>Name</label>
	                  	<input type="text" class="form-control" placeholder="Product Name" name="name" value="{{ $product->name }}">
	                  	@if($errors->has('name'))
	                  		<p style="color: red">{{ $errors->first('name') }}</p>
	                  	@endif
	                </div>
	                <div class="form-group">
	                  	<label>Category</label>
	                  	<select name="category_id" class="form-control">
	                  		@isset($category)
	                  			@foreach($category as $category)
	                  				<option value="{{ $category->id }}" >
	                  					{{ $category->name }}
	                  				</option>
	                  			@endforeach
	                  		@endisset
	                  	</select>
	                </div>
	                <div class="form-group">
	                  	<label>Price</label>
	                  	<input type="number" class="form-control" placeholder="Pirce" name="price" value="{{ $product->price }}">
	                  	@if($errors->has('price'))
	                  		<p style="color: red">{{ $errors->first('price') }}</p>
	                  	@endif
	                </div>
	                <div class="form-group">
	                  	<label>Quantity</label>
	                  	<input type="number" class="form-control" placeholder="Quantity" name="quantity" value="{{ $product->quantity }}">
	                  	@if($errors->has('quantity'))
	                  		<p style="color: red">{{ $errors->first('quantity') }}</p>
	                  	@endif
	                </div>
	                <div class="form-group">
	                	<label>Description</label>
	                	<textarea rows="6" cols="132" placeholder="Write something..." class="form-control" name="description"></textarea>
	            	</div>
	                <div class="form-group">
	                	<div class="col-sm-2" style="display: inline-block; padding-left: 0;">
		                  	<label>Image</label>
		                  	<br>
		                  	@isset($product->image)
		                  		<img src="{{asset($product->image)}}" class="img img-thumbnail">
		                  	@endisset
	                	</div>
	                	<div id="replace" style="display: inline-block;">
		                  	<label>Replace</label>
		                  	<br>
	                	</div>
	                  	<input type="file" name="image" class="form-control" id="inputFile">
	                  	@if($errors->has('image'))
	                  		<p style="color: red">{{ $errors->first('image') }}</p>
	                  	@endif
	                </div>
                	<div class="form-group">
	                  	<label>Album</label><br>
	                  	<p>New Image</p>
	                  	<div class="multiple col-sm-12 slides"></div>
	                  	<p>Old Image(Click checkbox to delete)</p>
              			<div class="col-sm-12 slides">
          					@if(!$imageAlbums->isEmpty())
			                	@foreach($imageAlbums as $imageAlbum)
			                	<div class="col-sm-2" style="width: 130px">
	                				<img src="{{asset($imageAlbum->url)}}" class="img img-thumbnail">
	                				<div class="remove image-action">
		                				<input type="checkbox" name="checkImage[]" value="{{ $imageAlbum->id }}">
		                			</div>	 
			                	</div>
			                	@endforeach
		                	@endif
              			</div>
              			<input type="file" name="album[]" multiple="multiple" class="form-control" id="inputMultiple">
	                  	@if($errors->has('album.*'))
	                  		<p style="color: red">{{ $errors->first('album.*') }}</p>
	                  	@endif
	                </div>
	                <div class="checkbox">
	                  	<label>
	                    	<input type="checkbox" name="status" value="{{ $product->status }}" 
	                    	@if($product->status == 1)
	                    		checked
	                    	@endif> 
	                    	Status
	                  	</label>
	                </div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" id="details"> Product Details
							</label>
						</div>
						<div id="caption">
			                <div class="form-group">
			                  	<label>Feature</label>
			                  	<input type="text" class="form-control" placeholder="Feature" name="feature" value="{{ $productDetail['feature'] }}">
			                </div>
			                <div class="form-group">
			                  	<label>Screen</label>
			                  	<input type="text" class="form-control" placeholder="Screen" name="screen" value="{{ $productDetail['screen'] }}">
			                </div>
			                <div class="form-group">
			                  	<label>Camera</label>
			                  	<input type="text" class="form-control" placeholder="Camera" name="camera" value="{{ $productDetail['camera'] }}">
			                </div>
			                <div class="form-group">
			                  	<label>Ram</label>
			                  	<input type="text" class="form-control" placeholder="Ram" name="ram" value="{{ $productDetail['ram'] }}">
			                </div>
			                <div class="form-group">
			                  	<label>Rom</label>
			                  	<input type="text" class="form-control" placeholder="Rom" name="rom" value="{{ $productDetail['rom'] }}">
			                </div>
			                <div class="form-group">
			                  	<label>Weight</label>
			                  	<input type="text" class="form-control" placeholder="Weight" name="weight" value="{{ $productDetail['weight'] }}">
			                </div>
			                <div class="form-group">
			                  	<label>Battery</label>
			                  	<input type="text" class="form-control" placeholder="Battery" name="battery" value="{{ $productDetail['battery'] }}">
			                </div>
		                </div>
					</div>
              	</div>
              	<div class="box-footer">
                	<button type="submit" class="btn btn-primary" value="submit">Update</button>
              	</div>
            </form>
        </div>
	</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('.side1').addClass('active');

		$('#caption').css('display', 'none');
		$('#details').click(function() {
			if($(this).is(':checked')) {
				$('#caption').css('display', 'block');
			}
			else {
				$('#caption').css('display', 'none');
			}
		});

	    $('#inputFile').change(function () {
			$('.create').remove();
        	var file = $(this)[0].files[0];
        	var fileReader = new FileReader();
        	fileReader.onload = function(event) {
        		var imgSrc = event.target.result;
        		var img = $('<img>').addClass('create img-thumbnail');
        		img.css('width', '100px');
        		img.css('height', '100px');
        		img.attr('src', imgSrc);
        		$('#replace').append(img);
        	}
        	fileReader.readAsDataURL(file);
        });

		function renderFile(file) {
			var fileReader = new FileReader();
			fileReader.onload = function(event) {
        		var imgSrc = event.target.result;
				var str = 	'<div class="create-multiple col-sm-2" style="width: 130px">' +
								'<img src="'+imgSrc+'" class="img img-thumbnail">' +
							'</div>';
        		$('.multiple').append(str);
			}
        	fileReader.readAsDataURL(file);
		}

		$('#inputMultiple').change(function () {
			$('.create-multiple').remove();
        	var files = $(this)[0].files;
        	for(var i = 0; i < files.length; i++) {
        		renderFile(files[i]);
        	}
        });
	});
</script>
@endsection