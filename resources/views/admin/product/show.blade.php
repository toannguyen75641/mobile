@extends('layouts.admin')
@section('title', 'Product Details')
@section('content')
<div class="form large-9 medium-8 columns content">
	<div class="box box-primary">
        <div class="box-header with-border">
          	<h3 class="box-title">{{ $product->name }}</h3>
          	<a href="{{ Route('product.edit', $product->id) }}"><i class="fas fa-pen-square" style="font-size: 18px"></i></a>
        </div>
        <ul class="nav nav-tabs">
		  	<li class="menu0 active">
		  		<a><i class="far fa-sticky-note"> Basic</i></a>
		  	</li>
		  	<li class="menu1">
		  		<a><i class="fas fa-info-circle"></i> Detail</a>
		  	</li>
		</ul>
		<aside id="product">
		    <table class="table table-bordered table-hover">
		        <tr>
		            <th scope="row">Category</th>
		            <td><a href="{{ Route('category.show', $product->category_id) }}">{{ $product->category_name }}</a></td>
		        </tr>
		        <tr>
		            <th scope="row">Price</th>
		            <td>{{ $product->price }}</td>
		        </tr>
		        <tr>
		            <th scope="row">Quantity</th>
		            <td>{{ $product->quantity }}</td>
		        </tr>
		        <tr>
		            <th scope="row">Description</th>
		            <td>{{ $product->description }}</td>
		        </tr>
		        <tr>
		            <th scope="row">Image</th>
		            <td>
		            	<div id="image">
		            		<img src="{{ asset($product->image) }}" style="height: 200px; width: 200px;" class="img-thumbnail">
		            	</div>
		            	<div id="album">
		            		@foreach($imageAlbums as $imageAlbum)
				            	<img src="{{ asset($imageAlbum->url) }}" style="height: 100px; width: 100px;" class="img-thumbnail">
				            @endforeach
		            	</div>
		            </td>
		        </tr>
		        <tr>
		            <th scope="row">Created_at</th>
		            <td>{{ $product->created_at }}</td>
		        </tr>
		        <tr>
		            <th scope="row">Status</th>
		            <td>
		            	@if($product->status == 1)
		            		Yes
		            	@else
		            		No
		            	@endif
		            </td>
		        </tr>
		    </table>
		</aside>
	    <aside id="detail">
		    <table class="table table-bordered table-hover">
		        <tr>
		            <th scope="row">Feature</th>
		            <td>{{ $productDetail['feature'] }}</td>
		        </tr>
		        <tr>
		            <th scope="row">Screen</th>
		            <td>{{ $productDetail['screen'] }}</td>
		        </tr>
		        <tr>
		            <th scope="row">Camera</th>
		            <td>{{ $productDetail['camera'] }}</td>
		        </tr>
		        <tr>
		            <th scope="row">Ram</th>
		            <td>{{ $productDetail['ram'] }}</td>
		        </tr>
		        <tr>
		            <th scope="row">Rom</th>
		            <td>{{ $productDetail['rom'] }}</td>
		        </tr>
		        <tr>
		            <th scope="row">Weight</th>
		            <td>{{ $productDetail['weight'] }}</td>
		        </tr>
		        <tr>
		            <th scope="row">Battery</th>
		            <td>{{ $productDetail['battery'] }}</td>
		        </tr>
		    </table>
	    </aside>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('.side1').addClass('active');

		$('#detail').css('display', 'none');

		$('.menu1').click(function() {
			$(this).addClass('active');
			$('.menu0').removeClass('active');
			$('#detail').css('display', 'block');
			$('#product').css('display', 'none');
		});

		$('.menu0').click(function() {
			$(this).addClass('active');
			$('.menu1').removeClass('active');
			$('#detail').css('display', 'none');
			$('#product').css('display', 'block');
		});
	});
</script>
@endsection