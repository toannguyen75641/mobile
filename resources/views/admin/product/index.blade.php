@extends('layouts.admin')
@section('title', 'Product Manager')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Product Table</h3>
                    </div>
                    <div class="box-body">
                        <table id="table" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Category</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Description</th>
                                    <th>Image</th>
                                    <th>Status</th>
                                    <th>Created_at</th>
                                    @can('products.update')
                                        <th>Edit</th>
                                    @endcan
                                    @can('products.delete')
                                        <th>Delete</th>
                                    @endcan
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($products as $product)
                                <tr>
                                    <td><a href="{{ Route('product.show', $product->id) }}">{{ $product->name }}</a></td>
                                    <td><a href="{{ Route('category.show', $product->category_id) }}">{{ $product->category_name }}</a></td>
                                    <td>{{ $product->price }}</td>
                                    <td>{{ $product->quantity }}</td>
                                    <td>{{ $product->description }}</td>
                                    <td>
                                        @if(!empty($product->image))
                                            <img src="{{asset($product->image)}}" style="width: 100px; height: 100px">
                                        @endif
                                    </td>
                                    <td>
                                        @if($product->status == 0)
                                            Unpublish
                                        @elseif($product->status == 1)
                                            Publish
                                        @endif
                                    </td>
                                    <td>{{ $product->created_at }}</td>
                                    @can('products.update')
                                        <td>
                                            <a href="{{ Route('product.edit', $product->id) }}"><i class="fas fa-pen-square" style="font-size: 20px"></i></a>
                                        </td>
                                    @endcan
                                    @can('products.delete')
                                        <td>
                                            <a href="{{ Route('product.destroy', $product->id) }}" onclick="return confirm('Are you sure?');"><i class="fas fa-trash" style="font-size: 20px"></i></a>
                                        </td>
                                    @endcan
                                </tr> 
                                @endforeach 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
<script type="text/javascript">
    $(document).ready(function() {
        $('.side1').addClass('active');
    });
</script>
  
@endsection