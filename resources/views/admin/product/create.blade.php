@extends('layouts.admin')
@section('title', 'Create Product')
@section('content')
	<div class="form large-9 medium-8 columns content">
		<div class="box box-primary">
            <div class="box-header with-border">
              	<h3 class="box-title">Create Product</h3>
            </div>
            <form action="{{ Route('product.store') }}" method="post" enctype="multipart/form-data">
            	@csrf
	            <div class="box-body">
	                <div class="form-group">
	                  	<label>Name</label>
	                  	<input type="text" class="form-control" placeholder="Product Name" name="name">
	                  	@if($errors->has('name'))
	                  		<p style="color: red">{{ $errors->first('name') }}</p>
	                  	@endif
	                </div>
	                <div class="form-group">
	                  	<label>Category</label>
	                  	<select name="category_id" class="form-control">
	                  		@isset($categories)
	                  			@foreach($categories as $category)
	                  				<option value="{{ $category->id }}">{{ $category->name }}</option>
	                  			@endforeach
	                  		@endisset
	                  	</select>
	                </div>
	                <div class="form-group">
	                  	<label>Price</label>
	                  	<input type="number" class="form-control" placeholder="Pirce" name="price">
	                  	@if($errors->has('price'))
	                  		<p style="color: red">{{ $errors->first('price') }}</p>
	                  	@endif
	                </div>
	                <div class="form-group">
	                  	<label>Quantity</label>
	                  	<input type="number" class="form-control" placeholder="Quantity" name="quantity">
	                  	@if($errors->has('quantity'))
	                  		<p style="color: red">{{ $errors->first('quantity') }}</p>
	                  	@endif
	                </div>
	                <div class="form-group">
	                	<label>Description</label>
	                	<textarea rows="6" cols="132" placeholder="Write something..." class="form-control" name="description"></textarea>
	            	</div>
	                <div class="form-group">
	                  	<label class="image">Image</label><br>
	                  	<input type="file" name="image" class="form-control" id="inputFile">
	                  	@if($errors->has('image'))
	                  		<p style="color: red">{{ $errors->first('image') }}</p>
	                  	@endif
	                </div>
	                <div class="form-group">
	                  	<label>Album</label>
              			<div class="multiple form-control"></div>
	                  	<input type="file" name="album[]" multiple="multiple" class="form-control" id="inputMultiple">
	                  	@if($errors->has('album.*'))
	                  		<p style="color: red">{{ $errors->first('album.*') }}</p>
	                  	@endif
	                </div>
	                <div class="checkbox">
	                  	<label>
	                    	<input type="checkbox" name="status" value="1"> Status
	                  	</label>
	                </div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" id="details"> Product Details
							</label>
						</div>
						<div id="caption">
			                <div class="form-group">
			                  	<label>Feature</label>
			                  	<input type="text" class="form-control" placeholder="Feature" name="feature">
			                </div>
			                <div class="form-group">
			                  	<label>Screen</label>
			                  	<input type="text" class="form-control" placeholder="Screen" name="screen">
			                </div>
			                <div class="form-group">
			                  	<label>Camera</label>
			                  	<input type="text" class="form-control" placeholder="Camera" name="camera">
			                </div>
			                <div class="form-group">
			                  	<label>Ram</label>
			                  	<input type="text" class="form-control" placeholder="Ram" name="ram">
			                </div>
			                <div class="form-group">
			                  	<label>Rom</label>
			                  	<input type="text" class="form-control" placeholder="Rom" name="rom">
			                </div>
			                <div class="form-group">
			                  	<label>Weight</label>
			                  	<input type="text" class="form-control" placeholder="Weight" name="weight">
			                </div>
			                <div class="form-group">
			                  	<label>Battery</label>
			                  	<input type="text" class="form-control" placeholder="Battery" name="battery">
			                </div>
		                </div>
					</div>
              	</div>
              	<div class="box-footer">
                	<button type="submit" class="btn btn-primary" value="Submit">Create</button>
              	</div>
            </form>
        </div>
	</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('.side1').addClass('active');

		$('#caption').css('display', 'none');
		$('#details').click(function() {
			if($(this).is(':checked')) {
				$('#caption').css('display', 'block');
			}
			else {
				$('#caption').css('display', 'none');
			}
		});

		$('#inputFile').change(function () {
			$('.img-thumbnail').remove();
        	var file = $(this)[0].files[0];
        	var fileReader = new FileReader();
        	fileReader.onload = function(event) {
        		var imgSrc = event.target.result;
        		var img = $('<img>').addClass('img-thumbnail');
        		img.css('width', '100px');
        		img.css('height', '100px');
        		img.attr('src', imgSrc);
        		$('#inputFile').before(img);
        	}
        	fileReader.readAsDataURL(file);
        });

		function renderFile(file) {
			var fileReader = new FileReader();
			fileReader.onload = function(event) {
        		var imgSrc = event.target.result;
        		var img = $('<img>').addClass('img img-thumbnail');
        		img.css('width', '100px');
        		img.css('height', '100px');
        		img.attr('src', imgSrc);
        		$('.multiple').append(img);
			}
        	fileReader.readAsDataURL(file);
		}

		$('#inputMultiple').change(function () {
			$('.img').remove();
        	var files = $(this)[0].files;
        	for(var i = 0; i < files.length; i++) {
        		renderFile(files[i]);
        	}
        });
	});
</script>
@endsection