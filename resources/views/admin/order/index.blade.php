@extends('layouts.admin')
@section('title', 'Order Manager')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Order Table</h3>
                    </div>
                    <div class="box-body">
                        <table id="table" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Customer</th>
                                    <th>Amount</th>
                                    <th>State</th>
                                    <th>Created_at</th>
                                    <th>Updated_at</th>
                                    @can('orders.update')
                                        <th>Edit</th>
                                    @endcan
                                    @can('orders.delete')
                                        <th>Delete</th>
                                    @endcan
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($orders as $order)
                                <tr>
                                    <td><a href="{{ Route('order.show', $order->id) }}">{{ $order->id }}</a></td>
                                    <td><a href="{{ Route('customer.show', $order->customer_id) }}">{{ $order->customer_name }}</a></td>
                                    <td>{{ $order->amount }}</td>
                                    <td>
                                        @if($order->state == 0) 
                                            Order
                                        @elseif($order->state == 1)
                                            Confirmed
                                        @elseif($order->state == 2)
                                            Paid
                                        @elseif($order->state == 3)
                                            Delivering
                                        @elseif($order->state == 4)
                                            Delivered
                                        @endif
                                    </td>
                                    <td>{{ $order->created_at }}</td>
                                    <td>{{ $order->updated_at }}</td>
                                    @can('orders.update')
                                        <td>
                                            <a href="{{ Route('order.edit', $order->id) }}"><i class="fas fa-pen-square" style="font-size: 20px"></i></a>
                                        </td>
                                    @endcan
                                    @can('orders.delete')
                                        <td>
                                            <a href="{{ Route('order.destroy', $order->id) }}" onclick="return confirm('Are you sure?');"><i class="fas fa-trash" style="font-size: 20px"></i></a>
                                        </td>
                                    @endcan
                                </tr> 
                                @endforeach 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
<script type="text/javascript">
    $(document).ready(function() {
        $('.side3').addClass('active');
    });
</script>
  
@endsection