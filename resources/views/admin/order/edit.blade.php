@extends('layouts.admin')
@section('title', 'Update Order')
@section('content')
	<div class="form large-9 medium-8 columns content">
		<div class="box box-primary">
            <div class="box-header with-border">
              	<h3 class="box-title">Update Order</h3>
            </div>
            <form action="{{ Route('order.update', $order->id) }}" method="post" enctype="multipart/form-data">
            	@csrf
	            <div class="box-body">
	                <div class="form-group">
	                  	<label>Customer</label>
	                  	<input type="text" class="form-control" placeholder="Customer Name" name="name" value="{{ $order->customer_name }}" disabled="disabled">
	                </div>
	                <div class="form-group">
	                	<label>Amount</label>
	                  	<input type="number" class="form-control" placeholder="Product Name" name="name" value="{{ $order->amount }}" disabled="disabled">
	            	</div>
	                <div class="form-group">
	                	<label>State</label>
	                  	<select class="form-control" name="state">
	                  		@for($i = 0; $i <= 4; $i++)
	                  			<option value="{{ $i }}"
		                  			@if($order->state == $i)
		                  				selected="selected"
		                  			@endif>
		                  			@switch($i)
					            		@case(1)
					            			Confirmed
					            			@break
					            		@case(2)
					            			Paid
					            			@break
					            		@case(3)
					            			Delivering
					            			@break
					            		@case(4)
					            			Delivered
					            			@break
				        				@default
				        					Order
					            	@endswitch
	                  			</option>
	                  		@endfor
	                  	</select>
	            	</div>
              	</div>
              	<div class="box-footer">
                	<button type="submit" class="btn btn-primary" value="submit">Update</button>
              	</div>
            </form>
        </div>
	</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.side3').addClass('active');
    });
</script>
@endsection