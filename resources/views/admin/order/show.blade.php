@extends('layouts.admin')
@section('title', 'Order Details')
@section('content')
<div class="form large-9 medium-8 columns content">
	<div class="box box-primary">
        <div class="box-header with-border">
          	<h3 class="box-title">Order {{ $order->id }}</h3>
          	<a href="{{ Route('order.edit', $order->id) }}"><i class="fas fa-pen-square" style="font-size: 18px"></i></a>
        </div>
	    <table class="table table-bordered table-hover">
	        <tr>
	            <th>Customer</th>
	            <td><a href="{{ Route('customer.show', $order->customer_id) }}">{{ $order->customer_name }}</a></td>
	        </tr>
	        <tr>
	        	<th>Product List</th>
        		<td>
		        	<div class="box-body">
		        	@foreach($orderDetails as $orderDetail)
			        	<div class="form-group">
			        		<label class="order-detail">Product Name</label>
			        		<span>:</span>
	        				<a href="{{ Route('product.show', $orderDetail->product_id) }}">{{ $orderDetail->product_name }}</a>
			        	</div>
			        	<div class="form-group">
			        		<label class="order-detail">Price</label>
			        		<span>:</span>
	        				{{ $orderDetail->price }}
			        	</div>
			        	<div class="form-group">
			        		<label class="order-detail">Quantity</label>
			        		<span>:</span>
	        				{{ $orderDetail->quantity }}
			        	</div>
			        	<div class="form-group">
			        		<label class="order-detail">Amount</label>
			        		<span>:</span>
	        				{{ $orderDetail->amount }}
			        	</div>
			        	<hr>
		        	@endforeach
		        	</div>
        		</td>
	        </tr>
	        <tr>
	            <th>Amount</th>
	            <td>{{ $order->amount }}</td>
	        </tr>
	        <tr>
	            <th>Created_at</th>
	            <td>{{ $order->created_at }}</td>
	        </tr>
	        <tr>
	            <th>State</th>
	            <td>
	            	@switch($order->state)
	            		@case(1)
	            			Confirmed
	            			@break
	            		@case(2)
	            			Paid
	            			@break
	            		@case(3)
	            			Delivering
	            			@break
	            		@case(4)
	            			Delivered
	            			@break
        				@default
        					Order
	            	@endswitch
	            </td>
	        </tr>
	    </table>
	</div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.side3').addClass('active');
    });
</script>
@endsection