@extends('layouts.admin')
@section('title', 'Update Slide')
@section('content')
	<div class="form large-9 medium-8 columns content">
		<div class="box box-primary">
            <div class="box-header with-border">
          		<h3 class="box-title">Update Slide</h3>
            </div>
            <!-- <ul class="nav nav-tabs">
			  	<li class="menu0 active">
			  		<a><i class="far fa-sticky-note"> Basic</i></a>
			  	</li>
			  	<li class="menu1">
			  		<a><i class="far fa-images"></i> Image</a>
			  	</li>
			</ul> -->
            <form action="{{ Route('slide.update', $slide->id) }}" method="post" enctype="multipart/form-data">
            	@csrf
	            <div class="box-body">
	                <div class="form-group">
	                	<div class="col-sm-2" style="display: inline-block; padding-left: 0;">
		                  	<label>Image</label>
		                  	<br>
		                  	@isset($slide->url)
		                  		<img src="{{asset($slide->url)}}" class="img img-thumbnail">
		                  	@endisset
	                	</div>
	                	<div id="replace" style="display: inline-block;">
		                  	<label>Replace</label>
		                  	<br>
	                	</div>
	                  	<input type="file" name="image" class="form-control" id="inputFile">
	                  	@if($errors->has('image'))
	                  		<p style="color: red">{{ $errors->first('image') }}</p>
	                  	@endif
	                </div>	                <div class="form-group">
	                  	<label>Order</label>
	                  	<input type="number" name="order" class="form-control" placeholder="Order" value="{{ $slide->order }}">
	                </div>
	                <div class="form-group">
	                  	<label>Title</label>
	                  	<input type="text" name="title" class="form-control" placeholder="Title" value="{{ $slide->title }}">
	                </div>
	                <div class="form-group">
	                	<label>Description</label>
	                  	<input type="text" name="description" class="form-control" placeholder="Description" value="{{ $slide->description }}">
	            	</div>
	                <div class="checkbox">
	                  	<label>
	                    	<input type="checkbox" name="status" value="{{ $slide->status }}"
	                    	@if($slide->status == 1)
	                    		checked
	                    	@endif
	                    	> Status
	                  	</label>
	                </div>
              	</div>
              	<div class="box-footer">
                	<button type="submit" class="btn btn-primary" value="Submit">Edit</button>
              	</div>
            </form>
        </div>
	</div>
<script type="text/javascript">
	$(document).ready(function() {
    	$('.side7').addClass('active');

	    $('#inputFile').change(function () {
			$('.create').remove();
	    	var file = $(this)[0].files[0];
	    	var fileReader = new FileReader();
	    	fileReader.onload = function(event) {
	    		var imgSrc = event.target.result;
	    		var img = $('<img>').addClass('create img-thumbnail');
	    		img.css('width', '100px');
	    		img.css('height', '100px');
	    		img.attr('src', imgSrc);
	    		$('#replace').append(img);
	    	}
	    	fileReader.readAsDataURL(file);
	    });
	});
</script>
@endsection