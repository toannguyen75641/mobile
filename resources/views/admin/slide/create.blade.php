@extends('layouts.admin')
@section('title', 'Create Slide')
@section('content')
	<div class="form large-9 medium-8 columns content">
		<div class="box box-primary">
            <div class="box-header with-border">
          		<h3 class="box-title">Create Slide</h3>
            </div>
            <form action="{{ Route('slide.store') }}" method="post" enctype="multipart/form-data">
            	@csrf
	            <div class="box-body">
	                <div class="form-group">
	                  	<label>Image</label>
	                  	<br>
	                  	<input type="file" name="image" class="form-control" id="inputFile">
	                  	@if($errors->has('image'))
	                  		<p style="color: red">{{ $errors->first('image') }}</p>
	                  	@endif
	                </div>
	                <div class="col-lg-6" style="padding-left: 0; padding-bottom: 15px">
	                  	<label>Order</label>
	                  	<input type="number" name="order" class="form-control" placeholder="Order">
	                </div>
	                <div class="col-lg-6" style="padding-right: 0; padding-bottom: 15px">
	                  	<label>Slider Name</label>
	                  	<select name="slider_id" class="form-control">
	                  		@foreach($sliders as $slider)
	                  			<option value="{{ $slider->id }}">{{ $slider->name }}</option>
	                  		@endforeach
	                  	</select>
	                </div>
	                <div class="form-group">
	                  	<label>Title</label>
	                  	<input type="text" name="title" class="form-control" placeholder="Title">
	                </div>
	                <div class="form-group">
	                	<label>Description</label>
	                  	<input type="text" name="description" class="form-control" placeholder="Description">
	            	</div>
	                <div class="checkbox">
	                  	<label>
	                    	<input type="checkbox" name="status" value="1"> Status
	                  	</label>
	                </div>
              	</div>
              	<div class="box-footer">
                	<button type="submit" class="btn btn-primary" value="Submit">Create</button>
              	</div>
            </form>
        </div>
	</div>
<script type="text/javascript">
	$(document).ready(function() {
    	$('.side7').addClass('active');

		$('#inputFile').change(function () {
			$('.img-thumbnail').remove();
        	var file = $(this)[0].files[0];
        	var fileReader = new FileReader();
        	fileReader.onload = function(event) {
        		var imgSrc = event.target.result;
        		var img = $('<img>').addClass('img-thumbnail');
        		img.css('width', '100px');
        		img.css('height', '100px');
        		img.attr('src', imgSrc);
        		$('#inputFile').before(img);
        	}
        	fileReader.readAsDataURL(file);
        });
    });
</script>
@endsection