@extends('layouts.admin')
@section('title', 'Slide Manager')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Slide Table</h3>
                    </div>
                    <div class="box-body">
                        <table id="table" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Slider Name</th>
                                    <th>Order</th>
                                    <th>Status</th>
                                    <th>Created_by</th>
                                    <th>Created_at</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($slides as $slide)
                                <tr>
                                    <td>
                                        @if(!empty($slide->image))
                                            <img src="{{asset($slide->url)}}" style="width: 100px; height: 100px">
                                        @endif
                                    </td>
                                    <td><a href="{{ Route('slider.show', $slide->slider_id) }}">{{ $slide->slider_name }}</a></td>
                                    <td>{{ $slide->order }}</td>
                                    <td>
                                        @if($slide->status == 0)
                                            Unpublish
                                        @elseif($slide->status == 1)
                                            Publish
                                        @endif
                                    </td>
                                    <td><a href="{{ Route('user.show', $slide->user_id) }}">{{ $slide->user_name }}</a></td>
                                    <td>{{ $slide->created_at }}</td>
                                    <td>
                                        @can('slides.update', $slide)
                                            <a href="{{ Route('slide.edit', $slide->id) }}"><i class="fas fa-pen-square" style="font-size: 20px"></i></a>
                                        @endcan
                                    </td>
                                    <td>
                                        @can('slides.delete', $slide)
                                            <a href="{{ Route('slide.destroy', $slide->id) }}" onclick="return confirm('Are you sure?');"><i class="fas fa-trash" style="font-size: 20px"></i></a>
                                        @endcan
                                    </td>
                                </tr> 
                                @endforeach 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
<script type="text/javascript">
    $(document).ready(function() {
        $('.side7').addClass('active');
    });
</script>
  
@endsection