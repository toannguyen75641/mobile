@extends('layouts.login')

@section('content')

<div class="login-box">
    <div class="login-logo">
      <a>My Shop<b> Admin</b></a>
    </div>
    <div class="login-box-body">
        <h3 class="login-box-msg">Login</h3>
        <div class="card-body">
            <form method="POST" action="{{ route('admin.login') }}">
                @csrf
                <div class="form-group has-feedback">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong style="color: red">{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group has-feedback">
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="row">
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">
                            {{ __('Login') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <br><a href="{{ Route('admin.register') }}" class="text-center">Register</a>
    </div>
</div>
@endsection
