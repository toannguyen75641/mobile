 <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="treeview side0">
          <a>
            <span>Categories</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ Route('category.index') }}"><i class="fas fa-list-alt"></i> List</a></li>
            @can('categories.create')
              <li><a href="{{ Route('category.create') }}"><i class="fas fa-plus-circle"></i> Add</a></li>
            @endcan
          </ul>
        </li>

        <li class="treeview side1">
          <a>
            <span>Products</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ Route('product.index') }}"><i class="fas fa-list-alt"></i> List</a></li>
            @can('products.create')
              <li><a href="{{ Route('product.create') }}"><i class="fas fa-plus-circle"></i> Add</a></li>
            @endcan
          </ul>
        </li>

        <li class="treeview side2">
          <a>
            <span>Customers</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ Route('customer.index') }}"><i class="fas fa-list-alt"></i> List</a></li>
          </ul>
        </li>

        <li class="treeview side9">
          <a>
            <span>Contacts</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ Route('contacts.index') }}"><i class="fas fa-list-alt"></i> List</a></li>
          </ul>
        </li>

        <li class="treeview side8">
          <a>
            <span>Carts</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ Route('carts.index') }}"><i class="fas fa-list-alt"></i> List</a></li>
          </ul>
        </li>

        <li class="treeview side3">
          <a>
            <span>Orders</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ Route('order.index') }}"><i class="fas fa-list-alt"></i> List</a></li>
          </ul>
        </li>

        <li class="treeview side4">
          <a>
            <span>Users</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ Route('user.index') }}"><i class="fas fa-list-alt"></i> List</a></li>
          </ul>
        </li>

        <li class="treeview side5">
          <a>
            <span>Roles</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ Route('role.index') }}"><i class="fas fa-list-alt"></i> List</a></li>
            @can('roles.create')
              <li><a href="{{ Route('role.create') }}"><i class="fas fa-plus-circle"></i> Add</a></li>
            @endcan
          </ul>
        </li>

        <li class="treeview side6">
          <a>
            <span>Sliders</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ Route('slider.index') }}"><i class="fas fa-list-alt"></i> List</a></li>
            @can('sliders.create')
              <li><a href="{{ Route('slider.create') }}"><i class="fas fa-plus-circle"></i> Add</a></li>
            @endcan
          </ul>
        </li>

        <li class="treeview side7">
          <a>
            <span>Slides</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ Route('slide.index') }}"><i class="fas fa-list-alt"></i> List</a></li>
            @can('slides.create')
              <li><a href="{{ Route('slide.create') }}"><i class="fas fa-plus-circle"></i> Add</a></li>
            @endcan
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>