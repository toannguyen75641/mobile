@if(session('success'))
	<div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ session('success') }}</strong>
	</div>
@endif

@if(session('fail'))
	<div class="alert alert-danger alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ session('fail') }}</strong>
	</div>
@endif