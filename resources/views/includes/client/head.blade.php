<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<title>My Shop</title>
 		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">
		<link rel="stylesheet" href="{{asset('css/bootstrap/dist/css/bootstrap.min.css')}}">
		<link rel="stylesheet" href="{{asset('css/font-awesome/css/font-awesome.min.css')}}">
		<link type="text/css" rel="stylesheet" href="{{asset('css/slick.css')}}"/>
		<link type="text/css" rel="stylesheet" href="{{asset('css/slick-theme.css')}}"/>
		<link type="text/css" rel="stylesheet" href="{{asset('css/nouislider.min.css')}}"/>
		<link type="text/css" rel="stylesheet" href="{{asset('css/style.css')}}"/>
		<link type="text/css" rel="stylesheet" href="{{asset('css/client.css')}}"/>
		<script src="{{asset('js/jquery.min.js')}}"></script>
		<script src="{{asset('js/js.cookie.js')}}"></script>

    </head>
	<body>