<nav id="navigation">
	<div class="container">
		<div id="responsive-nav">
			<ul class="main-nav nav navbar-nav">
				<li class="home">
					<a href="{{ Route('home') }}">Home</a>
				</li>
				<li class="store">
					<a href="{{ Route('store.index') }}">Store</a>
				</li>
				<li class="cart">
					<a href="{{ Route('cart.index') }}">Your Cart</a>
				</li>
				@if(!Auth::guest())
					<li class="contact">
						<a href="{{ Route('contact.index') }}">Contact</a>
					</li>
				@endif
			</ul>
		</div>
	</div>
</nav>