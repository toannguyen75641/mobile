<header>
	<div id="top-header">
		<div class="container">
			<ul class="header-links pull-left">
				<li><a href=""><i class="fa fa-phone"></i> +84909116097</a></li>
				<li><a href=""><i class="fa fa-envelope-o"></i> toannguyen75641@gmail.com</a></li>
				<li><a href=""><i class="fa fa-map-marker"></i> 58 Tố Hữu</a></li>
			</ul>
			<ul class="header-links pull-right">
				@if(Auth::guest())
					<li>
						<a href="{{ route('login') }}"><i class="fa fa-user-o"></i> Login</a>
						<span style="color: white;">|</span>
						<a href="{{ route('register') }}"> Register</a>
					</li>
				@else
					<li>
						<a href="">{{ Auth::user()->name }}</a>
						<span style="color: white;">|</span>
						<a href="{{ Route('logout') }}" 
							onclick="event.preventDefault(); 
										document.getElementById('logout-form').submit();">
							Sign out
	                  	</a>

	                  	<form id="logout-form" action="{{ Route('logout') }}" method="POST" style="display: none;">
	                      	@csrf
	                  	</form>
	              	</li>
				@endif
			</ul>
		</div>
	</div>
	<div id="header">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<div class="header-logo">
						<a href="#" class="logo">
							<img src="">
						</a>
					</div>
				</div>

				<!-- SEARCH BAR -->
				<div class="col-md-6">
					<div class="header-search">
						<form method="get" action="{{ Route('search') }}">
							<select class="input-select" name="category">
								<option value="0">All Categories</option>
								@foreach($categories as $category)
									<option value="{{ $category->id }}"
										@isset($cate)
											@if($cate == $category->id)
												selected="selected"
		                  					@endif
										@endisset>
										{{ $category->name }}
									</option>
								@endforeach
							</select>
							<input class="input" type="text" placeholder="Search here" name="keyword" 
							@isset($keyword)
								value="{{ $keyword }}"
							@endisset>
							<button type="submit" class="search-btn">Search</button>
						</form>
					</div>
				</div>
				<!-- /SEARCH BAR -->

				<!-- ACCOUNT -->
				<div class="col-md-3 clearfix">
					<div class="header-ctn">
						<!-- Cart -->
						<div class="dropdown" id="shoppingCart">
							<a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
								<i class="fa fa-shopping-cart"></i>
								<span>Your Cart</span>
								@isset($totalQty)
									<div class="qty" id="quantity">{{ $totalQty }}</div>
								@endisset
							</a>
							@if(isset($cartData) && !empty($cartData))
								<div class="cart-dropdown">
									<div class="cart-list">
										@foreach($cartData as $cartItem)
											<div class="product-widget">
												<div class="product-img">
													<img src="{{asset($cartItem['image'])}}">
												</div>
												<div class="product-body">
													<h3 class="product-name"><a href="{{ Route('product', $cartItem['product_id']) }}">{{ $cartItem['name'] }}</a></h3>
													<h4 class="product-price"><span class="qty">{{ $cartItem['qty'] }}x</span>${{ $cartItem['price'] }}</h4>
												</div>
												<button class="delete" data-id="{{ $cartItem['product_id'] }}"><i class="fa fa-close"></i></button>
											</div>
										@endforeach
									</div>
									<div class="cart-summary">
										<small>{{ $totalQty }} Item(s) selected</small>
										<h5>SUBTOTAL: ${{ $subtotal }}</h5>
									</div>
									<div class="cart-btns">
										<a href="{{ Route('cart.index') }}">View Cart</a>
										<a href="{{ Route('checkout.index') }}">Checkout <i class="fa fa-arrow-circle-right"></i></a>
									</div>
								</div>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>