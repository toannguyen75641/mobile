@extends('layouts.client')
@section('content')
<div class="section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="section-title">
					<h3 class="title">Your Cart</h3>
				</div>
			</div>
			@if(isset($cartData) && !empty($cartData))
				<div class="col-md-12" id="content">
					<div class="col-md-9">
						<div class="row" id="cart">
							@foreach($cartData as $cartItem)
								<div class="product product-cart">
									<div class="col-md-2 image">
										<img src="{{asset($cartItem['image'])}}">
									</div>
									<div class="col-md-10 product-body">
										<div class="col-md-8">
											<h3 class="product-name">{{ $cartItem['name'] }}</h3>
											<p class="action">
												<a class="delete btn-item-delete btn-link" data-id="{{ $cartItem['product_id']}}">Remove</a>
											</p>
										</div>
										<div class="col-md-2">
											<h4 class="product-price">${{ $cartItem['price'] }}</h4>
										</div>
										<div class="col-md-2 box-qty">
											<div class="input-number">
												<span class="qty-up" data-id="{{ $cartItem['product_id'] }}">+</span>
												<span class="qty-down" data-id="{{ $cartItem['product_id'] }}">-</span>
												<input type="number" class="number-qty" value="{{ $cartItem['qty'] }}" disabled="disabled">
												<input type="hidden" class="stock-qty" value="{{ $cartItem['stock_qty'] }}">
											</div>
										</div>
									</div>
								</div>
							@endforeach
						</div>
					</div>
					<div class="col-md-3">
						<div class="product">
							<div class="order-total">
								<span class="text-label"><strong>TOTAL:</strong></span>
								<div class="order-price">
									<strong>${{ $subtotal }}</strong>
								</div>
							</div>
						</div>
						<div>
							<button class="btn-order primary-btn">Order</button>
						</div>
					</div>
				</div>
			@endif
		</div>
	</div>
</div>
<script type="text/javascript" src="{{asset('js/cart.js')}}"></script>
<script type="text/javascript">
	var urlDel = "{{ Route('cart.delete') }}";

	function increaseQty(cart) {
		$(".input-number .qty-up").click(function(){
			var old_qty = $(this).siblings(".number-qty").val();
			var product_qty = $(this).siblings(".stock-qty").val();
			var new_qty = parseInt(old_qty) + 1;
			new_qty = (new_qty > product_qty) ? product_qty : new_qty;
			var id = $(this).attr('data-id');
			getAjax(cart, new_qty, id);

		});
	}

	function reduceQty(cart) {
		$(".input-number .qty-down").click(function(){
			var old_qty = $(this).siblings(".number-qty").val();
			var new_qty = parseInt(old_qty) - 1;
			new_qty = (new_qty < 1) ? 1 : new_qty;
			var id = $(this).attr('data-id');
			getAjax(cart, new_qty, id);
		});
	}

	function getAjax(cart, new_qty, id) {
		var loader = $("<div>").addClass("loader");
		var loader_img = $("<div>").addClass('loader_img');

		var position_key = false;
		$.each(cart, function(i, val) {
			if(val.product_id == id) {
				position_key = i;
				return(i =! position_key);
			}
		});

		if(position_key !== false) {
			cart[position_key]['qty'] = new_qty; 
		}

		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			type: 'patch',
			url: "{{ Route('cart.update') }}",
			data: {"id" : id, "new_qty" : new_qty},
			beforeSend: function() {
				$("body").append(loader);
				$("body").append(loader_img);
			},
			success: function(res) {
				$('div').removeClass("loader");
		    	$('div').removeClass("loader_img");
		    	if($.isEmptyObject(res)) {
		    		Cookies.set('cart', cart, { expires: 1 });
		    	}
		    	else {
		    		Cookies.set('cart', res, { expires: 1 });
		    	}
				var current_cart_after_delete = JSON.parse(Cookies.get('cart'));
				$(this).siblings(".number-qty").val(new_qty);
		    	appendWidgetElement(current_cart_after_delete);
				appendCartElement(current_cart_after_delete);
			},
		});
	}
	$(document).ready(function() {
		$('.cart').addClass('active');
		var current_cart = Cookies.get('cart');
		if(typeof current_cart !== "undefined") {
			var cart = JSON.parse(Cookies.get('cart'));
		}
		increaseQty(cart);
		reduceQty(cart);
		$('.btn-order').click(function() {
			location.href = "{{ Route('checkout.index') }}";
		});
	});
</script>

@endsection