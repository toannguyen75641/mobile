<div class="dropdown" id="shoppingCart">
	<a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
		<i class="fa fa-shopping-cart"></i>
		<span>Your Cart</span>
		<div class="qty" id="quantity">{{ $totalQty }}</div>
	</a>
	<div class="cart-dropdown">
		<div class="cart-list">
			@foreach($cartData as $cartItem)
				<div class="product-widget">
					<div class="product-img">
						<img src="{{asset($cartItem['image'])}}">
					</div>
					<div class="product-body">
						<h3 class="product-name">
							<a href="{{ Route('product', $cartItem['product_id']) }}">{{ $cartItem['name'] }}</a>
						</h3>
						<h4 class="product-price">
							<span class="qty">{{ $cartItem['qty'] }}x</span>
							${{ $cartItem['price'] }}
						</h4>
					</div>
					<button class="delete" data-id="{{ $cartItem['product_id'] }}"><i class="fa fa-close"></i></button>
				</div>
			@endforeach
		</div>
		<div class="cart-summary">
			<small>{{ $totalQty }} Item(s) selected</small>
			<h5>SUBTOTAL: ${{ $subtotal }}</h5>
		</div>
		<div class="cart-btns">
			<a href="{{ Route('cart.index') }}">View Cart</a>
			<a href="{{ Route('checkout.index') }}">Checkout <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
</div>