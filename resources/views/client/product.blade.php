@extends('layouts.client')
@section('content')
<div class="section">
	<div class="container">
		<div class="row">
			<div class="col-md-5 col-md-push-2">
				<div id="product-main-img">
					@foreach($imageAlbums as $imageAlbum)
						<div class="product-preview">
							<img src="{{asset($imageAlbum->url)}}">
						</div>
					@endforeach
				</div>
			</div>
			<div class="col-md-2  col-md-pull-5">
				<div id="product-imgs">
					@foreach($imageAlbums as $imageAlbum)
						<div class="product-preview">
							<img src="{{asset($imageAlbum->url)}}">
						</div>
					@endforeach
				</div>
			</div>
			<div class="col-md-5">
				<div class="product-details">
					<h2 class="product-name">{{ $product->name }}</h2>
					<div>
						<h3 class="product-price">${{ $product->price }}</h3>
						<span class="product-available">In Stock</span>
					</div>
					<p>{{ $product->description }}</p>

					<div class="add-to-cart">
						<div class="qty-label">
							Qty
							<div class="input-number">
								<input type="number" id="qty" disabled="disabled">
								<span class="qty-up">+</span>
								<span class="qty-down">-</span>
							</div>
						</div>
						<button class="add-to-cart-btn" data-id="{{ $product->id }}" data-name="{{ $product->name }}" data-image="{{ $product->image }}" data-price="{{ $product->price }}" data-qty="{{ $product->quantity }}"><i class="fa fa-shopping-cart"></i> add to cart</button>
					</div>

					<ul class="product-links">
						<li>Category: 
							<a href="{{ Route('store.id', $product->category_id) }}">{{ $product->category_name }}</a>
						</li>
					</ul>

				</div>
			</div>
			<div class="col-md-12">
				<div id="product-tab">
					<ul class="tab-nav">
						<li class="active">
							<a data-toggle="tab" href="#tab1">Description</a>
						</li>
						<li>
							<a data-toggle="tab" href="#tab2">Details</a>
						</li>
					</ul>
					<div class="tab-content">
						<div id="tab1" class="tab-pane fade in active">
							<div class="row">
								<div class="col-md-12">
									<p>{{ $product->description }}</p>
								</div>
							</div>
						</div>
						<div id="tab2" class="tab-pane fade in">
							<div class="row">
								<div class="col-md-12">
									@isset($productDetail)
									    <table class="table table-bordered">
									        <tr>
									            <th scope="row">Feature</th>
									            <td>{{ $productDetail['feature'] }}</td>
									        </tr>
									        <tr>
									            <th scope="row">Screen</th>
									            <td>{{ $productDetail['screen'] }}</td>
									        </tr>
									        <tr>
									            <th scope="row">Camera</th>
									            <td>{{ $productDetail['camera'] }}</td>
									        </tr>
									        <tr>
									            <th scope="row">Ram</th>
									            <td>{{ $productDetail['ram'] }}</td>
									        </tr>
									        <tr>
									            <th scope="row">Rom</th>
									            <td>{{ $productDetail['rom'] }}</td>
									        </tr>
									        <tr>
									            <th scope="row">Weight</th>
									            <td>{{ $productDetail['weight'] }}</td>
									        </tr>
									        <tr>
									            <th scope="row">Battery</th>
									            <td>{{ $productDetail['battery'] }}</td>
									        </tr>
									    </table>
									@endisset
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- <div class="section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="section-title text-center">
					<h3 class="title">Related Products</h3>
				</div>
			</div>
			<div class="col-md-3 col-xs-6">
				<div class="product">
					<div class="product-img">
						<img src="">
					</div>
					<div class="product-body">
						<p class="product-category"></p>
						<h3 class="product-name"></h3>
						<h4 class="product-price"></h4>
					</div>
					<div class="add-to-cart">
						<button class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> add to cart</button>
					</div>
				</div>
			</div>
			<div class="clearfix visible-sm visible-xs"></div>
		</div>
	</div>
</div> -->
<script type="text/javascript">
	var url = "{{ Route('cart.add') }}";
	var urlDel = "{{ Route('cart.delete') }}";
	var cart_db = {!! $cart !!};
	var product_qty = {!! $product->quantity !!};
	$(document).ready(function() {
		$('.store').addClass('active');

		var qty = $('#qty');
		qty.val(1);

		$('.qty-up').click(function() {
			var value = parseInt(qty.val()) + 1;
			value = (value > product_qty) ? product_qty : value;
			qty.val(value);
		});

		$('.qty-down').click(function() {
			var value = parseInt(qty.val()) - 1;
			value = (value < 1) ? 1 : value;
			qty.val(value);
		});
	});
</script>
<script type="text/javascript" src="{{asset('js/cart.js')}}"></script>
@endsection