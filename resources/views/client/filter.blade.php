<div class="row">
	@foreach($products as $product)
		<div class="col-md-4 col-xs-6">
			<div class="product">
				<a href="{{ Route('product', $product->id) }}">
					<div class="product-img">
						<img src="{{asset($product->image)}}">
					</div>
					<div class="product-body">
						<p class="product-category">{{ $product->category_name }}</p>
						<h3 class="product-name">{{ $product->name }}</h3>
						<h4 class="product-price">${{ $product->price }}</h4>
						<input type="hidden" id="qty" value="1">
					</div>
				</a>
				<div class="add-to-cart">
					<button class="add-to-cart-btn" data-id="{{ $product->id }}" data-name="{{ $product->name }}" data-image="{{ $product->image }}" data-price="{{ $product->price }}"><i class="fa fa-shopping-cart"></i> add to cart</button>
				</div>
			</div>
		</div>
	@endforeach
</div>
{{ $products->links('vendor.pagination.default') }}