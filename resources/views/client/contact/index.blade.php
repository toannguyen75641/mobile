@extends('layouts.client')
@section('content')
<div class="section">
	<div class="container">
		<div class="row">
			<form method="post" action="{{ Route('contact.sendmail') }}">
				@csrf
				<div class="section-title">
					<h3 class="title">Contact Us</h3>
				</div>
				<div class="form-group">
					<input class="input" type="hidden" name="name" value="{{ $user->name }}">
				</div>
				<div class="form-group">
					<input class="input" type="hidden" name="email" value="{{ $user->email }}">
				</div>
				<div class="form-group">
					<input class="input" type="text" name="subject" class="form-control" placeholder="Subject" >
                  	@if($errors->has('subject'))
                  		<p style="color: red">{{ $errors->first('subject') }}</p>
                  	@endif
				</div>
				<div class="form-group">
					<textarea name="message" class="form-control" rows="15" cols="40" placeholder="Write Something..."></textarea>
                  	@if($errors->has('message'))
                  		<p style="color: red">{{ $errors->first('message') }}</p>
                  	@endif
				</div>
				<button type="submit" class="btn-order primary-btn order-submit" value="submit">Send Mail</button>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('.contact').addClass('active');
	});
</script>
<script type="text/javascript" src="{{asset('js/cart.js')}}"></script>
@endsection