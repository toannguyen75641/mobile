@extends('layouts.client')
@section('content')
<!-- SlideShow -->
@if(!$sliders->isEmpty())
	<div class="top section">
		<div class="slideshow">
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
				<!-- <ol class="carousel-indicators">
					<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
					<li data-target="#myCarousel" data-slide-to="1"></li>
					<li data-target="#myCarousel" data-slide-to="2"></li>
				</ol> -->
			    <div class="carousel-inner">
			    	@foreach($sliders as $slider)
			    		@if($slider->type == 0)
					      	<div class="item">
					        	<img src="{{asset($slider->url)}}" width="100%" height="300">
					      	</div>
					    @endif
			      	@endforeach
			    </div>

			    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
			      	<span class="glyphicon glyphicon-chevron-left"></span>
			      	<span class="sr-only">Previous</span>
			    </a>
			    <a class="right carousel-control" href="#myCarousel" data-slide="next">
			    	<span class="glyphicon glyphicon-chevron-right"></span>
			      	<span class="sr-only">Next</span>
			    </a>
			</div>
	  	</div>
	  	<div class="banner-news">
	  		<div class="banner">
	  			@foreach($sliders as $slider)
	  				@if($slider->type == 1)
	  					<img src="{{asset($slider->url)}}" width="100%" height="90">
	  				@endif
	  			@endforeach
	  		</div>
	  	</div>
  	</div>
@endif
<!-- /SlideShow -->
<!-- SECTION -->
<div class="section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="section-title">
					<h3 class="title">New Products</h3>
				</div>
			</div>
			<div class="col-md-12">
				<div class="row">
					<div class="products-tabs">
						<div id="tab1" class="tab-pane active">
							<div class="products-slick" data-nav="#slick-nav-1">
								@foreach($newProducts as $newProduct)
									<div class="product">
										<a href="{{ Route('product', $newProduct->id) }}">
											<div class="product-img">
												<img src="{{asset($newProduct->image)}}" alt="">
											</div>
											<div class="product-body">
												<p class="product-category">{{ $newProduct->category_name }}</p>
												<h3 class="product-name">{{ $newProduct->name }}</h3>
												<h4 class="product-price">${{ $newProduct->price }} <!-- <del class="product-old-price">$990.00</del> --></h4>
												<input type="hidden" id="qty" value="1">
											</div>
										</a>
										<div class="add-to-cart">
											<button class="add-to-cart-btn" data-id="{{ $newProduct->id }}" data-name="{{ $newProduct->name }}" data-image="{{ $newProduct->image }}" data-price="{{ $newProduct->price }}" data-qty="{{ $newProduct->quantity }}"><i class="fa fa-shopping-cart"></i> add to cart</button>
										</div>
									</div>
								@endforeach
							</div>
							<div id="slick-nav-1" class="products-slick-nav"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /SECTION -->

<!-- SECTION -->
<div class="section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="section-title">
					<h3 class="title">Top selling</h3>
				</div>
			</div>
			<div class="col-md-12">
				<div class="row">
					<div class="products-tabs">
						<div id="tab2" class="tab-pane fade in active">
							<div class="products-slick" data-nav="#slick-nav-2">
								@foreach($topProducts as $topProduct)
									<div class="product">
										<a href="{{ Route('product', $topProduct->id) }}">
											<div class="product-img">
												<img src="{{asset($topProduct->image)}}" alt="">
											</div>
											<div class="product-body">
												<p class="product-category">{{ $topProduct->category_name }}</p>
												<h3 class="product-name">{{ $topProduct->name }}</h3>
												<h4 class="product-price">${{ $topProduct->price }} <!-- <del class="product-old-price">$990.00</del> --></h4>
												<input type="hidden" id="qty" value="1">
											</div>
										</a>
										<div class="add-to-cart">
											<button class="add-to-cart-btn" data-id="{{ $topProduct->id }}" data-name="{{ $topProduct->name }}" data-image="{{ $topProduct->image }}" data-price="{{ $topProduct->price }}" data-qty="{{ $topProduct->quantity }}"><i class="fa fa-shopping-cart"></i> add to cart</button>
										</div>
									</div>
								@endforeach
							</div>
							<div id="slick-nav-2" class="products-slick-nav"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /SECTION -->

<!-- SECTION -->
<div class="section">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-xs-6">
				<div class="section-title">
					<h4 class="title">Top selling</h4>
					<div class="section-nav">
						<div id="slick-nav-3" class="products-slick-nav"></div>
					</div>
				</div>

				<div class="products-widget-slick" data-nav="#slick-nav-3">
					@foreach($topGroupProducts as $topProducts)
						<div>
							@foreach($topProducts as $topProduct)
								<div class="product-widget">
									<a href="{{ Route('product', $topProduct->id) }}">
										<div class="product-img">
											<img src="{{asset($topProduct->image)}}" alt="">
										</div>
										<div class="product-body">
											<p class="product-category">{{ $topProduct->category_name }}</p>
											<h3 class="product-name">{{ $topProduct->name }}</h3>
											<h4 class="product-price">${{ $topProduct->price }}</h4>
										</div>
									</a>
								</div>
							@endforeach
						</div>
					@endforeach
				</div>
			</div>

			<div class="clearfix visible-sm visible-xs"></div>
			<div class="col-md-4 col-xs-6">
				<div class="section-title">
					<h4 class="title">New Product</h4>
					<div class="section-nav">
						<div id="slick-nav-4" class="products-slick-nav"></div>
					</div>
				</div>

				<div class="products-widget-slick" data-nav="#slick-nav-4">
					@foreach($newGroupProducts as $newProducts)
						<div>
							@foreach($newProducts as $newProduct)
								<div class="product-widget">
									<a href="{{ Route('product', $newProduct->id) }}">
										<div class="product-img">
											<img src="{{asset($newProduct->image)}}" alt="">
										</div>
										<div class="product-body">
											<p class="product-category">{{ $newProduct->category_name }}</p>
											<h3 class="product-name">{{ $newProduct->name }}</h3>
											<h4 class="product-price">${{ $newProduct->price }}</h4>
										</div>
									</a>
								</div>
							@endforeach
						</div>
					@endforeach
				</div>
			</div>
			<div class="clearfix visible-sm visible-xs"></div>


		</div>
	</div>
</div>
<!-- /SECTION -->
<script type="text/javascript">
	var url = "{{ Route('cart.add') }}";
	var urlDel = "{{ Route('cart.delete') }}";
	var cart_db = {!! $cart !!};
	$(document).ready(function() {
		$('.home').addClass('active');

		var item = $('.item');
		$.each(item, function(i, val) {
			$(val).addClass('active');
			return( i !== 0 );
		});
	});
</script>
<script type="text/javascript" src="{{asset('js/cart.js')}}"></script>
@endsection