@extends('layouts.client')
@section('content')
<div class="section">
	<div class="container">
		<div class="row">
			<div id="aside" class="col-md-3">
				<div class="aside">
					<h3 class="aside-title">Categories</h3>
					@isset($id)
						<input type="hidden" id="category_id" value="{{ $id }}">
					@endisset
					<div class="list-group">
						@foreach($categories as $category)
							<div class="item list-group-item">
								<a href="{{ Route('store.id', $category->id) }}">{{ $category->name }} <span>({{ $category->count }})</span></a>
							</div>
						@endforeach
					</div>
				</div>
				<div class="aside">
					<h3 class="aside-title">Price</h3>
					<div class="price-filter">
						<div class="input-number price-min">
							<input id="price-min" type="number">
							<span class="qty-up">+</span>
							<span class="qty-down">-</span>
						</div>
						<span>-</span>
						<div class="input-number price-max">
							<input id="price-max" type="number">
							<span class="qty-up">+</span>
							<span class="qty-down">-</span>
						</div>
						<button id="checkPrice" class="btn btn-default">OK</button>
					</div>
				</div>
				<div class="aside">
					<h3 class="aside-title">Top selling</h3>
					@foreach($topProducts as $topProduct)
						<div class="product-widget">
							<a href="{{ Route('product', $topProduct->id) }}">
								<div class="product-img">
									<img src="{{ $topProduct->image }}">
								</div>
								<div class="product-body">
									<p class="product-category">{{ $topProduct->category_name }}</p>
									<h3 class="product-name">{{ $topProduct->name }}</h3>
									<h4 class="product-price">${{ $topProduct->price }}</h4>
								</div>
							</a>
						</div>
					@endforeach
				</div>
			</div>
			<div id="store" class="col-md-9">
				<div class="store-filter clearfix">
					<div class="store-sort">
						<label>
							Sort By:
							<select class="input-select" id="sort">
								<option value="ASC">Low Price</option>
								<option value="DESC">High Price</option>
							</select>
						</label>
					</div>
				</div>
				<div id="item">
					<div class="row">
						@foreach($products as $product)
							<div class="col-md-4 col-xs-6">
								<div class="product">
									<a href="{{ Route('product', $product->id) }}">
										<div class="product-img">
											<img src="{{asset($product->image)}}">
										</div>
										<div class="product-body">
											<p class="product-category">{{ $product->category_name }}</p>
											<h3 class="product-name">{{ $product->name }}</h3>
											<h4 class="product-price">${{ $product->price }}</h4>
											<input type="hidden" id="qty" value="1">
										</div>
									</a>
									<div class="add-to-cart">
										<button class="add-to-cart-btn" data-id="{{ $product->id }}" data-name="{{ $product->name }}" data-image="{{ $product->image }}" data-price="{{ $product->price }}" data-qty="{{ $product->quantity }}"><i class="fa fa-shopping-cart"></i> add to cart</button>
									</div>
								</div>
							</div>
						@endforeach
					</div>
					{{ $products->links('vendor.pagination.default') }}
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var url = "{{ Route('cart.add') }}";
	var urlDel = "{{ Route('cart.delete') }}";
	var cart_db = {!! $cart !!};

	function getAjax(type, url) {
		var priceMax = $('#price-max').val();
		var priceMin = $('#price-min').val();
		var sort = $('#sort').val();
		var keyword = $("[name='keyword']").val();
		var cate = $("[name='category']").val();
		var id = $('#category_id').val();
		var loader = $("<div>").addClass("loader");
		var loader_img = $("<div>").addClass('loader_img');
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			type: type,
			url: url,
			data: {'priceMax' : priceMax, 'priceMin' : priceMin, 'sort' : sort, 'keyword': keyword, 'cate': cate, 'id': id},
			beforeSend: function() {
				$("body").append(loader);
				$("body").append(loader_img);
			},
			success: function(res) {
				$('div').removeClass("loader");
		    	$('div').removeClass("loader_img");
				$('#item').html(res);
			}
		});
	}
	$(document).ready(function() {
		$('.store').addClass('active');

		$('.input-number').each(function() {
			var input = $(this).find('input[type="number"]');
			input.val(0);

			$(this).find('.qty-up').click(function() {
				var value = parseInt(input.val()) + 1;
				input.val(value);
			});
			$(this).find('.qty-down').click(function() {
				var value = parseInt(input.val()) - 1;
				value = (value < 0) ? 0 : value;
				input.val(value);
			});

		});

		$(document).on('click', '.store-pagination a', function(event) {
			event.preventDefault();
			var page = $(this).attr('href').split('page=')[1];
			var url = "{{ Route('page.page', ':page') }}";
			url = url.replace(':page', page);
			var type = "get";
			getAjax(type, url);
		});

		$('#checkPrice').click(function() {
			var url = "{{ Route('filter.index') }}";
			var type = "post";
			getAjax(type, url);
		});

		$('#sort').change(function() {
    		$('#checkPrice').click();
		});
	});
</script>
<script type="text/javascript" src="{{asset('js/cart.js')}}"></script>
@endsection