@extends('layouts.client')
@section('content')
<div class="section">
	<div class="container">
		<div class="row">
			<form method="post" action="{{ Route('checkout.checkout') }}">
				@csrf
				<div class="col-md-7">
					<div class="billing-details">
						<div class="section-title">
							<h3 class="title">Billing address</h3>
						</div>
						<div class="form-group">
							<input class="input" type="text" name="name" placeholder="Full Name" 
							@isset($user)
								value="{{ $user->name }}"
							@endisset>
		                  	@if($errors->has('name'))
		                  		<p style="color: red">{{ $errors->first('name') }}</p>
		                  	@endif
						</div>
						<div class="form-group">
							<input class="input" type="hidden" name="email" placeholder="Email"
							@isset($user)
								value="{{ $user->email }}"
							@endisset>
						</div>
						<div class="form-group">
							<input class="input" type="text" name="phone" placeholder="Telephone"
							@isset($user)
								value="{{ $user->phone }}"
							@endisset>
		                  	@if($errors->has('phone'))
		                  		<p style="color: red">{{ $errors->first('phone') }}</p>
		                  	@endif
						</div>
						<div class="form-group">
							<input class="input" type="text" name="address" placeholder="Address"
							@isset($user)
								value="{{ $user->address }}"
							@endisset>
		                  	@if($errors->has('address'))
		                  		<p style="color: red">{{ $errors->first('address') }}</p>
		                  	@endif
						</div>
						<div class="form-group">
							<input type="radio" name="gender" value="male" 
							@if(isset($user) && $user->gender == 'male')
								checked="checked"
							@endif
							>Male
							<input type="radio" name="gender" value="female"
							@if(isset($user) && $user->gender == 'female')
								checked="checked"
							@endif
							>Female
		                  	@if($errors->has('gender'))
		                  		<p style="color: red">{{ $errors->first('gender') }}</p>
		                  	@endif
						</div>
						<div class="form-group">
							<input type="date" name="birthday" class="input"
							@isset($user)
								value="{{ $user->birthday }}"
							@endisset>
						</div>
					</div>
				</div>
				<div class="col-md-5 order-details">
					<div class="section-title text-center">
						<h3 class="title">Your Order</h3>
					</div>
					<div class="order-summary">
						<div class="order-col">
							<div><strong>PRODUCT</strong></div>
							<div><strong>TOTAL</strong></div>
						</div>
						@isset($cartData)
							@foreach($cartData as $cartItem)
								<div class="order-products">
									<div class="order-col">
										<div><strong>{{ $cartItem['qty'] }}x</strong></div>
										<div><a href="{{ Route('product', $cartItem['product_id']) }}">{{ $cartItem['name'] }}</a></div>
										<div>${{ $cartItem['price'] * $cartItem['qty'] }}</div>
									</div>
								</div>
							@endforeach
						@endisset
						<div class="order-col">
							<div><strong>TOTAL</strong></div>
							<div><strong class="order-total">${{ $subtotal }}</strong></div>
						</div>
					</div>
					<button type="submit" class="btn-order primary-btn order-submit" value="submit">place order</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('.cart').addClass('active');
	});
</script>
@endsection